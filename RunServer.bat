@echo off

if not exist Engine\ (
	echo "Engine\ dir is not found! Contact Kyryl Sydorov!"
	echo "If you are Kyryl Sydorov, do mklink! (Oh no, here we go again)"
	pause
	exit
)

set EditorExe="%CD%\Engine\Binaries\Win64\UE4Editor.exe"
set ProjectPath="%CD%\WorldOfBanks.uproject"
set ServerMap="L_MainLevel"

call %EditorExe% %ProjectPath% %ServerMap% -server -log -debug