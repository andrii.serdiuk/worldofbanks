// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(Log_Character, Log, Log);
DECLARE_LOG_CATEGORY_EXTERN(Log_Connection, Log, Log);
DECLARE_LOG_CATEGORY_EXTERN(Log_Interactable, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Log_UI, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Log_Processing, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Log_BankDevice, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Log_Login, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Log_Wallet_Pick, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Log_Phone, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Log_Save, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Log_Bank_Device, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Log_Transaction, Log, All);

struct FConnectOptionKeys
{
	static const FString Name;
	static const FString Password;
};

struct FConnectErrors
{
	static const FString NoOptionKeys;
	static const FString InvalidPassword;
	static const FString AlreadyPlaying;
	static const FString BadPlayerControllerClass;
};

struct FPhoneMessage
{
	static const FString POSPayment;
	static const FString TransferStringSender;
	static const FString TransferStringReceiver;
	static const FString DepositString;
	static const FString ATMWithdrawal;
};

struct FTransactions
{
	static const FString POSTransaction;
	static const FString ATMWithdrawalTransaction;
	static const FString DepositFundsTransaction;
	static const FString TransferReceiverTransaction;
	static const FString TransferSenderTransaction;
};

struct FBankActions
{
	static const FString CardHasBeenBlocked;
	static const FString CardHasBeenUnblocked;
	static const FString PINHasBeenChanged;
};

struct FOTPMessages
{
	static const FString NewOTP;
};

struct FSaveOptions
{
	static const FString ServerSlotName;
	static const int32 ServerUserID;

	static const FString ClientSlotName;
	static const int32 ClientUserID;

	static const FString DefaultHost;
	static const FString DefaultLogin;
	static const FString DefaultPassword;
};

struct FMessages
{
	static const FString PlayerJoined;
	static const FString PlayerExited;
	static const FString DeviceAlreadyOccupied;
};

enum class ECurrency : uint8;
extern const TMap<ECurrency, FString> CurrencySymbols;
extern const TMap<ECurrency, FString> CurrencyNames;

struct FStringHelpers
{
	static FString GetStringFromSum(int32 Sum);
};