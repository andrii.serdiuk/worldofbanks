﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "Banknote.h"


bool operator==(const FBanknoteInfo& First, const FBanknoteInfo& Second)
{
	return  First.Denomination == Second.Denomination && First.Currency == Second.Currency;
}

bool operator==(const FBanknote& First, const FBanknote& Second)
{
	return (First.Denomination == Second.Denomination) && (First.Currency == Second.Currency);
}
