﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

/** Structure FBanknoteInfo is used to hold two value ECurrency and int32 for many actions in the game.
 *  Structure FBanknote is a physical representation of the FBanknoteInfo instance which exists in the world and can be
 *  interacted with.
 */

#pragma once

#include "Currency.h"
#include "Banknote.generated.h"

class APickupCash;

USTRUCT()
struct FBanknoteInfo
{
	GENERATED_BODY()
	
	ECurrency Currency;
	int32 Denomination;
};

USTRUCT(BlueprintType)
struct FBanknote
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
	ECurrency Currency;

	UPROPERTY(EditAnywhere)
	int32 Denomination;

	/** Texture, which will be shown in UI */
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UTexture2D> Texture;

	/** Mesh for PickupCash actor */
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UStaticMesh> Mesh;
};

FORCEINLINE uint32 GetTypeHash(const FBanknote& Banknote)
{
	return FCrc::MemCrc32(&Banknote, sizeof(FBanknote));
}

FORCEINLINE uint32 GetTypeHash(const FBanknoteInfo& Banknote)
{
	uint32 Result = 0;
	uint16* ResultFirst = (uint16*) &Result;
	ResultFirst[0] = Banknote.Denomination;
	ResultFirst[1] = static_cast<uint16>(Banknote.Currency);
	return Result;
}

bool operator==(const FBanknoteInfo& First, const FBanknoteInfo& Second);


bool operator==(const FBanknote& First, const FBanknote& Second);
