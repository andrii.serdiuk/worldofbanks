// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CardInfo.h"
#include "BankAccount.generated.h"

USTRUCT(BlueprintType)
struct FTransactionInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	FString Action;

	UPROPERTY(EditAnywhere)
	int32 Sum;
	
};

/** This struct is plain old data. Make sure to initialize it! */
USTRUCT(BlueprintType)
struct WORLDOFBANKS_API FServerCardInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	FCardInfo PublicCard;

	UPROPERTY(EditAnywhere)
	int32 PIN = 1234;

	/** Store in int32 as cents to avoid floating-point errors. For example, 15.20 == 1520 */
	UPROPERTY(EditAnywhere)
	int32 Balance = 0;

	UPROPERTY(EditAnywhere)
	bool bBlocked = false;

	UPROPERTY(EditAnywhere)
	int32 TriesLeft = 3;

	UPROPERTY(EditAnywhere)
	TArray<FTransactionInfo> Transactions;
};

/** This struct is plain old data. Make sure to initialize it! */
USTRUCT(BlueprintType)
struct WORLDOFBANKS_API FBankAccount
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	FString ClientName;

	UPROPERTY(EditAnywhere)
	TArray<FServerCardInfo> Cards;

	UPROPERTY(EditAnywhere)
	int64 PhoneNumber;

	UPROPERTY()
	int32 CodeOTP = -1;
};

FORCEINLINE bool operator==(const FBankAccount& Lhs, const FBankAccount& Rhs)
{
	// For quick search, checking by PhoneNumbers since they are unique
	return Lhs.PhoneNumber == Rhs.PhoneNumber;
}