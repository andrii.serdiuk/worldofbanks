// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Currency.h"
#include "BankInfo.generated.h"

enum class EFeeType : uint8
{
	ATM,
	Terminal,
	POS
};

USTRUCT(BlueprintType)
struct FFeePolicy
{
	GENERATED_BODY()

public:
	/** Returns fee in % accordingly to FeeType. Used to avoid multiple switch-cases in code */
	float GetFeeByType(EFeeType FeeType) const;

	FORCEINLINE bool IsServiceBlocked() const { return bServiceBlocked; }

private:
	/** Fee in ATM (in %) */
	UPROPERTY(EditAnywhere)
	float FeeATM = 0.f;

	/** Fee in Terminal (in %) */
	UPROPERTY(EditAnywhere)
	float FeeTerminal = 0.f;

	/** Fee in POS terminal (in %) */
	UPROPERTY(EditAnywhere)
	float FeePOS = 0.f;

	/** If true, transactions will be blocked when using this fee policy */
	UPROPERTY(EditAnywhere)
	bool bServiceBlocked = false;
};

USTRUCT(BlueprintType)
struct FCurrencyRates
{
	GENERATED_BODY()

	/** How many UAH will bank charge you for 1 unit of this currency */
	UPROPERTY(EditAnywhere)
	float RateSell = 1.f;

	/** How many UAH will bank give for 1 unit of this currency */
	UPROPERTY(EditAnywhere)
	float RateBuy = 1.f;
};

struct FFeeCalculation
{
	float OwnFee = 0.f;
	float OtherFee = 0.f;
	bool bAllowTransactions = true;
	EFeeType FeeType;

	const static FFeeCalculation Invalid;
};

UCLASS()
class WORLDOFBANKS_API UBankInfo : public UDataAsset
{
	GENERATED_BODY()

public:
	static FFeeCalculation CalculateFee(TSoftObjectPtr<const UBankInfo> CardOwner, TSoftObjectPtr<const UBankInfo> OtherBank, EFeeType FeeType);

	static bool CanBeServiced(TSoftObjectPtr<const UBankInfo> CardOwner, TSoftObjectPtr<const UBankInfo> DeviceOwner);
	
	static int32 ConvertCurrency(TSoftObjectPtr<const UBankInfo> Bank, ECurrency From, ECurrency To, int32 Value);
	
	FORCEINLINE const FString& GetBankName() const { return BankName; }
	
private:
	UPROPERTY(EditAnywhere)
	FString BankName;

	/** Fee, charged from client for any transaction (in devices of own and other banks) */
	UPROPERTY(EditAnywhere)
	FFeePolicy OwnFeePolicy;
	
	/** Fee, charged additionally by the device owner, depends on bank-issuer */
	UPROPERTY(EditAnywhere)
	TMap<TSoftObjectPtr<const UBankInfo>, FFeePolicy> FeePolicies;

	/** Fee, charged in case we can't find appropriate fee in FeePolicies */
	UPROPERTY(EditAnywhere)
	FFeePolicy DefaultFeePolicy;

	UPROPERTY(EditAnywhere)
	TMap<ECurrency, FCurrencyRates> CurrencyRates;
};