﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once
#include "WorldOfBanks/WorldOfBanks.h"

#include "Currency.generated.h"

UENUM(BlueprintType)
enum class ECurrency : uint8
{
	MIN = 0 UMETA(Hidden),
	Dollar UMETA(DisplayName = "Dollar"),
	Euro UMETA(DisplayName = "Euro"),
	Hryvnia UMETA(DisplayName = "Hryvnia"),
	MAX UMETA(Hidden), // dummy value needed for iteration over all enum values
};

FORCEINLINE FString SignTranslation(ECurrency Currency)
{
	return CurrencySymbols.FindChecked(Currency);
}