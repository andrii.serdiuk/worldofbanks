// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "BankInfo.h"
#include "WorldOfBanks/WorldOfBanks.h"

const FFeeCalculation FFeeCalculation::Invalid = {-1.f, -1.f, false};

float FFeePolicy::GetFeeByType(EFeeType FeeType) const
{
	switch (FeeType)
	{
		case EFeeType::ATM : return FeeATM;
		case EFeeType::Terminal : return FeeTerminal;
		case EFeeType::POS : return FeePOS;
		default: return -1.f;
	}
}

FFeeCalculation UBankInfo::CalculateFee(TSoftObjectPtr<const UBankInfo> CardOwner, TSoftObjectPtr<const UBankInfo> OtherBank,
                                        EFeeType FeeType)
{
	if (CardOwner.IsNull() || OtherBank.IsNull())
	{
		UE_LOG(Log_Processing, Error, TEXT("Calculate Fee with invalid params"));
		return FFeeCalculation::Invalid;
	}

	// Sync load TSoftObjectPtrs
	CardOwner.LoadSynchronous();
	OtherBank.LoadSynchronous();

	FFeeCalculation FeeCalculation;
	FeeCalculation.FeeType = FeeType;
	FeeCalculation.OwnFee = CardOwner->OwnFeePolicy.GetFeeByType(FeeType);
	
	if (CardOwner == OtherBank)
	{
		// If we try to use the card in device, owner by card issuer, do not apply additional fees
		FeeCalculation.bAllowTransactions = !CardOwner->OwnFeePolicy.IsServiceBlocked(); // OMG! Who will block it?
		return FeeCalculation;
	}

	if (const FFeePolicy* FeePolicy = OtherBank->FeePolicies.Find(CardOwner))
	{
		// We found fee policy for a specific bank, using it
		FeeCalculation.bAllowTransactions = !FeePolicy->IsServiceBlocked();
		FeeCalculation.OtherFee = FeePolicy->GetFeeByType(FeeType);
	}
	else
	{
		// We are using default fee policy
		FeeCalculation.bAllowTransactions = !OtherBank->DefaultFeePolicy.IsServiceBlocked();
		FeeCalculation.OtherFee = OtherBank->DefaultFeePolicy.GetFeeByType(FeeType);
	}
	
	return FeeCalculation;
}

bool UBankInfo::CanBeServiced(TSoftObjectPtr<const UBankInfo> CardOwner, TSoftObjectPtr<const UBankInfo> DeviceOwner)
{
	if (CardOwner.IsNull() || DeviceOwner.IsNull())
	{
		UE_LOG(Log_Processing, Error, TEXT("UBankInfo::CanBeServiced with invalid params"));
		return false;
	}

	// Sync load TSoftObjectPtrs
	CardOwner.LoadSynchronous();
	DeviceOwner.LoadSynchronous();
	
	if (CardOwner == DeviceOwner)
	{
		// If we try to use the card in device, owner by card issuer, do not apply additional fees
		return !(CardOwner->OwnFeePolicy.IsServiceBlocked()); // OMG! Who will block it?
	}

	if (const FFeePolicy* FeePolicy = DeviceOwner->FeePolicies.Find(CardOwner))
	{
		// We found fee policy for a specific bank, using it
		return !FeePolicy->IsServiceBlocked();
	}
	else
	{
		// We are using default fee policy
		return !DeviceOwner->DefaultFeePolicy.IsServiceBlocked();
	}
}

int32 UBankInfo::ConvertCurrency(TSoftObjectPtr<const UBankInfo> Bank, ECurrency From, ECurrency To, int32 Value)
{
	if (From == To)
	{
		return Value;
	}
	
	if (Bank.IsNull())
	{
		UE_LOG(Log_Processing, Error, TEXT("Convert Currency with invalid params"));
		return -1;
	}
	Bank.LoadSynchronous();

	// National currency is UAH, so we should convert From to UAH, and then UAH to To
	int32 ValueUAH = (From == ECurrency::Hryvnia)
		? Value
		: Bank->CurrencyRates.FindChecked(From).RateBuy * Value;

	return (To == ECurrency::Hryvnia) ? ValueUAH : (Value / Bank->CurrencyRates.FindChecked(To).RateSell);
}
