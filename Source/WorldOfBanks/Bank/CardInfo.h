﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "BankInfo.h"
#include "Currency.h"
#include "CardInfo.generated.h"

/** This struct is plain old data. Make sure to initialize it!
 * Structure FCardInfo represents information about a card that can be obtained on the client`s side.
 * Instances of this class are used for performing transaction in the processing center. 
 */
USTRUCT(BlueprintType)
struct FCardInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
	FString CardNumber;

	/** Expiration month which is used only for UI.*/
	UPROPERTY(EditAnywhere)
	int32 ExpMonth;

	/** Expiration year which is used only for UI.*/
	UPROPERTY(EditAnywhere)
	int32 ExpYear;

	UPROPERTY(EditAnywhere)
	ECurrency Currency;

	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<const UBankInfo> BankIssuer;
	
	UPROPERTY(EditAnywhere)
	TSoftClassPtr<class APickupCard> CardClass;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<class UCardWidget> CardWidgetClass;

	// Static instance of invalid card used by bank devices when it is not occupied.
	static const FCardInfo InvalidCard;
};

bool operator==(const FCardInfo& Lhs, const FCardInfo& Rhs);

bool operator!=(const FCardInfo& Lhs, const FCardInfo& Rhs);