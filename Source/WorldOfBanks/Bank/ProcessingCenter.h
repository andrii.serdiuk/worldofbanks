// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

/** The core class for the whole bank logic in the project. This class is responsible for performing all the transactions, for blocking and unblocking the cards
* and for sending the users messages about different actions, i.e. transactions, blocking cars, OTP messages.
*/

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/Bank/Response.h"
#include "WorldOfBanks/Core/SaveObjects.h"
#include "ProcessingCenter.generated.h"

struct FMoneyInfo;
class ABankPOS;
class ABankTerminal;
class ABankATM;
class UBankInfo;
class ABankDevice;

class UPhoneComponent;
using PhoneFunc = void(UPhoneComponent::*)(const FString&);

class UCoreSaveGame;

UCLASS()
class WORLDOFBANKS_API UProcessingCenter : public UObject
{
	GENERATED_BODY()

	DECLARE_MULTICAST_DELEGATE_OneParam(FPhoneMessageCreated, const FString&);

	/** In general, friendship is bad!
	 * However, in this particular case, it doesn't ruin the encapsulation,
	 * since this function is called "from Server on Server" only when new player joins the game,
	 * we don't want to expose some private functions, so friendship is "the least evil"
	 */
	friend class ACoreGameMode;
	
public:	
	// Sets default values
	UProcessingCenter(const FObjectInitializer& ObjectInitializer);

	static UProcessingCenter* CreateInstance(UWorld* World);

	/** Performs payment using POS-terminal
	 * Possible responses:
	 * 1. TransactionNotAllowed - POS-terminal`s owner and the card`s bank do not support transactions
	 * 2. Blocked - The card is blocked
	 * 3. InsufficientFunds - The card is not blocked, but the balance is less than the payment sum (including fees)
	 * 4. Success - The card is not blocked and there is enough money to perform the transaction
	 * @param POS The POS-terminal being used.
	 * @param Card The card being used.
	 * @param Currency The specified currency.
	 * @param Sum The specified sum.
	 */
	EResponse POSPayment(ABankPOS * POS, const FCardInfo& Card, const ECurrency& Currency, int32 Sum);
	
	/** Withdraws money from an ATM
	 * Possible responses:
	 * 1. TransactionNotAllowed - ATM`s owner and the card`s bank do not support transactions
	 * 2. Blocked - The card is blocked
	 * 3. InsufficientFunds - The card is not blocked, but the balance is less than the payment sum (including fees)
	 * 4. UnknownCard - The player`s card is not register on the server which actually should never happen if the player is
	 * not a cheater
	 * 5. Success - The card is not blocked and there is enough money to perform the transaction
	 * @param ATM The ATM being used.
	 * @param Card The card which is being used.
	 * @param Currency The currency which was specified by the user.
	 * @param Sum The sum that the client wants to withdraw.
	 */
	EResponse WithdrawFunds(ABankATM* ATM, const FCardInfo& Card, const ECurrency& Currency, int32 Sum);
	
	/** Takes all the money in the terminal, applies all the fees and currency conversions, and adds the resulting sum
	 * to the card
	 * Possible responses:
	 * 1. TransactionNotAllowed - Terminal`s owner and the card`s bank do not support transactions
	 * 2. UnknownCard - Given card is unknown on the server
	 * 3. Success - Transactions are allowed and the card was found successfully
	 * Note that we don`t check whether the card is blocked or not
	 * @param Terminal The terminal being used.
	 * @param Card The client`s card.
	 * @param Money All the banknotes that were inserted in the terminal during the interaction.
	 */
	EResponse DepositFunds(ABankTerminal* Terminal, const FCardInfo& Card, const TArray<FBanknoteInfo>& Money);
	
	/** Validates the PIN received from the user
	 * Possible responses:
	 * 1. Blocked - Card has already been blocked - In this case we don`t even check if the PIN is correct.
	 * 2. HasBeenJustBlocked - The suggested PIN was invalid and all attempts have been used. The processing center blocks the card, the mini-game starts.
	 * 3. InvalidPIN - Card has not been blocked, but player`s PIN was wrong. There are still attempts left.
	 * 4. UnknownCard - The player`s card is not register on the server which actually shouldn`t happen if the player is
	 * not a cheater.
	 * 5. Success - The PIN is correct.
	 * @param BankDevice The device which is used
	 * @param Card The client`s card
	 * @param PIN PIN used for authentication
	 */
	EResponse TryAuthenticate(const ABankDevice* BankDevice, const FCardInfo& Card, const int32 PIN);
	
	/** Transfers money from one card to another
	 * Possible responses:
	 * 1. TransactionNotAllowed - Terminal`s owner and the card`s bank do not support transactions
	 * 2. UnknownDestination - The destination card is invalid
	 * 3. DestinationNotAllowedTransaction - The destination card`s bank and the terminal`s bank don`t get along with each other
	 * 4. InsufficientFunds - The card`s balance is lower than the specified (sum + fees) after possible currency conversion
	 * 5. Blocked - Sender`s card is blocked (it doesn't matter if the receiver`s card is blocked or not)
	 * 6. UnknownCard - Sender`s card is unknown
	 * 7. Success - The transaction is successful
	 * @param Terminal The terminal which is used.
	 * @param CardFrom The sender`s card.
	 * @param CardNumTo The receiver`s card.
	 * @param Currency The currency in which the transaction is performed.
	 * @param Sum The sum which is to be sent.
	 */
	EResponse TransferToCard(const ABankTerminal* Terminal, const FCardInfo& CardFrom,
	  const FString& CardNumTo, ECurrency Currency, int32 Sum);

	/** Returns the current balance of the card
	 * @param The card the balance of which (whose) will be returned.
	 */
	TPair<EResponse, int32> GetCurrentBalance(const FCardInfo& Card);
	
	/** This method changes the card`s PIN in the specified bank device. Possible responses:
	 * 1. UnknownCard: The specified card is not registered in the system.
	 * 2. Failure: The specified PIN is not correct which means that it is either negative or greater than 9999.
	 * 3. Success: The PIN has been changed successfully.
	 * @param Device The device (either a terminal or an ATM) which is used.
	 * @param CardInfo The card the PIN of which (whose) is to be changed.
	 * @param PIN The new PIN.
	 */
	EResponse ChangePin(const ABankDevice* Device, const FCardInfo& CardInfo, int32 PIN);

	/** Generates the OTP for the bank account which refers to this phone number. Possible responses:
	 * 1. InvalidPhoneNumber: The specified phone number is not bound to any bank account. 
	 * 2. Success: The corresponding account was found and the generated OTP was sent.
	 * @param Terminal The terminal being used.
	 * @param PhoneNumber The specified phone number.
	 */
	EResponse GenerateAndSendOTP(const ABankTerminal* Terminal, const int64 PhoneNumber);

	/** Returns all the cards that are bound to this account. This function is used for blocking/unblocking cards.
	 * If the specified phone number is incorrect, this method returns an empty array.
	 * @param Terminal The terminal being used.
	 * @param PhoneNumber The specified phone number.
	 */
	TArray<FCardInfo> GetAllCards(const ABankTerminal* Terminal, int64 PhoneNumber);

	/** This method checks that the given OTP is correct.
	 * @param Terminal The terminal being used.
	 * @param PhoneNumber The specified phone number.
	 * @param OTP The specified OTP.
	 */
	bool IsOTPValid(const ABankTerminal* Terminal, int64 PhoneNumber, int32 OTP);

	/** This method changes the blocked state of the given card. 
	 * @param Terminal The terminal which is used for this operation.
	 * @param CardInfo The card which the client wants to block or unblock.
	 * @param bToBlock Specified the client`s intention. If it is true, the card will be blocked, otherwise it will be unblocked.
	 */
	EResponse SetBlockedState(const ABankTerminal* Terminal, const FCardInfo& CardInfo, bool bToBlock);

	/** This method returns all the transactions of the specified card.
	 * @param CardInfo The card the transactions of which (whose) will be returned.
	 */
	TArray<FTransactionInfo> GetTransactions(const FCardInfo& CardInfo);
	
	/** Registers phone component, so it will receive message as soon as processing center sends it */
	FDelegateHandle RegisterPhone(int64 PhoneNumber, UPhoneComponent* Phone, PhoneFunc Handler);

	/** Unregisters phone component, so it will not receive new messages */
	void UnregisterPhone(int64 PhoneNumber, FDelegateHandle Handle);

	/** Registers new account in a specific bank and returns ref to account in the database
	 * @param Bank The bank in which this account will be registered.
	 * @param Account The account that is to be registered.
	 */
	const FBankAccount& RegisterNewAccount(TSoftObjectPtr<const UBankInfo> Bank, const FBankAccount& Account);

	/** Issues new card in a specific bank and returns ref to public info about card
	 * @param Bank The bank in which the card will be registered.
	 * @param Account Account to be registered.
	 * @param Currency The card`s currency.
	 */
	const FCardInfo& RegisterNewCard(TSoftObjectPtr<const UBankInfo> Bank, const FBankAccount& Account, ECurrency Currency);

	/** Get phone number connected with the card.
	 * @param Card The client`s card.
	 */
	int64* GetPhoneNumber(const FCardInfo& Card);
	
protected:
	/** Pseudo begin play. Called in static CreateInstance after construction */
	void BeginPlay();

private:
	/** Save information about UProcessingCenter to SaveData */
	const UProcessingCenter& operator>>(UCoreSaveGame* SaveData) const;

	/** Load information about UProcessingCenter from SaveData */
	UProcessingCenter& operator<<(UCoreSaveGame* SaveData);

	//TPair<const FServerCardInfo*, int64> FindServerCardAndPhoneNumber(const FCardInfo& PublicCard);
	/** This method finds the account to which the given card is bound.
	 * @param PublicCard The specified card.
	 */
	FBankAccount* FindAccount(const FCardInfo& PublicCard);
	
	/** This method finds the account to which the given phone number is bound.
	 * @param PhoneNumber The specified phone number.
	 */
	FBankAccount* FindAccount(int64 PhoneNumber);

	/** This method finds the account in the specified bank with the specified phone number.
	 * @param Bank The specified bank.
	 * @param PhoneNumber The specified phone number.
	 */
	FBankAccount* FindAccount(TSoftObjectPtr<const UBankInfo> Bank, int64 PhoneNumber);

	/** This method finds the card with the specified card number.
	 * @param CardNumber The specified card number.
	 */
	FCardInfo* FindCardByNumber(const FString& CardNumber);

	/** This method finds the server version of the card with the specified card number.
	 * @param CardNumber The specified card number.
	 */
	FServerCardInfo* FindServerCardByNumber(const FString& CardNumber);

	/** This method finds the card with the specified public number.
	 * @param CardNumber The specified public card.
	 */
	FServerCardInfo* FindServerCard(const FCardInfo& PublicCard);

	/** This method finds the server version of the card with the specified card number.
	 * @param CardNumber The specified card number.
	 */
	FServerCardInfo* FindServerCard(const FString& CardNumber);

	/** This method adds funds to the specified server card.
	 * @param ServerCard The specified server card.
	 * @param Currency The currency used for transaction.
	 * @param Sum The sum of the transaction.
	 */
	void AddFundsToCard(FServerCardInfo& ServerCard, ECurrency Currency, int32 Sum);

	/** This method adds funds to the specified server card.
	* @param PublicCard  The specified public card.
	* @param Currency The currency used for transaction.
	* @param Sum The sum of the transaction.
	*/
	void AddFundsToCard(const FCardInfo& PublicCard, ECurrency Currency, int32 Sum);
	
	/** This method removes funds from the specified server card.
	 * @param ServerCard The specified server card.
	 * @param Currency The currency used for transaction.
	 * @param Sum The sum of the transaction.
	 */
	EResponse RemoveFundsFromCard(FServerCardInfo& ServerCard, ECurrency Currency, int32 Sum);

	/** This method removes funds from the specified card.
	* @param PublicCard The specified server card.
	* @param Currency The currency used for transaction.
	* @param Sum The sum of the transaction.
	*/
	EResponse RemoveFundsFromCard(const FCardInfo& PublicCard, ECurrency Currency, int32 Sum);

	/** This method sends message to the given phone number.
	 * @param Phone The phone number.
	 * @param Message The message to be sent.
	 */
	void SendMessage(int64 Phone, const FString& Message);
	
	void SendMessage(int64 Phone, const FString& FormatString, const FFormatNamedArguments& FormatArgs);
	
	/** Additional function which formats the card number in a representative way. For instance, 1234567891234567
	 * will be formatted into 1234 5678 9123 4567
	 * @param CardNumber Unformatted card number.
	 */
	FString FormatCardNumber(const FString& CardNumber);
	
	/** Main "Data Base" of the Processing Center. Stores Map of Banks to Accounts */
	UPROPERTY()
	TMap<TSoftObjectPtr<const UBankInfo>, FBankData> Banks;

	TMap<int64, FPhoneMessageCreated> Subscribers;

	/** Information about used card numbers to avoid giving the same numbers for different cards */
	UPROPERTY()
	TSet<int64> UsedCardNumbers;

	int64 GenerateUniqueCardNumber();

#if !(UE_BUILD_SHIPPING)
	// should not be included in Shipping build mode
	public:
	FORCEINLINE void DebugSendMessage(int64 PhoneNumber, const FString& Message)
	{
		SendMessage(PhoneNumber, Message);
	}

	void DebugAddCents(const FString& CardNumber, int32 Cents);
#endif
};

