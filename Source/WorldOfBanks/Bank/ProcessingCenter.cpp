/** Realisation of the ProcessingCenter class specified in ProcessingCenter.h */

// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "ProcessingCenter.h"
#include "WorldOfBanks/Components/PhoneComponent.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "WorldOfBanks/Core/CoreGameState.h"
#include "WorldOfBanks/Core/CoreSaveGame.h"
#include "WorldOfBanks/Devices/BankATM.h"
#include "WorldOfBanks/Devices/BankPOS.h"
#include "WorldOfBanks/Devices/BankTerminal.h"

UProcessingCenter::UProcessingCenter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void UProcessingCenter::BeginPlay()
{
	ACoreGameState* CoreGameState = GetWorld()->GetGameState<ACoreGameState>();
	check(CoreGameState);

	for (const TSoftObjectPtr<const UBankInfo>& Bank : CoreGameState->GetBanks())
	{
		Banks.Add(Bank);
	}
}

const UProcessingCenter& UProcessingCenter::operator>>(UCoreSaveGame* SaveData) const
{
	check(SaveData);
	
	SaveData->Banks = Banks;
	SaveData->UsedCardNumbers = UsedCardNumbers;

	return *this;
}

UProcessingCenter& UProcessingCenter::operator<<(UCoreSaveGame* SaveData)
{
	check(SaveData);

	// We can do moving FROM save data, because these fields will not be used after this operator
	Banks = MoveTemp(SaveData->Banks);
	UsedCardNumbers = MoveTemp(SaveData->UsedCardNumbers);

	return *this;
}

UProcessingCenter* UProcessingCenter::CreateInstance(UWorld* World)
{
	UProcessingCenter* ProcessingCenter = NewObject<UProcessingCenter>(World);
	verifyf(ProcessingCenter, TEXT("Fatal: ProcessingCenter has not been created!"));

	ProcessingCenter->BeginPlay();
	
	return ProcessingCenter;
}


EResponse UProcessingCenter::POSPayment(ABankPOS* POS, const FCardInfo& Card, const ECurrency& Currency, int32 Sum)
{
	check(Sum > 0);
	FFeeCalculation FeeCalc = UBankInfo::CalculateFee(POS->GetBankIssuer(), Card.BankIssuer, EFeeType::POS);
	if (FeeCalc.bAllowTransactions)
	{
		const int32 SumWithFee = Sum * (1.f + FeeCalc.OwnFee + FeeCalc.OtherFee);
		FServerCardInfo* ServerCardInfo = FindServerCard(Card);
		const EResponse Response = RemoveFundsFromCard(*ServerCardInfo, Currency, SumWithFee);
		if (Response == EResponse::Success)
		{
			int32 Commission = SumWithFee - Sum;
			const FFormatNamedArguments MessageArgs = {
				{"Sum", FText::FromString(FStringHelpers::GetStringFromSum(SumWithFee))},
				{"Currency", FText::FromString(CurrencySymbols.FindChecked(Currency))},
				{"Card", FText::FromString(FormatCardNumber(Card.CardNumber))},
				{"Commission", FText::FromString(FStringHelpers::GetStringFromSum(Commission))}
			};

			const int64 PhoneNumber = FindAccount(Card)->PhoneNumber;
			SendMessage(PhoneNumber, FPhoneMessage::POSPayment, MessageArgs);
			
			const FTextFormat TransactionFormat = FTextFormat::FromString(FTransactions::POSTransaction);
			const FFormatNamedArguments TransactionArgs = {
				{"Sum", FText::FromString(FStringHelpers::GetStringFromSum(SumWithFee))},
				{"Currency", FText::FromString(CurrencySymbols.FindChecked(Currency))}
			};
			const FTransactionInfo Transaction {FText::Format(TransactionFormat, TransactionArgs).ToString(), -SumWithFee};
			ServerCardInfo->Transactions.Add(Transaction);
		}
		return Response;
	}
	return EResponse::TransactionNotAllowed;
}


EResponse UProcessingCenter::WithdrawFunds(ABankATM* ATM, const FCardInfo& Card, const ECurrency& Currency, int32 Sum)
{
	FFeeCalculation FeeCalc = UBankInfo::CalculateFee(Card.BankIssuer, ATM->GetBankIssuer(), EFeeType::ATM);
	if (FeeCalc.bAllowTransactions)
	{
		const int32 SumWithFee = Sum * (1.f + FeeCalc.OwnFee + FeeCalc.OtherFee);
		FServerCardInfo* ServerCardInfo = FindServerCard(Card);
		if (!ServerCardInfo)
		{
			return EResponse::UnknownCard;
		}
		
		EResponse WithdrawalResult = RemoveFundsFromCard(*ServerCardInfo, Currency, SumWithFee);
		if (WithdrawalResult == EResponse::Success)
		{
			if (FBankAccount* Account = FindAccount(Card))
			{
				int32 Commission = SumWithFee - Sum;
				const FFormatNamedArguments StreamerArgs = {
					{"Sum", FText::FromString(FStringHelpers::GetStringFromSum(SumWithFee))},
					{"Currency", FText::FromString(CurrencySymbols.FindChecked(Currency))},
					{"Card", FText::FromString(FormatCardNumber(Card.CardNumber))},
					{"Commission", FText::FromString(FStringHelpers::GetStringFromSum(Commission))}
				};
				SendMessage(Account->PhoneNumber, FPhoneMessage::ATMWithdrawal, StreamerArgs);
			}
			
			const FTextFormat TransactionFormat = FTextFormat::FromString(FTransactions::ATMWithdrawalTransaction);
			const FFormatNamedArguments TransactionArgs = {
				{"Sum", FText::FromString(FStringHelpers::GetStringFromSum(SumWithFee))},
				{"Currency", FText::FromString(CurrencySymbols.FindChecked(Currency))}
			};
			const FTransactionInfo Transaction {FText::Format(TransactionFormat, TransactionArgs).ToString(), -SumWithFee};
			ServerCardInfo->Transactions.Add(Transaction);
		}
		return WithdrawalResult;
	}
	return EResponse::TransactionNotAllowed;
}

EResponse UProcessingCenter::DepositFunds(ABankTerminal* Terminal, const FCardInfo& Card, const TArray<FBanknoteInfo>& Money)
{
	FFeeCalculation FeeCalc = UBankInfo::CalculateFee(Card.BankIssuer, Terminal->GetBankIssuer(), EFeeType::Terminal);
	if (FeeCalc.bAllowTransactions)
	{
		FBankAccount* Account = FindAccount(Card);
		FServerCardInfo* ServerCardInfo = FindServerCard(Card);
		if (!Account || !ServerCardInfo)
		{
			return EResponse::UnknownCard;
		}
		
		FString TransactionStringRepresentation;
		int32 TotalSumInCardCurrency = 0;
		int32 CommissionInCardCurrency = 0;
		for (const FBanknoteInfo& Info : Money)
		{
			const int32 Sum = Info.Denomination;
			const int32 SumWithFee = Sum * (1.f - FeeCalc.OwnFee - FeeCalc.OtherFee);
			AddFundsToCard(*ServerCardInfo,Info.Currency, SumWithFee);

			CommissionInCardCurrency += UBankInfo::ConvertCurrency(Card.BankIssuer, Info.Currency, Card.Currency, (Sum - SumWithFee));
			TotalSumInCardCurrency += UBankInfo::ConvertCurrency(Card.BankIssuer, Info.Currency, Card.Currency, Sum);
			TransactionStringRepresentation.Append(FStringHelpers::GetStringFromSum(Sum)).Append(CurrencySymbols.FindChecked(Info.Currency)).Append(", ");
		}
		TransactionStringRepresentation = TransactionStringRepresentation.LeftChop(2);
		
		//Додати назву банка
		const FFormatNamedArguments StreamerArgs = {
			{"Sum", FText::FromString(TransactionStringRepresentation)},
			{"Card", FText::FromString(FormatCardNumber(Card.CardNumber))},
			{"Commission", FText::FromString(FStringHelpers::GetStringFromSum(CommissionInCardCurrency))},
			{"Currency", FText::FromString(CurrencySymbols.FindChecked(Card.Currency))}
		};
				
		SendMessage(Account->PhoneNumber, FPhoneMessage::DepositString, StreamerArgs);
		
		const FTextFormat TransactionFormat = FTextFormat::FromString(FTransactions::DepositFundsTransaction);
		const FFormatNamedArguments TransactionArgs = {
			{"Money", FText::FromString(TransactionStringRepresentation)}
		};
		const FTransactionInfo Transaction {FText::Format(TransactionFormat, TransactionArgs).ToString(), TotalSumInCardCurrency};
		ServerCardInfo->Transactions.Add(Transaction);

		
		return EResponse::Success;
	}
	return EResponse::TransactionNotAllowed;
}

EResponse UProcessingCenter::TryAuthenticate(const ABankDevice* BankDevice, const FCardInfo& Card, const int32 PIN)
{
	if (FServerCardInfo* ServerCardInfo = FindServerCard(Card))
	{
		if (!UBankInfo::CanBeServiced(ServerCardInfo->PublicCard.BankIssuer, BankDevice->GetBankIssuer()))
		{
			return EResponse::TransactionNotAllowed;
		}
		
		if (ServerCardInfo->bBlocked)
		{
			return EResponse::Blocked;
		}
		
		checkf(ServerCardInfo->TriesLeft > 0,
			TEXT("If a card isn't blocked, its amount of left attempts must be greater than zero"));
		
		if (ServerCardInfo->PIN != PIN)
		{
			--ServerCardInfo->TriesLeft;
			if (ServerCardInfo->TriesLeft == 0)
			{
				ServerCardInfo->bBlocked = true;
				return EResponse::HasBeenJustBlocked;
			}
			return EResponse::InvalidPIN;
		}
		ACoreGameState* CoreGameState = GetWorld()->GetGameState<ACoreGameState>();
		checkf(CoreGameState, TEXT("CoreGameState is undefined!"));
		ServerCardInfo->TriesLeft = CoreGameState->GetInitialPINTriesAmount();
		return EResponse::Success;
	}
	return EResponse::UnknownCard;
}

EResponse UProcessingCenter::TransferToCard(const ABankTerminal* Terminal, const FCardInfo& CardFrom, const FString& CardNumTo,
                                       ECurrency Currency, int32 Sum)
{
	FFeeCalculation FeeCalc = UBankInfo::CalculateFee(CardFrom.BankIssuer, Terminal->GetBankIssuer(), EFeeType::Terminal);
	if (!FeeCalc.bAllowTransactions)
	{
		return EResponse::TransactionNotAllowed;
	}
	
	FServerCardInfo* ReceiverCard = FindServerCardByNumber(CardNumTo);
	if (!ReceiverCard)
	{
		return EResponse::UnknownDestination;
	}

	FServerCardInfo* SenderCard = FindServerCard(CardFrom);
	if (!SenderCard)
	{
		return EResponse::UnknownCard;
	}
	
	bool bDestinationCanAcceptTransaction = UBankInfo::CanBeServiced(ReceiverCard->PublicCard.BankIssuer, Terminal->GetBankIssuer());

	if (!bDestinationCanAcceptTransaction)
	{
		return EResponse::DestinationNotAllowedTransaction;
	}
	
	const int32 SumWithFee = Sum * (1.f + FeeCalc.OtherFee + FeeCalc.OwnFee);
	
	EResponse RemovalResult = RemoveFundsFromCard(*SenderCard, Currency, SumWithFee);
	if (RemovalResult == EResponse::Success)
	{
		AddFundsToCard(*ReceiverCard, Currency, Sum);
		
		const FTextFormat TransactionSenderFormat = FTextFormat::FromString(FTransactions::TransferSenderTransaction);
		const FFormatNamedArguments TransactionSenderArgs = {
			{"Sum", FText::FromString(FStringHelpers::GetStringFromSum(SumWithFee))},
			{"Currency", FText::FromString(CurrencySymbols.FindChecked(Currency))},
			{"ReceiverCard", FText::FromString(FormatCardNumber(ReceiverCard->PublicCard.CardNumber))}
		};
		
		const FTransactionInfo SenderTransaction {FText::Format(TransactionSenderFormat, TransactionSenderArgs).ToString(), -SumWithFee};
		SenderCard->Transactions.Add(SenderTransaction);

		const FTextFormat TransactionReceiverFormat = FTextFormat::FromString(FTransactions::TransferReceiverTransaction);
		const FFormatNamedArguments TransactionReceiverArgs = {
			{"Sum", FText::FromString(FStringHelpers::GetStringFromSum(Sum))},
			{"Currency", FText::FromString(CurrencySymbols.FindChecked(Currency))},
			{"SenderCard", FText::FromString(FormatCardNumber(SenderCard->PublicCard.CardNumber))}
		};
		
		const FTransactionInfo ReceiverTransaction {FText::Format(TransactionReceiverFormat, TransactionReceiverArgs).ToString(), SumWithFee};
		ReceiverCard->Transactions.Add(ReceiverTransaction);

		FBankAccount* SenderAccount = FindAccount(CardFrom);

		FString SenderCardFormatted = FormatCardNumber(CardFrom.CardNumber);
		FString ReceiverCardFormatted = FormatCardNumber(ReceiverCard->PublicCard.CardNumber);
		
		if (SenderAccount)
		{
			int32 Commission = SumWithFee - Sum;
			const FFormatNamedArguments StreamerArgs = {
				{"Sum", FText::FromString(FStringHelpers::GetStringFromSum(SumWithFee))},
				{"Currency", FText::FromString(CurrencySymbols.FindChecked(Currency))},
				{"ReceiverCard", FText::FromString(ReceiverCardFormatted)},
				{"SenderCard", FText::FromString(SenderCardFormatted)},
				{"Commission", FText::FromString(FStringHelpers::GetStringFromSum(Commission))}
			};
			SendMessage(SenderAccount->PhoneNumber, FPhoneMessage::TransferStringSender, StreamerArgs);
		}
		
		FBankAccount* ReceiverAccount = FindAccount(ReceiverCard->PublicCard);
		if (ReceiverAccount)
		{
			//Додати назву банка
			const FFormatNamedArguments StreamerArgs = {
				{"Sum", FText::FromString(FStringHelpers::GetStringFromSum(Sum))},
				{"Currency", FText::FromString(CurrencySymbols.FindChecked(Currency))},
				{"ReceiverCard", FText::FromString(ReceiverCardFormatted)},
				{"SenderCard", FText::FromString(SenderCardFormatted)}
			};
			SendMessage(ReceiverAccount->PhoneNumber, FPhoneMessage::TransferStringReceiver, StreamerArgs);
		}
	}
			
	return RemovalResult;
}

TPair<EResponse, int32> UProcessingCenter::GetCurrentBalance(const FCardInfo& Card)
{
	//Check that the card exists
	if (FServerCardInfo* ServerCardInfo = FindServerCard(Card))
	{
		if (ServerCardInfo->bBlocked)
		{
			return TPair<EResponse, int32>(EResponse::Blocked, -1);
		}
		return TPair<EResponse, int32>(EResponse::Success, ServerCardInfo->Balance);
	}
	return TPair<EResponse, int32>(EResponse::UnknownCard, -1);
}

EResponse UProcessingCenter::ChangePin(const ABankDevice* Device, const FCardInfo& CardInfo, int32 PIN)
{
	FServerCardInfo* ServerCardInfo = FindServerCard(CardInfo);
	if(!ServerCardInfo)
	{
		 return EResponse::UnknownCard;
	}
	
	if(!UBankInfo::CanBeServiced(ServerCardInfo->PublicCard.BankIssuer, Device->GetBankIssuer()))
	{
		//This card can`t be serviced by the device`s bank.
		return EResponse::UnknownCard;
	}
	
	ServerCardInfo->PIN = PIN;
	FBankAccount* Account = FindAccount(CardInfo);
	if (Account)
	{
		FTextFormat Format = FTextFormat::FromString(FBankActions::PINHasBeenChanged);
		FFormatNamedArguments StreamerArgs = {{"Card", FText::FromString(FormatCardNumber(CardInfo.CardNumber))},
				{"Bank", FText::FromString(Device->GetBankIssuer().LoadSynchronous()->GetBankName())}
		};
		SendMessage(Account->PhoneNumber, FText::Format(Format, StreamerArgs).ToString());
	}
		
	return EResponse::Success;
	
	
}

EResponse UProcessingCenter::GenerateAndSendOTP(const ABankTerminal* Terminal, const int64 PhoneNumber)
{
	if (FBankAccount* Account = FindAccount(Terminal->GetBankIssuer(), PhoneNumber))
	{
		int32 Code = FMath::RandRange(1,9999);
		Account->CodeOTP = Code;
		FString OTP = FString::FromInt(Code);
		FString ToAppendTo = FString::ChrN(4 - OTP.Len(), '0');
		const FTextFormat Format = FTextFormat::FromString(FOTPMessages::NewOTP);
		const FFormatNamedArguments StreamerArgs = {
			{"Code", FText::FromString(ToAppendTo.Append(OTP))},
		};
		SendMessage(Account->PhoneNumber, FText::Format(Format, StreamerArgs).ToString());
		return EResponse::Success;
	}
	return EResponse::InvalidPhoneNumber;
}

TArray<FCardInfo> UProcessingCenter::GetAllCards(const ABankTerminal* Terminal, int64 PhoneNumber)
{
	if (FBankAccount* Account = FindAccount(Terminal->GetBankIssuer(), PhoneNumber))
	{
		TArray<FCardInfo> Array;
		for (const FServerCardInfo& ServerCardInfo : Account->Cards)
		{
			Array.Add(ServerCardInfo.PublicCard);
		}
		return Array;
	}
	return TArray<FCardInfo>();
}

bool UProcessingCenter::IsOTPValid(const ABankTerminal* Terminal, int64 PhoneNumber, int32 OTP)
{
	FBankAccount* Account = FindAccount(Terminal->GetBankIssuer(), PhoneNumber);
	if (Account)
	{
		return Account->CodeOTP == OTP;
	}
	return false;
}

EResponse UProcessingCenter::SetBlockedState(const ABankTerminal* Terminal, const FCardInfo& CardInfo, bool bToBlock)
{
	
	if (FServerCardInfo* ServerCardInfo = FindServerCard(CardInfo))
	{
		ServerCardInfo->bBlocked = bToBlock;
		if (!bToBlock) 
		{
		    ServerCardInfo->TriesLeft = 3;
		}
		FBankAccount* Account = FindAccount(CardInfo);
		if (Account)
		{
			FString Message;
			FFormatNamedArguments StreamerArgs = {
				{"Card", FText::FromString(FormatCardNumber(CardInfo.CardNumber))},
				{"Bank", FText::FromString(Terminal->GetBankIssuer().LoadSynchronous()->GetBankName())}
			};
			if (bToBlock)
			{
				Message = FBankActions::CardHasBeenBlocked;
			}
			else
			{
				Message = FBankActions::CardHasBeenUnblocked;
			}
			SendMessage(Account->PhoneNumber, Message, StreamerArgs);
		}
		return EResponse::Success;
	}
	return EResponse::UnknownCard;
}

TArray<FTransactionInfo> UProcessingCenter::GetTransactions(const FCardInfo& CardInfo)
{
	FServerCardInfo* ServerCard = FindServerCard(CardInfo);
	return ServerCard ? ServerCard->Transactions : TArray<FTransactionInfo>();
}

FDelegateHandle UProcessingCenter::RegisterPhone(int64 PhoneNumber, UPhoneComponent* Phone, PhoneFunc Handler)
{
	check(Phone);
	FPhoneMessageCreated& Delegate = Subscribers.FindOrAdd(PhoneNumber);
	const FDelegateHandle Handle = Delegate.AddUObject(Phone, Handler);

	UE_LOG(Log_Processing, Log, TEXT("Phone %lld registered!"), PhoneNumber);
	return Handle;
}

void UProcessingCenter::UnregisterPhone(int64 PhoneNumber, FDelegateHandle Handle)
{
	FPhoneMessageCreated* Delegate = Subscribers.Find(PhoneNumber);
	
	if (Delegate)
	{
		// Remove subscriber
		Delegate->Remove(Handle);

		// If no other subscriber, delete delegate
		if (!Delegate->IsBound())
		{
			Subscribers.FindAndRemoveChecked(PhoneNumber);
		}
		
		UE_LOG(Log_Processing, Log, TEXT("Phone %lld unregistered!"), PhoneNumber);
	}
}

const FBankAccount& UProcessingCenter::RegisterNewAccount(TSoftObjectPtr<const UBankInfo> Bank, const FBankAccount& Account)
{
	FBankData& BankData = Banks.FindChecked(Bank);
	FBankAccount& AddedAccount = BankData.Accounts.Add_GetRef(Account);
	
	// Ensure that we don't add "cheated" cards to the account;
	AddedAccount.Cards.Empty();

	return AddedAccount;
}

const FCardInfo& UProcessingCenter::RegisterNewCard(TSoftObjectPtr<const UBankInfo> Bank, const FBankAccount& Account, ECurrency Currency)
{
	// Validate existence of Bank and Account in this bank
	FBankData& BankData = Banks.FindChecked(Bank);
	const int32 Idx = BankData.Accounts.Find(Account);
	check(Idx != INDEX_NONE);

	FServerCardInfo ServerCard;
	ServerCard.PublicCard.BankIssuer = Bank;
	ServerCard.PublicCard.Currency = Currency;
	ServerCard.PublicCard.CardNumber = FString::Printf(TEXT("%lli"), GenerateUniqueCardNumber());

	ACoreGameState* GameState = GetWorld()->GetGameState<ACoreGameState>();
	check(GameState);

	ServerCard.PublicCard.CardClass = GameState->GetPickupCardClass();
	ServerCard.PublicCard.CardWidgetClass = GameState->GetRandomCardWidgetClass();

	// Initialize expire date (current date + offset)
	const FDateTime NowTime = FDateTime::Now();
	constexpr int32 MonthOffset = 0;
	ServerCard.PublicCard.ExpMonth = NowTime.GetMonth() + MonthOffset;
	constexpr int32 YearOffset = 4;
	ServerCard.PublicCard.ExpYear = NowTime.GetYear() + YearOffset;

	const FServerCardInfo& NewCard = BankData.Accounts[Idx].Cards.Add_GetRef(ServerCard);
	UE_LOG(Log_Processing, Log, TEXT("Client %s received their card %s"),
		*BankData.Accounts[Idx].ClientName, *NewCard.PublicCard.CardNumber);

	return NewCard.PublicCard;
}

FBankAccount* UProcessingCenter::FindAccount(const FCardInfo& PublicCard)
{
	// Find Array of bank accounts for card issuer bank
	FBankData* BankData = Banks.Find(PublicCard.BankIssuer);
	if (!BankData)
	{
		UE_LOG(Log_Processing, Warning, TEXT("FindAccount with card from unknown bank!"));
		return nullptr;
	}

	return BankData->Accounts.FindByPredicate(
		[PublicCard](const FBankAccount& Account) -> bool
		{
			// Check whether Account contains PublicCard
			return Account.Cards.FindByPredicate(
				[PublicCard](const FServerCardInfo& ServerCard) -> bool
				{
					return ServerCard.PublicCard == PublicCard;
				}) != nullptr;
		});
}

FBankAccount* UProcessingCenter::FindAccount(int64 PhoneNumber)
{
	// Iterate through all accounts of all banks to find by PhoneNumber
	for (auto& Pair : Banks)
	{
		for (FBankAccount& Account : Pair.Value.Accounts)
		{
			if (Account.PhoneNumber == PhoneNumber)
			{
				return &Account;
			}
		}
	}

	// Nothing found
	return nullptr;
}

FCardInfo* UProcessingCenter::FindCardByNumber(const FString& CardNumber)
{
	// Iterate through all accounts of all banks to find by PhoneNumber
	for (auto& Pair : Banks)
	{
		for (FBankAccount& Account : Pair.Value.Accounts)
		{
			for (FServerCardInfo& Card : Account.Cards)
			{
				if (Card.PublicCard.CardNumber == CardNumber)
				{
					return &(Card.PublicCard);
				}
			}
		}
	}

	// Nothing found
	return nullptr;
}

FServerCardInfo* UProcessingCenter::FindServerCardByNumber(const FString& CardNumber)
{
	
	// Iterate through all accounts of all banks to find by PhoneNumber
	for (auto& Pair : Banks)
	{
		for (FBankAccount& Account : Pair.Value.Accounts)
		{
			for (FServerCardInfo& Card : Account.Cards)
			{
				if (Card.PublicCard.CardNumber == CardNumber)
				{
					return &Card;
				}
			}
		}
	}

	// Nothing found
	return nullptr;
}


FBankAccount* UProcessingCenter::FindAccount(TSoftObjectPtr<const UBankInfo> Bank, int64 PhoneNumber)
{
	if (FBankData* Data = Banks.Find(Bank))
	{
		return Data->Accounts.FindByPredicate(
		[PhoneNumber](const FBankAccount& Account) -> bool
		{
			return (Account.PhoneNumber == PhoneNumber);
		}
		);
	}
	
	return nullptr;
}


FServerCardInfo* UProcessingCenter::FindServerCard(const FCardInfo& PublicCard)
{
	FBankAccount* Account = FindAccount(PublicCard);
	if (!Account)
	{
		UE_LOG(Log_Processing, Warning, TEXT("FindServerCard with unregistered card!"));
		return nullptr;
	}

	return Account->Cards.FindByPredicate(
		[PublicCard](const FServerCardInfo& ServerCard) -> bool
		{
			return ServerCard.PublicCard == PublicCard;
		});
}

FServerCardInfo* UProcessingCenter::FindServerCard(const FString& CardNumber)
{
	// Iterate through all accounts of all banks to find by PhoneNumber
	for (auto& Pair : Banks)
	{
		for (FBankAccount& Account : Pair.Value.Accounts)
		{
			for (FServerCardInfo& Card : Account.Cards)
			{
				if (Card.PublicCard.CardNumber == CardNumber)
				{
					return &Card;
				}
			}
		}
	}

	// Nothing found
	return nullptr;
}

int64* UProcessingCenter::GetPhoneNumber(const FCardInfo& Card)
{
	FBankAccount* Account = FindAccount(Card);
	if (Account)
	{
		return &Account->PhoneNumber;
	}

	return nullptr;
}

void UProcessingCenter::AddFundsToCard(FServerCardInfo& ServerCard, ECurrency Currency, int32 Sum)
{
	const FCardInfo& PublicCard = ServerCard.PublicCard;
	const int32 SumInCardCurrency = UBankInfo::ConvertCurrency(PublicCard.BankIssuer, Currency, PublicCard.Currency, Sum);
	ServerCard.Balance += SumInCardCurrency;
}

void UProcessingCenter::AddFundsToCard(const FCardInfo& PublicCard, ECurrency Currency, int32 Sum)
{
	const int32 SumInCardCurrency = UBankInfo::ConvertCurrency(PublicCard.BankIssuer, Currency, PublicCard.Currency, Sum);
	if (FServerCardInfo* ServerCard = FindServerCard(PublicCard))
	{
		ServerCard->Balance += SumInCardCurrency;
	}
	else
	{
		UE_LOG(Log_Processing, Error, TEXT("Can't find Server card from Public Card %s"), *PublicCard.CardNumber);
	}
}

EResponse UProcessingCenter::RemoveFundsFromCard(FServerCardInfo& ServerCard, ECurrency Currency, int32 Sum)
{
	const FCardInfo& PublicCard = ServerCard.PublicCard;
	const int32 SumInCardCurrency = UBankInfo::ConvertCurrency(PublicCard.BankIssuer, Currency, PublicCard.Currency, Sum);
	if (ServerCard.bBlocked)
	{
		return EResponse::Blocked;
	}
	if (ServerCard.Balance < SumInCardCurrency)
	{
		return EResponse::InsufficientFunds;
	}
	
	ServerCard.Balance -= SumInCardCurrency;
	return EResponse::Success;
}

EResponse UProcessingCenter::RemoveFundsFromCard(const FCardInfo& PublicCard, ECurrency Currency, int32 Sum)
{
	const int32 SumInCardCurrency = UBankInfo::ConvertCurrency(PublicCard.BankIssuer, Currency, PublicCard.Currency, Sum);

	if (FServerCardInfo* ServerCard = FindServerCard(PublicCard))
	{
		if (ServerCard->bBlocked)
		{
			return EResponse::Blocked;
		}
		
		if (ServerCard->Balance >= SumInCardCurrency)
		{
			ServerCard->Balance -= SumInCardCurrency;
			return EResponse::Success;
		}
		return EResponse::InsufficientFunds;
	}
	UE_LOG(Log_Processing, Error, TEXT("Can't find Server card from Public Card %s"), *PublicCard.CardNumber);
	return EResponse::UnknownCard;
}

void UProcessingCenter::SendMessage(int64 Phone, const FString& Message)
{
	FPhoneMessageCreated* Delegate = Subscribers.Find(Phone);
	if (Delegate)
	{
		ensureAlwaysMsgf(Delegate->IsBound(), TEXT("Occured delegate, that is not bound to anything"));
		Delegate->Broadcast(Message);
	}
}

void UProcessingCenter::SendMessage(int64 Phone, const FString& FormatString, const FFormatNamedArguments& FormatArgs)
{
	const FTextFormat Format = FTextFormat::FromString(FormatString);
	const FText Text = FText::Format(Format, FormatArgs);
	
	SendMessage(Phone, Text.ToString());
}

FString UProcessingCenter::FormatCardNumber(const FString& CardNumber)
{
	FString Result;
	for (int32 i = 0; i < 4; ++i)
	{
		Result.AppendChar(' ').Append(CardNumber.Mid(4 * i, 4));
	}
	Result.TrimStartInline();
	return Result;
}

int64 UProcessingCenter::GenerateUniqueCardNumber()
{
	constexpr int32 MaxNumOfTries = 1000;
	for (int32 i = 0; i < MaxNumOfTries; ++i)
	{
		// Valid card numbers: from 1000 0000 0000 0000 to 9999 9999 9999 9999
		int64 PotentialNumber = FMath::RandRange(1000, 9999); // first digit should not be 0
		for (int32 j = 0; j < 3; ++j)
		{
			PotentialNumber *= 10000;
			PotentialNumber += FMath::RandRange(0000, 9999);
		}

		if (!UsedCardNumbers.Contains(PotentialNumber))
		{
			UsedCardNumbers.Add(PotentialNumber);
			return PotentialNumber;
		}
	}
	
	// Yes-yes, this algorithm doesn't guarantee that a valid card number will be generated.
	// However, Chance of it is very-very-very small and this gonna never happen in reality.
	UE_LOG(Log_Processing, Fatal,
		TEXT("If you see this, you are very lucky! Contact kyryl.sydorov@ukma.edu.ua to get your prize: 20 000 UAH"));
	return -1;
}

#if !UE_BUILD_SHIPPING
void UProcessingCenter::DebugAddCents(const FString& CardNumber, int32 Cents)
{
	if (FServerCardInfo* ServerCard = FindServerCard(CardNumber))
	{
		if (FBankAccount* Account = FindAccount(ServerCard->PublicCard))
		{
			AddFundsToCard(*ServerCard, ECurrency::Dollar, Cents);
			const FString Message = FString::Printf(TEXT("CHEATER?! Anyway, your card %s balance increased by %f dollars."),
				*CardNumber, Cents / 100.f);
			
			SendMessage(Account->PhoneNumber, Message);
		}
	}
}
#endif
