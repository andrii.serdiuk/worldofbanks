﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#pragma once

#include "Response.generated.h"

/* Enumeration class that represents the response from the server to the client`s query.
 * Success: The operation was successful.
 * InsufficientFunds: The client`s card does not have enough funds to perform the operation.
 * Blocked: The client tried to use a blocked card;
 * HasBeenJustBlocked: The client`s card was not blocked and there was only one attempt left to authenticate with the correct PIN, which was unsuccessful. When the client
 * receives this response, the mini-game begins.
 * InvalidPin: Authentication was unsuccessful due to an invalid input, but there are some attempts left.
 * UnknownDestination: The user tried to transfer money to an unknown destination
 * DestinationNotAllowedTransaction:
 * TransactionNotAllowed: The card`s bank and the device`s bank do not support transactions
 * ATMImpossibleExchange: The ATM cannot make change for the specified sum.
 * InvalidPhoneNumber: The client specified a phone number which does not exist.
 * InvalidOTP: The client used a wrong OTP.
 * Failure: A failure occurred during transaction which can happen due to invalid data.
 * UnknownCard:The card is not registered in the system.
 */
UENUM()
enum class EResponse : uint8
{
	Success,
	InsufficientFunds,
	Blocked,
	HasBeenJustBlocked,
	InvalidPIN,
	UnknownDestination,
	DestinationNotAllowedTransaction,
	UnknownCard,
	TransactionNotAllowed,
	ATMImpossibleExchange,
	InvalidPhoneNumber,
	InvalidOTP,
	Failure
};

inline FString ResponseToExplanation(EResponse Response)
{
	switch (Response)
	{
	case EResponse::Success:
		return "Operation was successful.";
	case EResponse::InsufficientFunds:
		return "Not enough funds to perform this operation.";
	case EResponse::Blocked:
		return "The card is blocked.";
	case EResponse::HasBeenJustBlocked:
		return "The card has been blocked.";
	case EResponse::InvalidPIN:
		return "Invalid PIN.";
	case EResponse::UnknownDestination:
		return "The specified account doesn't exist.";
	case EResponse::DestinationNotAllowedTransaction:
		return "Sorry, but the destination card`s bank and the terminal`s bank don't support any transactions.";
	case EResponse::UnknownCard:
		return "Your card is not registered in the system. Please, contact the bank manager.";
	case EResponse::TransactionNotAllowed:
		return "Sorry, but our bank doesn't even want to know about the existence of your bank.";
	case EResponse::ATMImpossibleExchange:
		return "This ATM cannot make change for the specified sum.";
	case EResponse::InvalidPhoneNumber:
		return "Couldn't find the bank account connected to this phone number.";
	case EResponse::InvalidOTP:
		return "Wrong OTP";
	case EResponse::Failure:
		return "There was something wrong on the server, try again.";
	default:
		return "Unknown response.";
	}
}