﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "CardInfo.h"

bool operator==(const FCardInfo& Lhs, const FCardInfo& Rhs)
{
	return Lhs.CardNumber == Rhs.CardNumber;
}

bool operator!=(const FCardInfo& Lhs, const FCardInfo& Rhs)
{
	return !(Lhs == Rhs);
}

const FCardInfo FCardInfo::InvalidCard = FCardInfo({""});