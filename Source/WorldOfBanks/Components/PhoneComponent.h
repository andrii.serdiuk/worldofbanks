// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhoneComponent.generated.h"

struct FPlayerPhoneSaveData;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class WORLDOFBANKS_API UPhoneComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPhoneComponent(const FObjectInitializer& ObjectInitializer);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	FORCEINLINE const TArray<FString>& GetMessages() const { return Messages; }
	
	FORCEINLINE int64 GetPhoneNumber() const { return PhoneNumber; }

	/** Load UPhoneComponent from FPlayerPhoneSaveData */
	UPhoneComponent& operator<<(const FPlayerPhoneSaveData& SaveData);

	/** Load UPhoneComponent to FPlayerPhoneSaveData */
	const UPhoneComponent& operator>>(FPlayerPhoneSaveData& SaveData) const;
	
protected:
	/** Called when component dies or game ends */
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	UPROPERTY(Replicated)
	int64 PhoneNumber = 380671234567;

	void OnMessageReceived(const FString& Message);
	
	UFUNCTION()
	void OnRep_Messages();

	UPROPERTY(ReplicatedUsing=OnRep_Messages)
	TArray<FString> Messages;

	/** Used to unbind OnMessageReceived from message delegate in UProcessingCenter */
	FDelegateHandle MessageHandle;
};