﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "DeviceStateful.generated.h"

class UWidgetComponent;
struct FBankStateContainer;
// This class does not need to be modified.
UINTERFACE()
class UDeviceStateful : public UInterface
{
	GENERATED_BODY()
};

/**
 * Used primarily to get nested state containers if there are any.
 * Example: POS has state container, one of its states is Login, which itself has state container.
 */
class WORLDOFBANKS_API IDeviceStateful
{
	GENERATED_BODY()

public:

	virtual FBankStateContainer* GetStateContainer() { return nullptr; }
	virtual UWidgetComponent* GetWidgetComponent() { return nullptr; }
};
