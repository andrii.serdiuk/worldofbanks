﻿#pragma once
#include "BankDeviceBehaviorComponent.h"
#include "BankDeviceTransientBehaviors.generated.h"

// Behaviors that have no logic and have only one purpose -- serve as state, identifier
// so that corresponding widget will be shown

// think of these guys as enum values that can be reused in different enums
// e.g. UBankDeviceCardBlockedBehavior class is used as state identifier in all devices

UCLASS(Abstract, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceCardBlockedBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceGetSumBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceGreetingPosBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceInsufficientFundsBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceSuccessBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UBankDeviceMainMenuBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UBankDeviceGreetingATMBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UBankDeviceShowResponseBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UBankDeviceShowMoneyBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UBankDeviceWithdrawBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UBankDeviceTopUpBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UBankDeviceGreetingTerminalBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UBankDeviceTransferMoneyBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };

UCLASS(Abstract, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UBankDevicePOSReceiptBehavior : public UBankDeviceBehaviorComponent {GENERATED_BODY() };