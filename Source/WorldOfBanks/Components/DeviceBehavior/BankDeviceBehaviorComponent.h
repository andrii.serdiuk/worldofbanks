﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "DeviceStateful.h"
#include "Components/ActorComponent.h"
#include "BankDeviceBehaviorComponent.generated.h"

class UWidgetComponent;
struct FBankStateContainer;
class ABankDevice;

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class WORLDOFBANKS_API UBankDeviceBehaviorComponent : public UActorComponent, public IDeviceStateful
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UBankDeviceBehaviorComponent(const FObjectInitializer& Initializer);

	// StateContainer only to get valid widget component which is needed for nested behaviors with their own states
	virtual void Init(ABankDevice* InBankDevice, const FBankStateContainer& InStateContainer);

	virtual FBankStateContainer* GetStateContainer() override;
	virtual UWidgetComponent* GetWidgetComponent() override;
	
protected:

	UPROPERTY(Transient)
	ABankDevice* BankDevice;

	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override;
	
private:

	const FBankStateContainer* ParentStateContainer;
};
