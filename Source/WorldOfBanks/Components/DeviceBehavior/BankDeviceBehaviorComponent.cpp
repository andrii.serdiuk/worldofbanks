﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "BankDeviceBehaviorComponent.h"
#include "WorldOfBanks/Utility/BankStateContainer.h"


// Sets default values for this component's properties
UBankDeviceBehaviorComponent::UBankDeviceBehaviorComponent(const FObjectInitializer& Initializer) : Super(Initializer)
{
	PrimaryComponentTick.bCanEverTick = false;

	// SetNetAddressable();
	// SetIsReplicatedByDefault(true);
}

void UBankDeviceBehaviorComponent::Init(ABankDevice* InBankDevice, const FBankStateContainer& InStateContainer)
{
	BankDevice = InBankDevice;
	ParentStateContainer = &InStateContainer;
}

FBankStateContainer* UBankDeviceBehaviorComponent::GetStateContainer()
{
	return nullptr;
}

UWidgetComponent* UBankDeviceBehaviorComponent::GetWidgetComponent()
{
	return ParentStateContainer->GetWidgetComponent();
}

void UBankDeviceBehaviorComponent::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	if (FBankStateContainer* Container = GetStateContainer())
	{
		Container->Reset();
	}

	Super::OnComponentDestroyed(bDestroyingHierarchy);
}
