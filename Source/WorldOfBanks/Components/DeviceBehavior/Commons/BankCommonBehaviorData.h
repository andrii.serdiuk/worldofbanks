﻿#pragma once

#include "BankCommonBehaviorDelegates.h"
#include "BankCommonBehaviorData.generated.h"

// Widget mirror entities in order to replicate data

USTRUCT()
struct FBehaviorInput
{
	GENERATED_BODY()
	
public:

	FOnInputChangedDelegate OnValueChanged;
	FOnInputCommitedDelegate OnValueCommited;
	
	void SetValue(const FText& NewValue);
	FText GetValue() const;
	void Commit(ETextCommit::Type CommitType);

private:
	
	FText Value;
};


USTRUCT()
struct FBehaviorOutput
{
	GENERATED_BODY()
	
public:

	FOnMessageChangedDelegate OnChanged;
	void SetValue(const FText& NewValue);
	FText GetValue() const;

protected:

	UPROPERTY(EditDefaultsOnly)
	FText Value;
};


USTRUCT()
struct FBehaviorAction
{
	GENERATED_BODY()

public:

	FOnActionTriggeredDelegate OnTriggered;
	void Trigger();
};