﻿#pragma once

#include <WorldOfBanks/Bank/Response.h>

#include "CoreMinimal.h"

DECLARE_MULTICAST_DELEGATE(FOnActionTriggeredDelegate);
DECLARE_MULTICAST_DELEGATE(FOnMessageChangedDelegate);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnInputCommitedDelegate, ETextCommit::Type);
DECLARE_MULTICAST_DELEGATE(FOnInputChangedDelegate);

DECLARE_MULTICAST_DELEGATE_OneParam(FOnResponseReceivedDelegate, EResponse);