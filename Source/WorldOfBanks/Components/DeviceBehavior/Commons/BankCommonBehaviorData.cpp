﻿#include "BankCommonBehaviorData.h"

void FBehaviorInput::SetValue(const FText& NewValue)
{
	Value = NewValue; OnValueChanged.Broadcast();
}

FText FBehaviorInput::GetValue() const
{
	return Value;
}

void FBehaviorInput::Commit(ETextCommit::Type CommitType)
{
	OnValueCommited.Broadcast(CommitType);
}

void FBehaviorOutput::SetValue(const FText& NewValue)
{
	Value = NewValue; OnChanged.Broadcast();
}

FText FBehaviorOutput::GetValue() const
{
	return Value;
}

void FBehaviorAction::Trigger()
{
	OnTriggered.Broadcast();
}
