﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "BankDeviceBehaviorComponent.h"
#include "BankDeviceChangePinBehavior.generated.h"


UCLASS()
class WORLDOFBANKS_API UBankDeviceChangePinBehavior : public UBankDeviceBehaviorComponent
{
	GENERATED_BODY()

};
