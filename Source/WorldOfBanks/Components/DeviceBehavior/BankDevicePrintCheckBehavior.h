﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "BankDeviceBehaviorComponent.h"
#include "BankDevicePrintCheckBehavior.generated.h"


UCLASS()
class WORLDOFBANKS_API UBankDevicePrintCheckBehavior : public UBankDeviceBehaviorComponent
{
	GENERATED_BODY()

public:

	// TODO: backend
	FString GetCheck() const { return "ДЯКУЮ ЗА ВІДНОШЕННЯ, ТРИМАЙТЕ НЕЗАРАХ"; }
};
