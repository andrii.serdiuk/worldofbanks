﻿#pragma once
#include "WorldOfBanks/Bank/Response.h"

UENUM(BlueprintType)
enum class ECardBlockAfterLoginReason : uint8
{
	CardIsAlreadyBlocked,
	NoAttemptsLeft,
};

UENUM(BlueprintType)
enum class ELoginAbortReason : uint8
{
	CardEjected,
	CancelledByUser,
};

using FLoginFailureReason = TVariant<EResponse, ELoginAbortReason>;

DECLARE_MULTICAST_DELEGATE(FOnLoginActionDelegate)
DECLARE_DELEGATE_OneParam(FOnLoginFinishedWithFailureDelegate, FLoginFailureReason)
DECLARE_MULTICAST_DELEGATE_OneParam(FOnLoginAttemptDelegate, bool)

