﻿#include "BankDeviceLoginSubBehaviors.h"

#include "WorldOfBanks/Devices/BankDevice.h"
#include "WorldOfBanks/WorldOfBanks.h"

void UBankDeviceWaitingForPinBehavior::Init(ABankDevice* InBankDevice, const FBankStateContainer& InStateContainer)
{
	Super::Init(InBankDevice, InStateContainer);
	BankDevice->OnLoginAttemptClient.AddUObject(this, &UBankDeviceWaitingForPinBehavior::OnLoginAttemptResultReceived);
}

void UBankDeviceWaitingForPinBehavior::TryLogin(int32 PIN)
{
	BankDevice->ServerLoginAttempt(PIN);
}

void UBankDeviceWaitingForPinBehavior::OnLoginAttemptResultReceived(EResponse Response)
{
	UE_LOG(Log_Login, Log, TEXT("UBankDeviceWaitingForPinBehavior::OnLoginAttemptResultReceived, Response = %s"), *UEnum::GetValueAsString(Response))
	
	const bool bSuccess = BankDevice->IsAuthorized();

	if (bSuccess)
	{
		OnLoginFinishedWithSuccess.Broadcast();
	}
	else
	{
		FLoginFailureReason Reason;
		Reason.Set<EResponse>(Response);
		OnLoginFinishedWithFailure.Execute(Reason);
	}

    // in order to update attempt counter in gui
    OnLoginAttempt.Broadcast(bSuccess);
}
