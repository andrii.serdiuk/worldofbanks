﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "BankDeviceLoginBehaviorCommons.h"
#include "BankDeviceLoginSubBehaviors.h"
#include "WorldOfBanks/Bank/CardInfo.h"
#include "WorldOfBanks/Utility/BankStateContainer.h"
#include "BankDeviceLoginBehavior.generated.h"

class UBankDeviceLoginBehavior;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class WORLDOFBANKS_API UBankDeviceLoginBehavior : public UBankDeviceBehaviorComponent
{
	GENERATED_BODY()

public:
	
	// Sets default values for this component's properties
	UBankDeviceLoginBehavior(const FObjectInitializer& Initializer);
	
	void SetInsertedCard(); // <-- entry point, entry state, read code starting from this funco

	// TODO: Backend : call it from device itself, but be careful with POS:
	// in POS, device card is expected to be "inserted" all the time, up until we've finished transaction !!
	// TODO: k.sydorov: trigger it somehow
	void ResetInsertedCard();
	void Clear();

	// For the "outer world". This is result of execution of this state
	FOnLoginFinishedWithFailureDelegate OnLoginFinishedWithFailure;
	FOnLoginActionDelegate OnLoginFinishedWithSuccess;
	
	bool IsCardInserted() const { return bCardInserted; }
	
protected:

	virtual void BeginPlay() override;
	virtual FBankStateContainer* GetStateContainer() override;

	void OnCardInserted();
	void OnCardEjected();
	void OnCardInsertionCancelled();

	void OnLoginFinishedWithFailureInternal(FLoginFailureReason Reason);
	void OnLoginFinishedWithSuccessInternal();

	void OnShowFailureResponseSuccess(EResponse Response); // when user sees that card got blocked and clicks "ok"

	template <typename EnumType>
	void FinishWithFailureImmediately(EnumType Reason);
	
	bool bCardInserted = false;

	TOptional<FLoginFailureReason> FailureReason; // for deferred broadcast of failure in case of card blocked

	UPROPERTY()
	FBankStateContainer StateContainer;

	UPROPERTY(EditDefaultsOnly)
	FBankDeviceStateContainerConfig Config;
};

template <typename EnumType>
void UBankDeviceLoginBehavior::FinishWithFailureImmediately(EnumType Reason)
{
	FailureReason = FLoginFailureReason();
	FailureReason.GetValue().Set<EnumType>(Reason);
	StateContainer.SetDeviceState<UBankDeviceWaitingForCardBehavior>();

	OnLoginFinishedWithFailure.Execute(FailureReason.GetValue());
}
