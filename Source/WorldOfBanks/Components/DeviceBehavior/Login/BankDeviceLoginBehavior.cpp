﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "BankDeviceLoginBehavior.h"

#include "Components/EditableText.h"

#include "BankDeviceLoginSubBehaviors.h"
#include "Components/EditableTextBox.h"
#include "Kismet/GameplayStatics.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceTransientBehaviors.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceLoginWidgets.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceBehaviorWidgets.h"
#include "WorldOfBanks/Bank/Response.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceShowResponseWidget.h"

// TODO: Tick disabling duplication in parent class
// Sets default values for this component's properties
UBankDeviceLoginBehavior::UBankDeviceLoginBehavior(const FObjectInitializer& Initializer) : Super(Initializer)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}

void UBankDeviceLoginBehavior::SetInsertedCard()
{
	bCardInserted = true;
	
	OnCardInserted();
}

void UBankDeviceLoginBehavior::OnCardInserted()
{
	if (StateContainer.IsCurrentState<UBankDeviceWaitingForCardBehavior>())
	{
		StateContainer.SetDeviceState<UBankDeviceWaitingForPinBehavior>(); // <-- widget has changed to waiting for pin as well!
	}
}

void UBankDeviceLoginBehavior::ResetInsertedCard()
{
	bCardInserted = false;
	
	OnCardEjected();
}

void UBankDeviceLoginBehavior::Clear()
{
	StateContainer.GetWidgetFor<UBankDeviceWaitingForPinBehavior, UBankDeviceWaitingForPinWidget>()->PINTextBoxWidget->SetText(FText::FromString(""));
}

void UBankDeviceLoginBehavior::BeginPlay()
{
	Super::BeginPlay();

	Config.OwnerPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	Config.BankDevice = BankDevice;
	Config.WidgetComponent = GetWidgetComponent();
	
	StateContainer.Init(this, Config);

	UBankDeviceWaitingForCardWidget* WaitingForCardWidget = StateContainer.GetWidgetFor<UBankDeviceWaitingForCardBehavior, UBankDeviceWaitingForCardWidget>();
	WaitingForCardWidget->OnCancelled.AddUObject(this, &UBankDeviceLoginBehavior::OnCardInsertionCancelled);
	
	UBankDeviceWaitingForPinBehavior* WaitingForPin = StateContainer.GetBehavior<UBankDeviceWaitingForPinBehavior>();
	WaitingForPin->OnLoginFinishedWithFailure.BindUObject(this, &UBankDeviceLoginBehavior::OnLoginFinishedWithFailureInternal);
	WaitingForPin->OnLoginFinishedWithSuccess.AddUObject(this, &UBankDeviceLoginBehavior::OnLoginFinishedWithSuccessInternal);

	StateContainer.GetWidgetFor<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()
		->OnConfirmation.AddUObject(this, &UBankDeviceLoginBehavior::OnShowFailureResponseSuccess);

	StateContainer.SetDeviceState<UBankDeviceWaitingForCardBehavior>();
}

FBankStateContainer* UBankDeviceLoginBehavior::GetStateContainer()
{
	return &StateContainer;
}

void UBankDeviceLoginBehavior::OnCardEjected()
{
	if (StateContainer.IsCurrentState<UBankDeviceWaitingForCardBehavior>())
	{
		FinishWithFailureImmediately(ELoginAbortReason::CardEjected);
	}
	else if (StateContainer.IsCurrentState<UBankDeviceWaitingForPinBehavior>())
	{
		UE_LOG(LogTemp, Warning, TEXT("Trying to UBankDeviceLoginBehavior::OnCardEjected from UBankDeviceWaitingForPinBehavior state. Why are you doing this?"))
	}
}

void UBankDeviceLoginBehavior::OnCardInsertionCancelled()
{
	FinishWithFailureImmediately(ELoginAbortReason::CancelledByUser);
}

void UBankDeviceLoginBehavior::OnLoginFinishedWithFailureInternal(FLoginFailureReason Reason)
{
	if (Reason.IsType<EResponse>())
	{
		const EResponse Response = Reason.Get<EResponse>();
		FailureReason = FLoginFailureReason(); // set value of TOptional
		FailureReason.GetValue().Set<EResponse>(Response);
		
		StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()->ShowResponse(Response);
	}
	else if (Reason.IsType<ELoginAbortReason>())
	{
		check(Reason.Get<ELoginAbortReason>() != ELoginAbortReason::CardEjected) // SubBehaviors are not allowed to eject card
		FinishWithFailureImmediately(Reason.Get<ELoginAbortReason>());
	}
}

void UBankDeviceLoginBehavior::OnLoginFinishedWithSuccessInternal()
{
	// StateContainer.SetDeviceState<UBankDeviceSuccessBehavior>(); // <-- probably redundant, do we want delay here?
	StateContainer.SetDeviceState<UBankDeviceWaitingForCardBehavior>(); // ORDER MATTERS AS HELL, because WidgetComponent is common for all layers of hierarchy

	OnLoginFinishedWithSuccess.Broadcast();
}

void UBankDeviceLoginBehavior::OnShowFailureResponseSuccess(EResponse Response)
{
	check(StateContainer.IsCurrentState<UBankDeviceShowResponseBehavior>())

	if (Response == EResponse::InvalidPIN)
	{
		StateContainer.SetDeviceState<UBankDeviceWaitingForPinBehavior>();
	}
	else
	{
		StateContainer.SetDeviceState<UBankDeviceWaitingForCardBehavior>();
		OnLoginFinishedWithFailure.Execute(FailureReason.GetValue());
	}
}