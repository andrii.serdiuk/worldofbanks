﻿#pragma once
#include <WorldOfBanks/Bank/Response.h>

#include "BankDeviceLoginBehaviorCommons.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceBehaviorComponent.h"
#include "BankDeviceLoginSubBehaviors.generated.h"

class UBankDeviceWaitingForPinBehavior;


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceWaitingForPinBehavior : public UBankDeviceBehaviorComponent
{
	GENERATED_BODY()

public:

	virtual void Init(ABankDevice* InBankDevice, const FBankStateContainer& InStateContainer) override;

	/// Gets called from widget when user presses "login" button
	void TryLogin(int32 PIN);
	
	// for the "outer world". In this case -- for the BankDeviceLoginBehavior
	FOnLoginFinishedWithFailureDelegate OnLoginFinishedWithFailure;
	FOnLoginActionDelegate OnLoginFinishedWithSuccess;
	FOnLoginAttemptDelegate OnLoginAttempt;

protected:

	void OnLoginAttemptResultReceived(EResponse Response);

};


UCLASS(Abstract, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceWaitingForCardBehavior : public UBankDeviceBehaviorComponent { GENERATED_BODY() };