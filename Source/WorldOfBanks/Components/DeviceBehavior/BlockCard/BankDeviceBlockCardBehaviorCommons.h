// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once
#include "WorldOfBanks/Bank/Response.h"

DECLARE_MULTICAST_DELEGATE(FOnBlockCardSuccess)

DECLARE_DELEGATE_OneParam(FOnCardCodeTyped, int32)