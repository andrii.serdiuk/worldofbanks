// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/Bank/Response.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceBehaviorComponent.h"
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/Utility/BankStateContainer.h"
#include "BankDeviceUnlockCardBehavior.generated.h"

struct FCardInfo;

UCLASS()
class WORLDOFBANKS_API UBankDeviceUnlockCardBehavior final: public UBankDeviceBehaviorComponent
{
	GENERATED_BODY()
public:
	void UnlockInsertedCard(int32 Code);
	void ClearState();
	void ReceivedPhone(int64 Phone);
	void ReceivedOTP(int64 Phone);
	void OnFinished(EResponse Response);

	FOnActionTriggeredDelegate OnBackToGreeting;
	FOnActionTriggeredDelegate OnUnlockSuccess;
protected:
	virtual void BeginPlay() override;
	virtual FBankStateContainer* GetStateContainer() override;
	
	//template <typename EnumType>
	//void FinishWithFailureImmediately(EnumType Reason);
	void UnlockingStarted();
	void OnCancelPressed();
	//TOptional<FLoginFailureReason> FailureReason; // for deferred broadcast of failure in case of card blocked

	UPROPERTY()
	FBankStateContainer StateContainer;

	UPROPERTY(EditDefaultsOnly)
	FBankDeviceStateContainerConfig Config;
private:
	int64 Phone;
	void OnShowGenericResponse(EResponse Response);
};