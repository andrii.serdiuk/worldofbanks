// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/Bank/Response.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceBehaviorComponent.h"
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/Utility/BankStateContainer.h"
#include "BankDeviceBlockCardBehavior.generated.h"

struct FCardInfo;

UCLASS()
class WORLDOFBANKS_API UBankDeviceBlockCardBehavior : public UBankDeviceBehaviorComponent
{
	GENERATED_BODY()
public:
	void OnPhoneNotFound(EResponse Response);
	void OnPhoneFound(int64 PhoneIn);

	void OnCodeTypingSuccess(const TArray<FCardInfo>& Cards);
	void OnCodeTypingFailure();

	void CodeTyped(int32 Code);

	void OnBlockedSuccess();

	FOnActionTriggeredDelegate OnBackToGreeting;
protected:
	virtual void BeginPlay() override;
	virtual FBankStateContainer* GetStateContainer() override;

	void BackToGreeting();

	void OnBackToPhoneTyping(EResponse Response);

	//template <typename EnumType>
	//void FinishWithFailureImmediately(EnumType Reason);

	//TOptional<FLoginFailureReason> FailureReason; // for deferred broadcast of failure in case of card blocked

	UPROPERTY()
	FBankStateContainer StateContainer;

	UPROPERTY(EditDefaultsOnly)
	FBankDeviceStateContainerConfig Config;
private:
	int64 Phone;
};