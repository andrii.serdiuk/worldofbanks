// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "BankDeviceUnlockCardBehavior.h"
#include "BankDeviceBlockCardSubBehavior.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceTransientBehaviors.h"
#include "WorldOfBanks/Core/CorePlayerController.h"
#include "WorldOfBanks/Devices/BankTerminal.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceShowResponseWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/Terminal/BankDeviceTerminalBehaviorWidgets.h"
#include "WorldOfBanks/UI/Devices/Behaviors/Terminal/BankDeviceTerminalBlockCardWidgets.h"

struct FCardInfo;

void UBankDeviceUnlockCardBehavior::BeginPlay()
{
	Super::BeginPlay();

	Config.BankDevice = GetOwner<ABankDevice>();

	// We know we are on client, so get first controller 
	Config.OwnerPlayerController = GetWorld()->GetFirstPlayerController();
	Config.WidgetComponent = GetWidgetComponent();

	StateContainer.Init(this, Config);

	//Used from terminal to avoid creating one more class
	UBankDeviceGreetingTerminalWidget* GreetingWidget = StateContainer.GetWidgetFor<UBankDeviceGreetingTerminalBehavior, UBankDeviceGreetingTerminalWidget>();
	GreetingWidget->Message1->SetText(FText::FromString("Want to unlock?"));
	GreetingWidget->Action1Name->SetText(FText::FromString("Yes"));
	GreetingWidget->BlockCardButtonName->SetText(FText::FromString("No"));
	//First button
	GreetingWidget->OnNextPressed.AddUObject(this, &UBankDeviceUnlockCardBehavior::UnlockingStarted);
	//Second button
	GreetingWidget->OnBlockCardPressed.AddUObject(this, &UBankDeviceUnlockCardBehavior::OnCancelPressed);

	UBankDeviceWaitingForCodeWidget* WaitingForCodeWidget = StateContainer.GetWidgetFor<UBankDeviceWaitingForCodeBehavior, UBankDeviceWaitingForCodeWidget>();
	WaitingForCodeWidget->OnCancelled.AddUObject(this, &UBankDeviceUnlockCardBehavior::OnCancelPressed);
	WaitingForCodeWidget->OnCardCodeTyped.BindUObject(this, &UBankDeviceUnlockCardBehavior::UnlockInsertedCard);

	StateContainer.GetWidgetFor<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()
		->OnConfirmation.AddUObject(this, &UBankDeviceUnlockCardBehavior::OnShowGenericResponse);

	StateContainer.SetDeviceState<UBankDeviceGreetingTerminalBehavior>();
}

FBankStateContainer* UBankDeviceUnlockCardBehavior::GetStateContainer()
{
	return &StateContainer;
}

void UBankDeviceUnlockCardBehavior::UnlockInsertedCard(int32 Code)
{
	ABankTerminal* Terminal = Cast<ABankTerminal>(BankDevice);
	Terminal->ServerUnlockInsertedCard(Code);
}

void UBankDeviceUnlockCardBehavior::ClearState()
{
	StateContainer.SetDeviceState<UBankDeviceGreetingTerminalBehavior>();
	StateContainer.GetWidgetFor<UBankDeviceWaitingForCodeBehavior, UBankDeviceWaitingForCodeWidget>()->Form1->SetText(FText::FromString(""));
}

void UBankDeviceUnlockCardBehavior::ReceivedPhone(int64 PhoneIn)
{
	Phone = PhoneIn;

	UBankDeviceWaitingForCodeWidget* WaitingForCodeWidget = StateContainer.GetWidgetFor<UBankDeviceWaitingForCodeBehavior, UBankDeviceWaitingForCodeWidget>();
	WaitingForCodeWidget->Message1->SetText(FText::FromString("OTP was send to the connected number:\n********" + FString::FromInt(Phone / 1e8)));

	ABankTerminal* Terminal = Cast<ABankTerminal>(BankDevice);
	Terminal->ServerGenerateOTP(Phone);
}

void UBankDeviceUnlockCardBehavior::ReceivedOTP(int64 PhoneIn)
{
	check(Phone == PhoneIn);

	StateContainer.SetDeviceState<UBankDeviceWaitingForCodeBehavior>();
}

void UBankDeviceUnlockCardBehavior::OnFinished(EResponse Response)
{
	StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()
				   ->ShowResponse(Response);
}

void UBankDeviceUnlockCardBehavior::UnlockingStarted()
{
	//Get phone connected to the card
	ABankTerminal* Terminal = Cast<ABankTerminal>(BankDevice);
	Terminal->ServerGetCurrentCardPhone();
}

void UBankDeviceUnlockCardBehavior::OnCancelPressed()
{
	ClearState();
	OnBackToGreeting.Broadcast();
}

void UBankDeviceUnlockCardBehavior::OnShowGenericResponse(EResponse Response)
{
	if(Response == EResponse::Success)
	{
		ClearState();
		OnUnlockSuccess.Broadcast();
	}
	else
	{
		OnCancelPressed();	
	}
}
