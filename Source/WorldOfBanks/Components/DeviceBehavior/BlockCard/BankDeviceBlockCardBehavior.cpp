// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "BankDeviceBlockCardBehavior.h"

#include "BankDeviceBlockCardSubBehavior.h"
#include "Components/EditableTextBox.h"
#include "Kismet/GameplayStatics.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceTransientBehaviors.h"
#include "WorldOfBanks/Devices/BankTerminal.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceShowResponseWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/Terminal/BankDeviceTerminalBlockCardWidgets.h"

struct FCardInfo;

void UBankDeviceBlockCardBehavior::BeginPlay()
{
	Super::BeginPlay();

	Config.OwnerPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	Config.BankDevice = BankDevice;
	Config.WidgetComponent = GetWidgetComponent();

	StateContainer.Init(this, Config);

	UBankDeviceWaitingForPhoneWidget* WaitingForPhoneWidget = StateContainer.GetWidgetFor<UBankDeviceWaitingForPhoneBehavior, UBankDeviceWaitingForPhoneWidget>();
	WaitingForPhoneWidget->OnCancelled.AddUObject(this, &UBankDeviceBlockCardBehavior::BackToGreeting);

	UBankDeviceWaitingForCodeWidget* WaitingForCodeWidget = StateContainer.GetWidgetFor<UBankDeviceWaitingForCodeBehavior, UBankDeviceWaitingForCodeWidget>();
	WaitingForCodeWidget->OnCancelled.AddUObject(this, &UBankDeviceBlockCardBehavior::OnBackToPhoneTyping, EResponse::Success);
	WaitingForCodeWidget->OnCardCodeTyped.BindUObject(this, &UBankDeviceBlockCardBehavior::CodeTyped);

	UBankDeviceTerminalChoosingCardWidget* ChoosingCardWidget = StateContainer.GetWidgetFor<UBankDeviceTerminalChoosingCardBehavior, UBankDeviceTerminalChoosingCardWidget>();
	ChoosingCardWidget->OnCancelled.AddUObject(this, &UBankDeviceBlockCardBehavior::BackToGreeting);

	StateContainer.GetWidgetFor<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()
		->OnConfirmation.AddUObject(this, &UBankDeviceBlockCardBehavior::OnBackToPhoneTyping);

	//OnDeviceStateChange won't be calling without this
	StateContainer.OnChanged.AddUObject(WaitingForPhoneWidget, &UBankDeviceWaitingForPhoneWidget::OnDeviceStateChanged);
	StateContainer.OnChanged.AddUObject(WaitingForCodeWidget, &UBankDeviceWaitingForCodeWidget::OnDeviceStateChanged);
	StateContainer.OnChanged.AddUObject(ChoosingCardWidget, &UBankDeviceTerminalChoosingCardWidget::OnDeviceStateChanged);

	StateContainer.SetDeviceState<UBankDeviceWaitingForPhoneBehavior>();
}

FBankStateContainer* UBankDeviceBlockCardBehavior::GetStateContainer()
{
	return &StateContainer;
}

void UBankDeviceBlockCardBehavior::OnPhoneNotFound(EResponse Response)
{
	StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()->ShowResponse(Response);
}

void UBankDeviceBlockCardBehavior::OnPhoneFound(int64 PhoneIn)
{
	Phone = PhoneIn;
	StateContainer.SetDeviceState<UBankDeviceWaitingForCodeBehavior>();
}

void UBankDeviceBlockCardBehavior::OnCodeTypingSuccess(const TArray<FCardInfo>& Cards)
{
	StateContainer.GetWidgetFor<UBankDeviceTerminalChoosingCardBehavior, UBankDeviceTerminalChoosingCardWidget>()->InitList(Cards);
	StateContainer.SetDeviceState<UBankDeviceTerminalChoosingCardBehavior>();
}

void UBankDeviceBlockCardBehavior::OnCodeTypingFailure()
{
	StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()->ShowResponse(EResponse::InvalidOTP);
}

void UBankDeviceBlockCardBehavior::CodeTyped(int32 Code)
{
	ABankTerminal* Terminal = Cast<ABankTerminal>(BankDevice);
	Terminal->ServerGetAllCards(Phone, Code);
}

void UBankDeviceBlockCardBehavior::BackToGreeting()
{
	OnBlockedSuccess();
	OnBackToGreeting.Broadcast();
}

void UBankDeviceBlockCardBehavior::OnBackToPhoneTyping(EResponse Response)
{
	StateContainer.SetDeviceState<UBankDeviceWaitingForPhoneBehavior>();
}

void UBankDeviceBlockCardBehavior::OnBlockedSuccess()
{
	StateContainer.SetDeviceState<UBankDeviceWaitingForPhoneBehavior>();
	StateContainer.GetWidgetFor<UBankDeviceWaitingForPhoneBehavior, UBankDeviceWaitingForPhoneWidget>()->GetPhoneNumberWidget->SetText(FText::FromString(""));
	StateContainer.GetWidgetFor<UBankDeviceWaitingForCodeBehavior, UBankDeviceWaitingForCodeWidget>()->Form1->SetText(FText::FromString(""));
}