// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceBehaviorComponent.h"
#include "BankDeviceBlockCardSubBehavior.generated.h"


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceWaitingForCodeBehavior : public UBankDeviceBehaviorComponent
{
	GENERATED_BODY()
};

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceTerminalChoosingCardBehavior : public UBankDeviceBehaviorComponent
{
	GENERATED_BODY()
};

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UBankDeviceWaitingForPhoneBehavior : public UBankDeviceBehaviorComponent
{
	GENERATED_BODY()
};