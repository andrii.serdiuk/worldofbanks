 // Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WorldOfBanks/Bank/Banknote.h"
#include "WorldOfBanks/Bank/CardInfo.h"
#include "WorldOfBanks/Core/ReplicationUtils.h"

#include "WalletComponent.generated.h"

class APickupCard;
struct FPlayerWalletSaveData;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WORLDOFBANKS_API UWalletComponent : public UActorComponent
{
	GENERATED_BODY()
public:
	// Sets default values
	UWalletComponent(const FObjectInitializer& ObjectInitializer);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	bool AddCard(const FCardInfo& CardInfo);
	bool AddCash(const FBanknote& Banknote, const int32 Amount);

	FORCEINLINE const TArray<FCardInfo>& GetCards() const { return Cards; }
	FORCEINLINE const TArray<FMoneyInfo>& GetMoney() const { return Money; }

	UFUNCTION()
	bool DropCard(const FCardInfo& CardInfo);

	UFUNCTION()
	bool DropCash(const FBanknote& Banknote, const int32 Amount);

	bool RemoveCard(const FCardInfo& CardInfo);
	bool RemoveCash(const FBanknote& Banknote, const int32 Amount);

	TMap<ECurrency, int32> GetCashSum() const;

	/** Load UWalletComponent from FPlayerWalletSaveData */
	UWalletComponent& operator<<(const FPlayerWalletSaveData& SaveData);

	/** Load UWalletComponent to FPlayerWalletSaveData */
	const UWalletComponent& operator>>(FPlayerWalletSaveData& SaveData) const;

private:
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerDropCard(const FCardInfo& CardInfo);
	void ServerDropCard_Implementation(const FCardInfo& CardInfo);
	bool ServerDropCard_Validate(const FCardInfo& CardInfo);
	
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerDropCash(const FBanknote& Banknote, const int32 Amount);
	void ServerDropCash_Implementation(const FBanknote& Banknote, int32 Amount);
	bool ServerDropCash_Validate(const FBanknote& Banknote, int32 Amount);

	//Remove card from wallet(don't spawn)
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerRemoveCard(const FCardInfo& CardInfo);
	void ServerRemoveCard_Implementation(const FCardInfo& CardInfo);
	bool ServerRemoveCard_Validate(const FCardInfo& CardInfo);

	//Remove cash from wallet(don't spawn)
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerRemoveCash(const FBanknote& Banknote, const int32 Amount);
	void ServerRemoveCash_Implementation(const FBanknote& Banknote, int32 Amount);
	bool ServerRemoveCash_Validate(const FBanknote& Banknote, int32 Amount);
	
	UPROPERTY(Replicated)
	TArray<FMoneyInfo> Money;

	UPROPERTY(Replicated)
	TArray<FCardInfo> Cards;

	UPROPERTY(Replicated)
	int32 BanknotesAmount = 0;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxCardsAmount = 4;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxBanknotesAmount = 10;

	UPROPERTY(EditDefaultsOnly)
	float DropDistance = 100.f;

protected:
	// Called when the game starts or when spawned
	void BeginPlay() override;
};