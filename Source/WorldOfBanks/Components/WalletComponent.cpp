// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "WalletComponent.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "Algo/Accumulate.h"
#include "Net/UnrealNetwork.h"
#include "WorldOfBanks/Pickup/PickupCard.h"
#include "WorldOfBanks/Pickup/PickupCash.h"
#include "WorldOfBanks/Core/CoreSaveGame.h"
#include "WorldOfBanks/Core/CoreGameState.h"

UWalletComponent::UWalletComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UWalletComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UWalletComponent, Money);
	DOREPLIFETIME(UWalletComponent, Cards);
	DOREPLIFETIME(UWalletComponent, BanknotesAmount);
}

bool UWalletComponent::AddCard(const FCardInfo& CardInfo)
{
	checkf(!Cards.Contains(CardInfo), TEXT("Character`s wallet already contains this card")); 
	if(Cards.Num() < MaxCardsAmount)
	{
		UE_LOG(Log_Wallet_Pick, Log, TEXT("Character has picked up a new card"));
		Cards.Add(CardInfo);
		return true;
	}
	UE_LOG(Log_Wallet_Pick, Log, TEXT("Character`s wallet containts the maximum amount of cards"));
	return false;
}

bool UWalletComponent::AddCash(const FBanknote& Banknote, const int32 Amount)
{
	if(BanknotesAmount + Amount < MaxBanknotesAmount)
	{
		UE_LOG(Log_Wallet_Pick, Log, TEXT("Character has picked up some cash"));
		FindOrAddByBanknote(Money, Banknote) += Amount;
		
		BanknotesAmount += Amount;
		return true;
	}
	UE_LOG(Log_Wallet_Pick, Log, TEXT("Character`s wallet containts the maximum amount of banknotes"));
	return false;
}

bool UWalletComponent::DropCard(const FCardInfo& CardInfo)
{
	if (ensure(ServerDropCard_Validate(CardInfo)))
	{
		ServerDropCard(CardInfo);
		return true;
	}
	
	return false;
}

void UWalletComponent::ServerDropCard_Implementation(const FCardInfo& CardInfo)
{
	check(Cards.Remove(CardInfo) == 1);

	APawn* PlayerPawn = GetOwner<APlayerController>()->GetPawn();

	const FVector SpawnLocation = PlayerPawn->GetActorLocation() + PlayerPawn->GetActorForwardVector() * DropDistance;
	const TSubclassOf<APickupCard> CardClass = CardInfo.CardClass.LoadSynchronous();
	check(CardClass);
	
	APickupCard* CardToSpawn = GetWorld()->SpawnActorDeferred<APickupCard>(CardClass, FTransform::Identity);
	CardToSpawn->Init(CardInfo);
	const FTransform Transform(SpawnLocation);
	CardToSpawn->FinishSpawning(Transform);
}

bool UWalletComponent::ServerDropCard_Validate(const FCardInfo& CardInfo)
{
	if (Cards.Contains(CardInfo))
	{
		if (APawn* PlayerPawn = GetOwner<APlayerController>()->GetPawn())
		{
			return true;
		}
	}

	return false;
}

bool UWalletComponent::DropCash(const FBanknote& Banknote, const int32 Amount)
{
	// use server validate function as we assume that client has the same data
	if (ensure(ServerDropCash_Validate(Banknote, Amount)))
	{
		ServerDropCash(Banknote, Amount);
		return true;
	}

	return false;
}

void UWalletComponent::ServerDropCash_Implementation(const FBanknote& Banknote, int32 Amount)
{
	int32* CurrentAmount = FindByBanknote(Money, Banknote);
	*CurrentAmount -= Amount;
	APawn* PlayerPawn = GetOwner<APlayerController>()->GetPawn();
	
	const FVector SpawnLocation = PlayerPawn->GetActorLocation() + PlayerPawn->GetActorForwardVector() * DropDistance;

	TSubclassOf<APickupCash> CashClass = GetWorld()->GetGameState<ACoreGameState>()->GetPickupCashClass().LoadSynchronous();
	check(CashClass);
	
	APickupCash* BanknotesToSpawn = GetWorld()->SpawnActorDeferred<APickupCash>(CashClass, FTransform::Identity);
	BanknotesToSpawn->Init(Banknote, Amount);
	FTransform Transform(SpawnLocation);
	Transform.SetScale3D(FVector(1.f));
	BanknotesToSpawn->FinishSpawning(Transform);
	
	if (*CurrentAmount == 0)
	{
		RemoveByBanknote(Money, Banknote);
	}
	
	BanknotesAmount -= Amount;
}

bool UWalletComponent::ServerDropCash_Validate(const FBanknote& Banknote, int32 Amount)
{
	int32* CurrentAmount = FindByBanknote(Money, Banknote);
	if (CurrentAmount && *CurrentAmount >= Amount)
	{
		if (APawn* PlayerPawn = GetOwner<APlayerController>()->GetPawn())
		{
			return true;
		}
	}
	
	return false;
}

bool UWalletComponent::RemoveCard(const FCardInfo& CardInfo)
{
	if (ensure(ServerRemoveCard_Validate(CardInfo)))
	{
		ServerRemoveCard(CardInfo);
		return true;
	}

	return false;
}

bool UWalletComponent::RemoveCash(const FBanknote& Banknote, const int32 Amount)
{
	// use server validate function as we assume that client has the same data
	if (ensure(ServerRemoveCash_Validate(Banknote, Amount)))
	{
		ServerRemoveCash(Banknote, Amount);
		return true;
	}

	return false;
}

void UWalletComponent::ServerRemoveCard_Implementation(const FCardInfo& CardInfo)
{
	check(Cards.Remove(CardInfo) == 1);
}

bool UWalletComponent::ServerRemoveCard_Validate(const FCardInfo& CardInfo)
{
	if (Cards.Contains(CardInfo))
	{
		return true;
	}

	return false;
}

void UWalletComponent::ServerRemoveCash_Implementation(const FBanknote& Banknote, int32 Amount)
{
	int32* CurrentAmount = FindByBanknote(Money, Banknote);
	*CurrentAmount -= Amount;

	if (*CurrentAmount == 0)
	{
		RemoveByBanknote(Money, Banknote);
	}

	BanknotesAmount -= Amount;
}

bool UWalletComponent::ServerRemoveCash_Validate(const FBanknote& Banknote, int32 Amount)
{
	int32* CurrentAmount = FindByBanknote(Money, Banknote);
	if (CurrentAmount && *CurrentAmount >= Amount)
	{
		return true;
	}

	return false;
}

TMap<ECurrency, int32> UWalletComponent::GetCashSum() const
{
	TMap<ECurrency, int32> Res;
	for (const FMoneyInfo& Info : Money)
	{
		Res.FindOrAdd(Info.Key.Currency) += Info.Key.Denomination * Info.Value;
	}
	return Res;
}

UWalletComponent& UWalletComponent::operator<<(const FPlayerWalletSaveData& SaveData)
{
	verifyf(GetWorld()->IsServer(), TEXT("Save logic should be done only on server!"));
	Cards = SaveData.WalletCards;
	Money = SaveData.WalletCash;

	BanknotesAmount = 0;
	for (const FMoneyInfo& Info : Money)
	{
		BanknotesAmount += Info.Value;
	}
	
	return *this;
}

const UWalletComponent& UWalletComponent::operator>>(FPlayerWalletSaveData& SaveData) const
{
	verifyf(GetWorld()->IsServer(), TEXT("Save logic should be done only on server!"));
	SaveData.WalletCards = Cards;
	SaveData.WalletCash = Money;
	
	return *this;
}

void UWalletComponent::BeginPlay()
{
	Super::BeginPlay();
}
