// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "PhoneComponent.h"
#include "Net/UnrealNetwork.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "WorldOfBanks/Bank/ProcessingCenter.h"
#include "WorldOfBanks/Core/CoreGameMode.h"
#include "WorldOfBanks/Core/CoreHUD.h"
#include "WorldOfBanks/Core/CoreSaveGame.h"

UPhoneComponent::UPhoneComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPhoneComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UPhoneComponent, PhoneNumber);
	DOREPLIFETIME(UPhoneComponent, Messages);
}

UPhoneComponent& UPhoneComponent::operator<<(const FPlayerPhoneSaveData& SaveData)
{
	verifyf(GetWorld()->IsServer(), TEXT("Save logic should be done only on server!"));
	PhoneNumber = SaveData.PhoneNumber;
	Messages = SaveData.Messages;

	// Register phone number after loading Save to subscribe to new SMS
	if (ACoreGameMode* GameMode = GetWorld()->GetAuthGameMode<ACoreGameMode>())
	{
		UProcessingCenter* ProcessingCenter = GameMode->GetProcessingCenter();
		checkf(ProcessingCenter, TEXT("Processing Center should always exist, but was not retrieved!"));

		MessageHandle = ProcessingCenter->RegisterPhone(PhoneNumber, this, &UPhoneComponent::OnMessageReceived);
	}
	
	return *this;
}

const UPhoneComponent& UPhoneComponent::operator>>(FPlayerPhoneSaveData& SaveData) const
{
	verifyf(GetWorld()->IsServer(), TEXT("Save logic should be done only on server!"));
	SaveData.PhoneNumber = PhoneNumber;
	SaveData.Messages = Messages;
	return *this;
}

void UPhoneComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (GetWorld()->IsServer())
	{
		if (ACoreGameMode* GameMode = GetWorld()->GetAuthGameMode<ACoreGameMode>())
		{
			UProcessingCenter* ProcessingCenter = GameMode->GetProcessingCenter();
			checkf(ProcessingCenter, TEXT("Processing Center should always exist, but was not retrieved!"));

			ProcessingCenter->UnregisterPhone(PhoneNumber, MessageHandle);
		}
	}
	
	Super::EndPlay(EndPlayReason);
}

void UPhoneComponent::OnMessageReceived(const FString& Message)
{
	UE_LOG(Log_Phone, Log, TEXT("Server - Message received on %llu: %s"), PhoneNumber, *Message)
	Messages.Add(Message);
}

void UPhoneComponent::OnRep_Messages()
{
	if (GetWorld()->IsClient())
	{
		APlayerController* Controller = GetOwner<APlayerController>();
		check(Controller);
		
		ACoreHUD* HUD = Controller->GetHUD<ACoreHUD>();
		check(HUD);

		HUD->UpdatePhoneWidget(Messages);
	}
}
