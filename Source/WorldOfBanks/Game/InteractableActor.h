// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableActor.generated.h"

class UWidgetComponent;

UCLASS(Abstract)
class WORLDOFBANKS_API AInteractableActor : public AActor
{
	GENERATED_BODY()
	
public:
	AInteractableActor(const FObjectInitializer& ObjectInitializer);
	
	bool TryInteractWith(APawn* Interactor);
	
	virtual bool CanInteractWith(APawn* Interactor) const;

protected:
	/** Called on Server */
	virtual void InteractWith(APawn* Interactor);

	/** In order to display interaction button in world space */
	UPROPERTY(EditDefaultsOnly, Category = Interactable)
	UWidgetComponent* InteractionWidgetComponent;
};
