// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "InteractableActor.h"
#include "Components/WidgetComponent.h"

AInteractableActor::AInteractableActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
}

bool AInteractableActor::TryInteractWith(APawn* Interactor)
{
	if (ensureAlwaysMsgf(GetWorld()->IsServer(), TEXT("Interact logic should only be done on Server")))
	{
		if (CanInteractWith(Interactor))
		{		
			InteractWith(Interactor);
			return true;
		}
	}
	return false;
}

bool AInteractableActor::CanInteractWith(APawn* Interactor) const
{
	return true;
}

void AInteractableActor::InteractWith(APawn* Interactor)
{
	ensureAlwaysMsgf(GetWorld()->IsServer(), TEXT("Interact logic should only be done on Server"));
}