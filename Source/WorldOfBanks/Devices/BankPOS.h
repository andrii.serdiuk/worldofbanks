// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

/**	This file contains representation for ABankPOS which inherits ABankDevice. 
* This class allows the users to pay some sum of money using a POS-terminal.
*/

#pragma once

#include "CoreMinimal.h"
#include "BankDevice.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceBehaviorWidgets.h"
#include "BankPOS.generated.h"

UCLASS(Abstract)
class WORLDOFBANKS_API ABankPOS : public ABankDevice
{
	GENERATED_BODY()
	
public:
	
	// Sets default values
	ABankPOS(const FObjectInitializer& ObjectInitializer);
	
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerPay(const int32 Sum, ECurrency Currency);
	bool CanPay() const;
	
	UFUNCTION(Client, Reliable)
	void ClientPay(EResponse Response, const int32 Sum, ECurrency Currency);
	
private:
	
	void ServerPay_Implementation(const int32 Sum, ECurrency Currency);
	
	bool ServerPay_Validate(const int32 Sum, ECurrency Currency);
	
	void ClientPay_Implementation(EResponse Response, const int32 Sum, ECurrency Currency);
	
protected:

	virtual void SubscribeToEventsFromStates() override;
	// Entry point for state machine
	virtual void ClientOnInteract() override;
	
	void OnGreetingSuccess();
	void OnGetSumSuccess();
	void OnGetSumCancelled();
	
	void OnLoginSuccess();
	void OnLoginFailure(FLoginFailureReason Reason);

	void OnResponseSuccess(EResponse Response);

	void OnOkSuccess();

	FCardInfo ReceiverCard;
	
};
