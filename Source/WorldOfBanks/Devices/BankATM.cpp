// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "BankATM.h"
#include "Components/Button.h"
#include "Components/ArrowComponent.h"
#include "Components/TextBlock.h"
#include "WorldOfBanks/Pickup/PickupCard.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceTransientBehaviors.h"
#include "WorldOfBanks/UI/Devices/Behaviors/ATM/BankDeviceATMBehaviorWidgets.h"
#include "WorldOfBanks/UI/Devices/Behaviors/ATM/BankDeviceATMMainMenuWidget.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceChangePinBehavior.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDevicePrintCheckBehavior.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceChangePinWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceShowMoneyWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDevicePrintCheckWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceShowResponseWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceGetMoneyWithButtonsWidget.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceGetMoneyWidget.h"
#include "WorldOfBanks/Core/CoreGameState.h"

ABankATM::ABankATM(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	CashSocket = CreateDefaultSubobject<UArrowComponent>("Cash Socket");
	CashSocket->SetupAttachment(RootComponent);
}

void ABankATM::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld()->IsServer())
	{
		CalculateOptimalSolutions();
	}
}

bool ABankATM::CanWithdrawFunds() const
{
	return true;
}

void ABankATM::ClientOnInteract()
{
	Super::ClientOnInteract();

	if (InsertedCard == FCardInfo::InvalidCard)
	{
		StateContainer.SetDeviceState<UBankDeviceGreetingATMBehavior>();
	}
	else
	{
		GoToMainMenu();
	}
}

void ABankATM::SubscribeToEventsFromStates()
{
	Super::SubscribeToEventsFromStates();

	UBankDeviceGreetingATMWidget* GreetingWidget = StateContainer.GetWidgetFor<UBankDeviceGreetingATMBehavior, UBankDeviceGreetingATMWidget>();
	GreetingWidget->OnNextPressed.AddUObject(this, &ABankATM::OnGreetingSuccess);

	UBankDeviceLoginBehavior* LoginBehavior = StateContainer.GetBehavior<UBankDeviceLoginBehavior>();
	LoginBehavior->OnLoginFinishedWithSuccess.AddUObject(this, &ABankATM::GoToMainMenu);
	LoginBehavior->OnLoginFinishedWithFailure.BindUObject(this, &ABankATM::OnLoginFailure);

	UBankDeviceATMMainMenuWidget* MainMenu = StateContainer.GetWidgetFor<UBankDeviceMainMenuBehavior, UBankDeviceATMMainMenuWidget>();
	const UBankInfo* BankInfo = GetBankIssuer().LoadSynchronous();
	MainMenu->BankName->SetText(FText::FromString(BankInfo->GetBankName()));
	MainMenu->ChangePinButton->OnClicked.AddDynamic(this, &ABankATM::OnGotoChangePinSuccess);
	MainMenu->WithdrawButton->OnClicked.AddDynamic(this, &ABankATM::OnGotoWithdrawSuccess);
	MainMenu->CheckMoneyButton->OnClicked.AddDynamic(this, &ABankATM::OnGotoCheckMoneySuccess);
	MainMenu->PrintCheckButton->OnClicked.AddDynamic(this, &ABankATM::OnGotoPrintCheckSuccess);
	
	UBankDeviceChangePinWidget* ChangePinWidget = StateContainer.GetWidgetFor<UBankDeviceChangePinBehavior, UBankDeviceChangePinWidget>();
	ChangePinWidget->OnGoBack.AddUObject(this, &ABankATM::GoToMainMenu);

	UBankDeviceGetMoneyWithButtonsWidget* WithdrawWidget = StateContainer.GetWidgetFor<UBankDeviceWithdrawBehavior, UBankDeviceGetMoneyWithButtonsWidget>();
	WithdrawWidget->OnSubmit.AddUObject(this, &ABankATM::OnWithdrawSuccess);
	WithdrawWidget->OnGoBack.AddUObject(this, &ABankATM::GoToMainMenu);
	UBankDeviceWithdrawSuccessWidget* WithdrawSuccessWidget = StateContainer.GetWidgetFor<UBankDeviceSuccessBehavior, UBankDeviceWithdrawSuccessWidget>();
	WithdrawSuccessWidget->OnConfirmed.AddUObject(this, &ABankATM::GoToMainMenu);
	
	UBankDevicePrintCheckWidget* PrintCheckWidget = StateContainer.GetWidgetFor<UBankDevicePrintCheckBehavior, UBankDevicePrintCheckWidget>();
	PrintCheckWidget->OnGoBack.AddUObject(this, &ABankATM::GoToMainMenu);

	UBankDeviceShowMoneyWidget* CheckMoneyWidget = StateContainer.GetWidgetFor<UBankDeviceShowMoneyBehavior, UBankDeviceShowMoneyWidget>();
	CheckMoneyWidget->OnGoBack.AddUObject(this, &ABankATM::GoToMainMenu);
	
	UBankDeviceShowResponseWidget* ResponseWidget = StateContainer.GetWidgetFor<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>();
	ResponseWidget->OnConfirmation.AddUObject(this, &ABankATM::OnShowGenericResponseSuccess);
}

void ABankATM::OnGreetingSuccess()
{
	StateContainer.SetDeviceState<UBankDeviceLoginBehavior>();
}

void ABankATM::OnShowGenericResponseSuccess(EResponse Response)
{
	if (IsAuthorized())
	{
		GoToMainMenu();
	}
	else
	{
		StateContainer.SetDeviceState<UBankDeviceGreetingATMBehavior>();
	}
}

void ABankATM::ClientOnCardInserted_Implementation()
{
	Super::ClientOnCardInserted_Implementation();
	
	if (StateContainer.IsCurrentState<UBankDeviceLoginBehavior>())
	{
		if (UBankDeviceLoginBehavior* LoginBehavior = StateContainer.GetBehavior<UBankDeviceLoginBehavior>())
		{
			LoginBehavior->SetInsertedCard();
		}
	}
	else
	{
		ServerCardEject();
	}
}

void ABankATM::OnLoginFailure(FLoginFailureReason Reason)
{
	if (Reason.IsType<EResponse>() && Reason.Get<EResponse>() == EResponse::HasBeenJustBlocked)
	{
		ServerCardEject();
	}
	
	StateContainer.SetDeviceState<UBankDeviceGreetingATMBehavior>();
}

void ABankATM::OnGotoChangePinSuccess()
{
	StateContainer.SetDeviceState<UBankDeviceChangePinBehavior>();
}

void ABankATM::OnGotoCheckMoneySuccess()
{
	StateContainer.SetDeviceState<UBankDeviceShowMoneyBehavior>();
	StateContainer.GetCurrentWidget<UBankDeviceShowMoneyWidget>()->ServerUpdateCall();
}

void ABankATM::OnGotoWithdrawSuccess()
{
	StateContainer.SetDeviceState<UBankDeviceWithdrawBehavior>();
}

void ABankATM::OnGotoPrintCheckSuccess()
{
	StateContainer.SetDeviceState<UBankDevicePrintCheckBehavior>();
	ServerGetTransactions();
}

void ABankATM::OnClientChangePin(EResponse Response)
{
	StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()->ShowResponse(Response);
}

void ABankATM::GoToMainMenu()
{
	StateContainer.SetDeviceState<UBankDeviceMainMenuBehavior>();
}

void ABankATM::OnWithdrawSuccess()
{
	const UBankDeviceGetMoneyWithButtonsWidget* WithdrawWidget = StateContainer.GetWidgetFor<UBankDeviceWithdrawBehavior, UBankDeviceGetMoneyWithButtonsWidget>();
	
	const TOptional<int32> Sum = WithdrawWidget->GetMoneyWidget->GetSum();
	check(Sum.IsSet())
	
	ServerWithdrawFunds(WithdrawWidget->GetMoneyWidget->GetCurrency(), 100 * Sum.GetValue());
}

void ABankATM::ClientWithdrawFunds_Implementation(EResponse Response, ECurrency Currency, int32 Sum)
{
	if (Response == EResponse::Success)
	{
		StateContainer.SetDeviceStateW<UBankDeviceSuccessBehavior, UBankDeviceWithdrawSuccessWidget>()->SetSum(Sum, Currency);
	}
	else
	{
		StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()->ShowResponse(Response);
	}
}

void ABankATM::ClientGetTransactions_Implementation(const TArray<FTransactionInfo>& Transactions)
{
	StateContainer.GetWidgetFor<UBankDevicePrintCheckBehavior, UBankDevicePrintCheckWidget>()->UpdateExtracts(Transactions);
}

void ABankATM::CalculateOptimalSolutions()
{
	TSet<ECurrency> AvailableCurrencies;
	for (const FBanknote& Banknote : AvailableBanknotes)
	{
		AvailableCurrencies.Add(Banknote.Currency);
	}

	struct FOptimalSolutionTemp
	{
		int32 TotalBanknotesAmount;
		TArray<FMoneyInfo> BestApproach;
	};

	for (ECurrency Currency : AvailableCurrencies)
	{
		int32 Minimum = INT_MAX;
		int32 Maximum = 0;

		TArray<int32> AvailableDenominations;
		for (const FBanknote& Banknote : AvailableBanknotes)
		{
			if (Banknote.Currency == Currency)
			{
				int32 Value = Banknote.Denomination / 100;
				AvailableDenominations.Add(Value);
				Minimum = FMath::Min(Minimum, Value);
				Maximum = FMath::Max(Maximum, Value);
			}
		}

		TArray<FOptimalSolutionTemp> Array;
		const int32 LocalSum = BanknotesLimit * Maximum;
		Array.SetNum(LocalSum + 1);

		Array[0].TotalBanknotesAmount = 0;
		for (int32 i = 1; i < LocalSum + 1; i++)
		{
			Array[i].TotalBanknotesAmount = LocalSum + 1;
		}
		
		FBanknote ToAdd{ Currency };

		for (int32 i = Minimum; i < LocalSum + 1; i++)
		{
			for (int32 Value : AvailableDenominations)
			{
				int32 SumLeft = i - Value;
				if (SumLeft >= 0 && Array[SumLeft].TotalBanknotesAmount + 1 < Array[i].TotalBanknotesAmount)
				{
					Array[i].TotalBanknotesAmount = Array[SumLeft].TotalBanknotesAmount + 1;
					Array[i].BestApproach = Array[SumLeft].BestApproach;
					ToAdd.Denomination = 100 * Value;

					// We can't add ToAdd directly, because we'll lose metadata like Actor Class or Texture,
					// So have to find full banknote in AvailableBanknotes
					const int32 BanknoteIndex = AvailableBanknotes.Find(ToAdd);
					++FindOrAddByBanknote(Array[i].BestApproach, AvailableBanknotes[BanknoteIndex]);
				}
			}
		}

		for (int32 i = 0; i < LocalSum + 1; i++)
		{
			FOptimalSolutionTemp Temp = Array[i];
			if (Temp.BestApproach.Num() > 0)
			{
				FBanknoteInfo Value{ Currency, 100 * i };
				FOptimalSolution OptimalSolution{ Temp.BestApproach };
				OptimalSolutions.Add(Value, OptimalSolution);
			}
		}
	}
}

void ABankATM::ServerWithdrawFunds_Implementation(ECurrency Currency, int32 Sum)
{
	checkf(Sum > 0, TEXT("The amount of money to withdraw must be positive"));
	FBanknoteInfo ToRetrieve;
	ToRetrieve.Currency = Currency;
	ToRetrieve.Denomination = Sum;
	FOptimalSolution* OptimalSolution = OptimalSolutions.Find(ToRetrieve);
	if (!OptimalSolution)
	{
		ClientWithdrawFunds(EResponse::ATMImpossibleExchange, Currency, Sum);
		return;
	}
	
	TArray<FMoneyInfo> Solution = OptimalSolution->BestApproach;
	checkf(Solution.Num() > 0, TEXT("If there is a solution, it must contain at least one FMoneyInfo"));

	const EResponse Response = GetProcessingCenter()->WithdrawFunds(this, InsertedCard, Currency, Sum);
	//TODO Remove second condition which is being used for convenience
	if (Response == EResponse::Success) // || Response == EResponse::TransactionNotAllowed)
	{
		TSubclassOf<APickupCash> CashClass = GetWorld()->GetGameState<ACoreGameState>()->GetPickupCashClass().LoadSynchronous();
		checkf(CashClass, TEXT("APickupCash is undefined"));

		for (int32 i = 0; i < Solution.Num(); ++i)
		{
			const FMoneyInfo& MoneyInfo = Solution[i];

			// Spawn different types of banknotes with interval to avoid problems with physics
			FTimerDelegate Delegate;
			Delegate.BindWeakLambda(this, [this, CashClass, MoneyInfo]() -> void
			{
				FTransform Transform = CashSocket->GetComponentTransform();
				Transform.SetScale3D(FVector(1.f));
				APickupCash* CashToSpawn = GetWorld()->SpawnActorDeferred<APickupCash>(CashClass, Transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);
				CashToSpawn->Init(MoneyInfo.Key, MoneyInfo.Value);
				CashToSpawn->FinishSpawning(Transform);
				UE_LOG(Log_BankDevice, Log, TEXT("Cash %s spawned"), *CashToSpawn->GetName());
			});

			FTimerHandle Handle;
			GetWorldTimerManager().SetTimer(Handle, Delegate, (i + 1) * SpawnInterval, false);
		}
	}
	
	ClientWithdrawFunds(Response, Currency, Sum);
}

bool ABankATM::ServerWithdrawFunds_Validate(ECurrency Currency, int32 Sum)
{
	UE_LOG(LogTemp, Warning, TEXT("ServerWithdrawFunds_Validate, IsAuthorized = %d, CanWithdrawFunds = %d"), int32(IsAuthorized()), int32(CanWithdrawFunds()))
	UE_LOG(LogTemp, Warning, TEXT("ABankATM = 0x%x"), this)
	UE_LOG(LogTemp, Warning, TEXT("ABankATM = 0x%x"), this)
	return IsAuthorized() && CanWithdrawFunds() && Sum > 0;
}

void ABankATM::ServerGetTransactions_Implementation()
{
	ClientGetTransactions(GetProcessingCenter()->GetTransactions(InsertedCard));
}

bool ABankATM::ServerGetTransactions_Validate()
{
	return true;
}
