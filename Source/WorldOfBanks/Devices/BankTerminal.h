// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

/**	This file contains class ABankTerminal which inherits ABankDevice.
* This class allows the users to:
 * 1. Add funds to the card.
 * 2. Block or unblock the card.
 * 3. Get current balance of the card.
 * 4. Print the list of all transactions which used this card.
 * 5. Change the PIN of the card.
 * 6. Transfer money to another account.
 */

#pragma once

#include "CoreMinimal.h"
#include "BankDevice.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceTransientBehaviors.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceBehaviorWidgets.h"
#include "BankTerminal.generated.h"

class APickupCard;
struct FBanknote;
struct FCardInfo;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnMoneyPushedDelegate, const FMoneyInfo&)

UCLASS(Abstract)
class WORLDOFBANKS_API ABankTerminal : public ABankDevice
{
	GENERATED_BODY()

public:
	
	// Sets default values
	ABankTerminal(const FObjectInitializer& ObjectInitializer);
	
	UFUNCTION(Server,Reliable, WithValidation)
	void ServerPushMoney(const FMoneyInfo& Money);
	bool CanPushMoney() const;

	FOnMoneyPushedDelegate OnMoneyPushed;
	
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTransferMoney(const FString& DestinationAccount, const int32 Sum, ECurrency Currency);
	bool CanTransferMoney() const;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerGenerateOTP(int64 PhoneNumber);
	bool CanBlockCardGenerateOTP() const;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerGetAllCards(int64 PhoneNumber, int32 OTP);
	bool CanBlockCardVerifyOTP() const;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTopUpFunds();
	bool CanTopUpFunds() const;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerBlockCard(const FCardInfo& CardInfo);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerUnlockInsertedCard(int32 Code);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerGetTransactions();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerGetCurrentCardPhone();

	UFUNCTION(Client, Reliable)
	void ClientGetConnectedPhone(EResponse Response, int64 Phone);

	UFUNCTION(Client, Reliable)
	void ClientBlockCard(EResponse Response);

	UFUNCTION(Client, Reliable)
	void ClientUnlockCard(EResponse Response);

	UFUNCTION(Client, Reliable)
	void ClientGenerateOTP(EResponse Response, int64 PhoneNumber);

	UFUNCTION(Client, Reliable)
	void ClientPushMoney(const FMoneyInfo& AddedMoney);
	
	UFUNCTION(Client, Reliable)
	void ClientTransferMoney(EResponse Response, const FString& Destination, const int32 Sum, ECurrency Currency);
	
	UFUNCTION(Client, Reliable)
	void ClientShowAllCards(const TArray<FCardInfo>& Cards);

	UFUNCTION(Client, Reliable)
	void ClientInvalidOTP();

	UFUNCTION(Client, Reliable)
	void ClientTopUpFunds(EResponse Response);

	UFUNCTION(Client, Reliable)
	void ClientGetTransactions(const TArray<FTransactionInfo>& Extracts);
	
protected:
	
	void ClientOnInteract() override;
	virtual void SubscribeToEventsFromStates() override;

	virtual void ClientOnCardInserted_Implementation() override;

	void OnLoginFailure(FLoginFailureReason Reason);

	void OnPINTyping();
	void OnGreetingSuccess();
	void OnBlockCardSuccess();
	void OnShowGenericResponseSuccess(EResponse Response);

	void BackToGreeting();

	//For main menu
	UFUNCTION()
	void OnGotoChangePinSuccess();
	UFUNCTION()
	void OnGotoCheckMoneySuccess();
	UFUNCTION()
	void OnGotoTopUpSuccess();
	UFUNCTION()
	void OnGotoTransferMoneySuccess();
	UFUNCTION()
	void OnGotoPrintCheckSuccess();

	void GoToMainMenu();

private:
	UPROPERTY(VisibleAnywhere)
	UArrowComponent* CashSocket;

	UPROPERTY()
	TMap<ECurrency, int32> InsertedCash;

	UPROPERTY(EditAnywhere)
	TArray<FBanknote> AcceptableBanknotes;

	//Server functions
	void ServerPushMoney_Implementation(const FMoneyInfo& Money);
	bool ServerPushMoney_Validate(const FMoneyInfo& Money);
	
	void ServerTransferMoney_Implementation(const FString& DestinationAccount, const int32 Sum, ECurrency Currency);
	bool ServerTransferMoney_Validate (const FString& DestinationAccount, const int32 Sum, ECurrency Currency);
	
	void ServerGenerateOTP_Implementation(int64 PhoneNumber);
	bool ServerGenerateOTP_Validate(int64 PhoneNumber);

	void ServerGetAllCards_Implementation(int64 PhoneNumber, int32 OTP);
	bool ServerGetAllCards_Validate(int64 PhoneNumber, int32 OTP);

	void ServerGetTransactions_Implementation();
	bool ServerGetTransactions_Validate();
	
	void ServerTopUpFunds_Implementation();
	bool ServerTopUpFunds_Validate();
	
	virtual void OnClientChangePin(EResponse Response) override;
	
	void ClientTransferMoney_Implementation(EResponse Response, const FString& Destination, const int32 Sum, ECurrency Currency);
	
	void ClientPushMoney_Implementation(const FMoneyInfo& MoneyInfo);

	void ClientGenerateOTP_Implementation(EResponse Response, int64 PhoneNumber);

	void ClientShowAllCards_Implementation(const TArray<FCardInfo>& Cards);

	void ClientInvalidOTP_Implementation();

};
