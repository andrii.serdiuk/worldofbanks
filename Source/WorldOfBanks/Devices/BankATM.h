// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

/**	This file contains class ABankATM which inherits ABankDevice.
 * This class allows the users to:
 * 1. Withdraw money from a card.
 * 2. Get current balance of the card.
 * 3. Print the list of all transactions which used this card.
 * 4. Change the PIN of the card.
 * This class contains additional structure FOptimalSolution which is used for solving the change-making problem.
 */

#pragma once

#include "CoreMinimal.h"
#include "BankDevice.h"
#include "WorldOfBanks/Pickup/PickupCash.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceBehaviorWidgets.h"
#include "BankATM.generated.h"

class APickupCard;
enum class EResponse : uint8;

USTRUCT()
struct FOptimalSolution
{
	GENERATED_BODY()
	TArray<FMoneyInfo> BestApproach;
};

UCLASS(Abstract)
class WORLDOFBANKS_API ABankATM : public ABankDevice
{
	GENERATED_BODY()

public:
	// Sets default values
	ABankATM(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	
	FORCEINLINE const TArray<FBanknote>& GetAvailableBanknotes() const {return AvailableBanknotes;}
	
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerWithdrawFunds(ECurrency Currency, int32 Sum);
	bool CanWithdrawFunds() const;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerGetTransactions();

	UFUNCTION(Client, Reliable)
	void ClientWithdrawFunds(EResponse Response, ECurrency Currency, const int32 Sum);

	UFUNCTION(Client, Reliable)
	void ClientGetTransactions(const TArray<FTransactionInfo>& Extracts);

protected:
	
	virtual void ClientOnInteract() override;
	virtual void SubscribeToEventsFromStates() override;

	void OnGreetingSuccess();
	void OnShowGenericResponseSuccess(EResponse Response);

	virtual void ClientOnCardInserted_Implementation() override;
	
	void OnLoginFailure(FLoginFailureReason Reason);

	//For main menu
	UFUNCTION()
	void OnGotoChangePinSuccess();
	UFUNCTION()
	void OnGotoCheckMoneySuccess();
	UFUNCTION()
	void OnGotoWithdrawSuccess();
	UFUNCTION()
	void OnGotoPrintCheckSuccess();

	virtual void OnClientChangePin(EResponse Response) override;

	void GoToMainMenu();

	void OnWithdrawSuccess();
	
private:
	UPROPERTY(EditDefaultsOnly)
	TArray<FBanknote> AvailableBanknotes;
	
	// Using FBanknote with null Texture and Actor instead of TPair<int32, ECurrency> since we can`t use nested templates in Unreal Engine 4
	/** Stores calculated best solutions for each Sum */
	UPROPERTY(Transient)
	TMap<FBanknoteInfo, FOptimalSolution> OptimalSolutions;
	
	UPROPERTY(VisibleAnywhere)
	UArrowComponent* CashSocket;
	
	void ServerWithdrawFunds_Implementation(ECurrency Currency, int32 Sum);
	bool ServerWithdrawFunds_Validate(ECurrency Currency, int32 Sum);

	void ServerGetTransactions_Implementation();
	bool ServerGetTransactions_Validate();

	void ClientWithdrawFunds_Implementation(EResponse Response, ECurrency Currency, int32 Sum);

	void ClientGetTransactions_Implementation(const TArray<FTransactionInfo>& Transactions);

	UPROPERTY(EditAnywhere)
	int32 BanknotesLimit = 40;

	/** When withdraw optimal solution has more that 1 type of banknote, 
	 *	spawn actors with this interval to avoid collision problems */
	UPROPERTY(EditAnywhere)
	float SpawnInterval = 0.5f;
	
	void CalculateOptimalSolutions();
};