// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "BankDevice.h"
#include "Components/ArrowComponent.h"
#include "Components/TextRenderComponent.h"
#include "WorldOfBanks/Pickup/PickupCard.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "WorldOfBanks/Core/CorePlayerController.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/RotatingMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "WorldOfBanks/Core/CoreGameMode.h"
#include "WorldOfBanks/Core/CoreHUD.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceShowMoneyWidget.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceTransientBehaviors.h"
#include "WorldOfBanks/Components/DeviceBehavior/Login/BankDeviceLoginBehavior.h"
#include "WorldOfBanks/Utility/BankStateContainer.h"

ABankDevice::ABankDevice(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(RootComponent);

	GUIWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("GUIWidgetComponent");
	GUIWidgetComponent->SetupAttachment(Mesh);
	GUIWidgetComponent->SetIsReplicated(false);
	GUIWidgetComponent->SetDrawSize(FVector2D(1000, 750));

	CardSocket = CreateDefaultSubobject<UArrowComponent>("Card Socket");
	CardSocket->SetupAttachment(RootComponent);

	BankName = CreateDefaultSubobject<UTextRenderComponent>("Bank Name");
	BankName->SetupAttachment(RootComponent);
	BankName->SetHorizontalAlignment(EHTA_Center);
	BankName->SetVerticalAlignment(EVRTA_TextCenter);

	URotatingMovementComponent* MovingComponent = CreateDefaultSubobject<URotatingMovementComponent>("Name Rotating");
	MovingComponent->SetUpdatedComponent(BankName);
}

void ABankDevice::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABankDevice, bAuthorized);
	DOREPLIFETIME(ABankDevice, InsertedCard);
}

void ABankDevice::BeginPlay()
{
	Super::BeginPlay();
	if(GetWorld()->IsClient())
	{
		BankName->SetText(FText::FromString(BankIssuer.LoadSynchronous()->GetBankName()));
	}
}

void ABankDevice::ServerLoginAttempt_Implementation(int32 PIN)
{
	const EResponse Response = GetProcessingCenter()->TryAuthenticate(this, InsertedCard, PIN);
	bAuthorized = (Response == EResponse::Success);

	if (Response == EResponse::HasBeenJustBlocked)
	{
		CurrentPlayer->LaunchPinFailedMinigame();
	}

	ClientLoginAttempt(Response);
}

bool ABankDevice::ServerLoginAttempt_Validate(int32 PIN)
{
	return true;
}

void ABankDevice::ServerInsertCard_Implementation(const FCardInfo& Card)
{
	if (InsertedCard == FCardInfo::InvalidCard)
	{
		InsertedCard = Card;
		ClientOnCardInserted();
	}
	else
	{
		FTransform Transform = CardSocket->GetComponentTransform();
		Transform.SetScale3D(FVector(1.f));
		APickupCard* CardToSpawn = GetWorld()->SpawnActorDeferred<APickupCard>(Card.CardClass.LoadSynchronous(), FTransform::Identity);
		CardToSpawn->Init(Card);
		CardToSpawn->FinishSpawning(Transform);
	}
}

bool ABankDevice::ServerInsertCard_Validate(const FCardInfo&)
{
	return true;//CanInsertCard();
}

bool ABankDevice::CanInsertCard() const
{
	return InsertedCard == FCardInfo::InvalidCard;
}

bool ABankDevice::CanGetCurrentBalance() const
{
	return IsAuthorized();
}

bool ABankDevice::ServerCardEject_Validate()
{
	return CanEjectCard();
}

bool ABankDevice::CanEjectCard() const
{
	return true;
}

void ABankDevice::ClientGetCurrentBalance_Implementation(EResponse Response, int32 Value)
{
	switch (Response)
	{
	case EResponse::Success:
		if(UBankDeviceShowMoneyWidget* Widget = StateContainer.GetCurrentWidget<UBankDeviceShowMoneyWidget>())
		{
			Widget->SetMoneyAmount(FString::FromInt(Value), InsertedCard.Currency);
		}
		break;
	case EResponse::Blocked:
		StateContainer.SetDeviceState<UBankDeviceCardBlockedBehavior>();
		break;
	case EResponse::UnknownCard:

		break;
	default:

		break;
	}
}

void ABankDevice::ServerGetCurrentBalance_Implementation()
{
	TPair<EResponse, int32> Pair = GetProcessingCenter()->GetCurrentBalance(InsertedCard);
	ClientGetCurrentBalance(Pair.Key, Pair.Value);
}

void ABankDevice::ClientLoginAttempt_Implementation(EResponse Response)
{
	UE_LOG(Log_BankDevice, Log, TEXT("ClientLoginAttempt, Response = %s"), *UEnum::GetValueAsString(Response));
	bAuthorized = (Response == EResponse::Success);
	OnLoginAttemptClient.Broadcast(Response);
}

void ABankDevice::ServerCardEject_Implementation()
{
	if (InsertedCard == FCardInfo::InvalidCard)
	{
		UE_LOG(Log_BankDevice, Log, TEXT("Trying to eject card when there's no card inserted."))
		return;
	}
		
	const TSubclassOf<APickupCard> CardClass = InsertedCard.CardClass.LoadSynchronous();
	check(CardClass);
	FTransform Transform = CardSocket->GetComponentTransform();
	Transform.SetScale3D(FVector(1.f));
	APickupCard* CardToSpawn = GetWorld()->SpawnActorDeferred<APickupCard>(CardClass, FTransform::Identity);
	CardToSpawn->Init(InsertedCard);
	CardToSpawn->FinishSpawning(Transform);

	UE_LOG(Log_BankDevice, Log, TEXT("Successfuly ejected card %s on server."), *InsertedCard.CardNumber);
	
	// Don't forget to set InsertedCard to InvalidCard
	InsertedCard = FCardInfo::InvalidCard;

	bAuthorized = false;
	
	ClientOnCardEjected();
}

bool ABankDevice::ServerGetCurrentBalance_Validate()
{
	return IsAuthorized() && CanGetCurrentBalance();
}

void ABankDevice::ServerChangePin_Implementation(const int32 NewPin)
{
	ClientChangePin(GetProcessingCenter()->ChangePin(this, InsertedCard, NewPin));
}

bool ABankDevice::ServerChangePin_Validate(const int32 NewPin)
{
	return IsAuthorized() && CanChangePin() && NewPin >= 0 && NewPin <= 9999;
}

bool ABankDevice::CanChangePin() const
{
	return true;
}

void ABankDevice::ClientChangePin_Implementation(EResponse Response)
{
	OnClientChangePin(Response);
}

void ABankDevice::InteractWith(APawn* Interactor)
{
	Super::InteractWith(Interactor);
	
	if (ACorePlayerController* Controller = Interactor->GetController<ACorePlayerController>())
	{
		const bool bIsAlreadyOccupied = GetOwner() != nullptr;
		
		if (!bIsAlreadyOccupied)
		{
			// Set network ownership
			OccupyDevice(Controller);
		
			// Call Client logic of Interaction
			ClientInteract();
		}
		else if (Controller != CurrentPlayer)
		{
			// If device is occupied by other player, show notification about this
			Controller->ClientNotifyAlreadyOccupied();
		}
		else
		{
			// If occupied by this player, release the device
			ReleaseDevice(Controller);
		}
	}
}

void ABankDevice::ClientOnInteract()
{
	UE_LOG(Log_Interactable, Log, TEXT("ABankDevice::ClientOnInteract called!"));

	check(GetWorld()->IsClient())
	
	ACorePlayerController* PlayerController = Cast<ACorePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

	PlayerController->SetInteractionWidgetVisibility(false);
	GUIWidgetComponent->SetVisibility(true);
	PlayerController->GetPawn()->DisableInput(PlayerController); // on client, because bInputEnabled is not replicated

	if(ACoreHUD* CoreHUD = PlayerController->GetHUD<ACoreHUD>())
	{
		CoreHUD->OnStartDeviceInteraction();
	}

	// TODO: Backend : Place camera in front of device screen
	
	Config.OwnerPlayerController = PlayerController;
	Config.BankDevice = this;
	Config.WidgetComponent = GUIWidgetComponent;
	
	StateContainer.Init(this, Config);
	SubscribeToEventsFromStates();
}

void ABankDevice::OccupyDevice(ACorePlayerController* PlayerController)
{
	check(GetWorld()->IsServer());
	ensureAlwaysMsgf(CurrentPlayer == nullptr, TEXT("Trying to occupy device, which is already occupated by other PC"));
	UE_LOG(LogTemp, Warning, TEXT("ABankATM = 0x%x"), this)

	CurrentPlayer = PlayerController;
	SetOwner(PlayerController);

	PlayerController->CurrentDevice = this;
}

void ABankDevice::ReleaseDevice(ACorePlayerController* PlayerController)
{
	check(GetWorld()->IsServer());

	UE_LOG(Log_BankDevice, Log, TEXT("Executing ReleaseDevice on server."))

	ServerCardEject_Implementation();
	
	ClientReleaseDevice(PlayerController);
	
	CurrentPlayer = nullptr;
	SetOwner(nullptr);

	PlayerController->CurrentDevice = nullptr;
}

void ABankDevice::ClientOnReleaseDevice(ACorePlayerController* PlayerController)
{
	UE_LOG(Log_BankDevice, Verbose, TEXT("ClientOnReleaseDevice"));

	PlayerController->SetInteractionWidgetVisibility(true);
	GUIWidgetComponent->SetVisibility(false);
	PlayerController->GetPawn()->EnableInput(PlayerController);

	if (ACoreHUD* CoreHUD = PlayerController->GetHUD<ACoreHUD>())
	{
		CoreHUD->OnEndDeviceInteraction();
	}

	StateContainer.Reset();
}

UProcessingCenter* ABankDevice::GetProcessingCenter() const
{
	checkf(GetWorld()->IsServer(), TEXT("ABankDevice::GetProcessingCenter() should only be called on Server!"));
	
	UProcessingCenter* ProcessingCenter = nullptr;
	
	if (ACoreGameMode* GameMode = GetWorld()->GetAuthGameMode<ACoreGameMode>())
	{
		ProcessingCenter = GameMode->GetProcessingCenter();
	}
	
	verifyf(ProcessingCenter, TEXT("ProcessingCenter should always exist!"));
	return ProcessingCenter;
}

void ABankDevice::ClientOnCardInserted_Implementation()
{
	if (StateContainer.IsCurrentState<UBankDeviceLoginBehavior>())
	{
		if (UBankDeviceLoginBehavior* LoginBehavior = StateContainer.GetBehavior<UBankDeviceLoginBehavior>())
		{
			LoginBehavior->SetInsertedCard();
		}
	}
	else
	{
		ServerCardEject();
	}
}

void ABankDevice::ServerReleaseDevice_Implementation(ACorePlayerController* PlayerController)
{
	ensureAlwaysMsgf(CurrentPlayer == PlayerController, TEXT("Trying to release occupation by other PC"));

	ReleaseDevice(PlayerController);
}

void ABankDevice::ClientInteract_Implementation()
{
	ClientOnInteract();
}

void ABankDevice::ClientReleaseDevice_Implementation(ACorePlayerController* PlayerController)
{
	ClientOnReleaseDevice(PlayerController);
}

