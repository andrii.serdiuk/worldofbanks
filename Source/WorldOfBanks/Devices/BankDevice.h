// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


/** Abstract class for all bank devices which contains functions for authentication, interaction and basic operations.
*/

#pragma once

#include "Components/ArrowComponent.h"
#include "WorldOfBanks/Game/InteractableActor.h"
#include "WorldOfBanks/Bank/ProcessingCenter.h"
#include "WorldOfBanks/Core/ServerPlayerInfo.h"
#include "WorldOfBanks/Utility/BankStateContainer.h"
#include "WorldOfBanks/Utility/BankStateContainerCommons.h"
#include "WorldOfBanks/Bank/Response.h"
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "BankDevice.generated.h"

class UTextRenderComponent;
class UArrowComponent;
class ACorePlayerController;
class UBaseDeviceWidget;
struct FCardInfo;

// 0. We have a state machine over here. It maps current state to corresponding widget automatically.
// 1. There are ~three entities:
//  - Widgets react on input and serve as storage for input. Almost no logic. Delegate stuff to corresponding behaviors
//  - Behavior component -- piece of reusable logic. Should contain primarily RPCs. Its type serves as a state identifier (instead of enum!)
//  - BankDevice -- reacts to events from both widgets and behaviors. ONLY BankDevice is allowed to manage transitions between states.

// 2. Each behavior is defined in C++ so that we can use it as state.
//	  Behavior is defined in Blueprints only if it has subbehaviors (e.g. it has StateContainer in itself)
// 3. There are multiple gui forms in BankDeviceCommonForms.h
// 4. Abstract behaviors shall not be created in the StateContainer.
// 5. RPC must be contained in ABankDevice!!! Behaviors can't contain RPC's because they do not exist on server.

UCLASS(Abstract)
class WORLDOFBANKS_API ABankDevice : public AInteractableActor, public IDeviceStateful
{
	GENERATED_BODY()

public:
	// Sets default values
	ABankDevice(const FObjectInitializer& ObjectInitializer);
	
	FORCEINLINE FBankStateContainer& GetState() { return StateContainer; }

	FORCEINLINE ACorePlayerController* GetCurrentPlayer() const { return CurrentPlayer; }

	FORCEINLINE const FCardInfo& GetInsertedCard() const { return InsertedCard; }

	/** Returns Transform of the Socket (place, where Card will be spawned when ejected) */
	FORCEINLINE FTransform GetCardSocketTransform() const { return CardSocket->GetComponentTransform(); }
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	FORCEINLINE bool IsAuthorized() const { return bAuthorized; }

	FORCEINLINE const TSoftObjectPtr<const UBankInfo> GetBankIssuer() const {return BankIssuer;}

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerLoginAttempt(const int32 PIN);
	
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerInsertCard(const FCardInfo& Card);
	bool CanInsertCard() const;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerGetCurrentBalance();
	bool CanGetCurrentBalance() const;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerCardEject();
	bool CanEjectCard() const;

	UFUNCTION(Client, Reliable)
	void ClientLoginAttempt(EResponse Response);
	FOnResponseReceivedDelegate OnLoginAttemptClient;

	UFUNCTION(Client, Reliable)
	void ClientGetCurrentBalance(EResponse Response, int32 Value);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerChangePin(const int32 NewPin);
	bool CanChangePin() const;

	virtual void OnClientChangePin(EResponse Response) {}

	/** Called on Server to notify that InteractableActor is not used by owning Client */
	UFUNCTION(Server, Reliable)
	void ServerReleaseDevice(ACorePlayerController* PlayerController);

	void ReleaseDevice(ACorePlayerController* PlayerController);
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "GUI")
	UWidgetComponent* GUIWidgetComponent; // renders and places our widgets from state machine in world
	
	UPROPERTY()
	FBankStateContainer StateContainer; // that's where states live, manages lifetime of behaviors and widgets
	
	UPROPERTY(EditDefaultsOnly)
	FBankDeviceStateContainerConfig Config; // in order to allow editing in details so that we can set widget classes

	virtual FBankStateContainer* GetStateContainer() override { return &StateContainer; }
	virtual UWidgetComponent* GetWidgetComponent() override { return StateContainer.GetWidgetComponent(); }

	virtual void BeginPlay() override;
	virtual void SubscribeToEventsFromStates() {};

	/** Server logic of Interaction */
	virtual void InteractWith(APawn* Interactor) override;

	/** Client logic of Interaction. Called on Client after Server interact logic is done */
	virtual void ClientOnInteract();
	
	void OccupyDevice(ACorePlayerController* PlayerController);
	
	virtual void ClientOnReleaseDevice(ACorePlayerController* PlayerController);
	
	/** Retrieves UProcessingCenter from ACoreGameMode. Does not cache anything */
	UProcessingCenter* GetProcessingCenter() const;

	UPROPERTY(Replicated)
	FCardInfo InsertedCard = FCardInfo::InvalidCard;
	
	UFUNCTION(Client, Reliable)
	void ClientOnCardInserted();
	virtual void ClientOnCardInserted_Implementation();

	UFUNCTION(Client, Reliable)
	void ClientOnCardEjected();
	virtual void ClientOnCardEjected_Implementation() {}
	
	UFUNCTION(Client, Reliable)
	void ClientChangePin(EResponse Response);

private:
	/** "Occupier's" Player controller. CurrentPlayer is Net Owner of this ABankDevice */
	UPROPERTY(Transient)
	ACorePlayerController* CurrentPlayer;
	

	/** Auxiliary function, that calls virtual ClientOnInteract to avoid problems with virtual RPCs */
	UFUNCTION(Client, Reliable)
	void ClientInteract();
	virtual void ClientInteract_Implementation();

	UFUNCTION(Client, Reliable)
	void ClientReleaseDevice(ACorePlayerController* PlayerController);
	virtual void ClientReleaseDevice_Implementation(ACorePlayerController* PlayerController);

	void ServerReleaseDevice_Implementation(ACorePlayerController* PlayerController);

	void ServerInsertCard_Implementation(const FCardInfo&);
	bool ServerInsertCard_Validate(const FCardInfo&);

	void ServerLoginAttempt_Implementation(int32 PIN);
	bool ServerLoginAttempt_Validate(int32 PIN);

	void ServerCardEject_Implementation();
	bool ServerCardEject_Validate();
	
	void ServerGetCurrentBalance_Implementation();
	bool ServerGetCurrentBalance_Validate();

	void ClientLoginAttempt_Implementation(EResponse Response);

	void ClientGetCurrentBalance_Implementation(EResponse Response, int32 Value);
	
	UPROPERTY(VisibleAnywhere)
	UArrowComponent* CardSocket;

	UPROPERTY(VisibleAnywhere)
	UTextRenderComponent* BankName;
	
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<const UBankInfo> BankIssuer;
	
	UPROPERTY(Replicated)
	bool bAuthorized;
};