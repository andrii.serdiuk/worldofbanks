// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "BankTerminal.h"
#include "Components/Button.h"
#include "Components/ArrowComponent.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceTransientBehaviors.h"
#include "WorldOfBanks/UI/Devices/Behaviors/Terminal/BankDeviceTerminalMainMenuWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/Terminal/BankDeviceTerminalBehaviorWidgets.h"
#include "WorldOfBanks/Pickup/PickupCard.h"
#include "WorldOfBanks/Components/DeviceBehavior/Login/BankDeviceLoginBehavior.h"
#include "WorldOfBanks/Components/DeviceBehavior/BlockCard/BankDeviceBlockCardBehavior.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceChangePinBehavior.h"
#include "WorldOfBanks/Components/DeviceBehavior/BlockCard/BankDeviceUnlockCardBehavior.h"
#include "WorldOfBanks/Core/CoreGameState.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceChangePinWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceGetMoneyWithButtonsWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDevicePrintCheckWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceShowMoneyWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceShowResponseWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceTopUpWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceTransferMoneyWidget.h"
#include "WorldOfBanks/UI/Devices/Receipts/BankDevicePOSReceiptWidget.h"

ABankTerminal::ABankTerminal(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	CashSocket = CreateDefaultSubobject<UArrowComponent>("Cash Socket");
	CashSocket->SetupAttachment(RootComponent);	
}

void ABankTerminal::ClientOnInteract()
{
	Super::ClientOnInteract();
	if (InsertedCard == FCardInfo::InvalidCard)
	{
		StateContainer.SetDeviceState<UBankDeviceGreetingTerminalBehavior>();
	}
	else
	{
		GoToMainMenu();
	}
}

void ABankTerminal::SubscribeToEventsFromStates()
{
	Super::SubscribeToEventsFromStates();

	UBankDeviceGreetingTerminalWidget* GreetingWidget = StateContainer.GetWidgetFor<UBankDeviceGreetingTerminalBehavior, UBankDeviceGreetingTerminalWidget>();
	GreetingWidget->OnNextPressed.AddUObject(this, &ABankTerminal::OnGreetingSuccess);
	GreetingWidget->OnBlockCardPressed.AddUObject(this, &ABankTerminal::OnBlockCardSuccess);

	UBankDeviceBlockCardBehavior* BlockCardBehavior = StateContainer.GetBehavior<UBankDeviceBlockCardBehavior>();
	BlockCardBehavior->OnBackToGreeting.AddUObject(this, &ABankTerminal::BackToGreeting);

	UBankDeviceUnlockCardBehavior* UnlockCardBehavior = StateContainer.GetBehavior<UBankDeviceUnlockCardBehavior>();
	UnlockCardBehavior->OnBackToGreeting.AddUObject(this, &ABankTerminal::BackToGreeting);
	UnlockCardBehavior->OnUnlockSuccess.AddUObject(this, &ABankTerminal::OnPINTyping);

	UBankDeviceLoginBehavior* LoginBehavior = StateContainer.GetBehavior<UBankDeviceLoginBehavior>();
	LoginBehavior->OnLoginFinishedWithSuccess.AddUObject(this, &ABankTerminal::GoToMainMenu);
	LoginBehavior->OnLoginFinishedWithFailure.BindUObject(this, &ABankTerminal::OnLoginFailure);

	UBankDeviceTerminalMainMenuWidget* MainMenu = StateContainer.GetWidgetFor<UBankDeviceMainMenuBehavior, UBankDeviceTerminalMainMenuWidget>();
	const UBankInfo* BankInfo = GetBankIssuer().LoadSynchronous();
	MainMenu->BankName->SetText(FText::FromString(BankInfo->GetBankName()));
	MainMenu->ChangePinButton->OnClicked.AddDynamic(this, &ABankTerminal::OnGotoChangePinSuccess);
	MainMenu->TopUpButton->OnClicked.AddDynamic(this, &ABankTerminal::OnGotoTopUpSuccess);
	MainMenu->TransferMoneyButton->OnClicked.AddDynamic(this, &ABankTerminal::OnGotoTransferMoneySuccess);
	MainMenu->CheckMoneyButton->OnClicked.AddDynamic(this, &ABankTerminal::OnGotoCheckMoneySuccess);
	MainMenu->PrintTransactionsButton->OnClicked.AddDynamic(this, &ABankTerminal::OnGotoPrintCheckSuccess);
	
	UBankDeviceChangePinWidget* ChangePinWidget = StateContainer.GetWidgetFor<UBankDeviceChangePinBehavior, UBankDeviceChangePinWidget>();
	ChangePinWidget->OnGoBack.AddUObject(this, &ABankTerminal::GoToMainMenu);
	
	UBankDeviceTopUpWidget* TopUpWidget = StateContainer.GetWidgetFor<UBankDeviceTopUpBehavior, UBankDeviceTopUpWidget>();
	TopUpWidget->OnGoBack.AddUObject(this, &ABankTerminal::GoToMainMenu);
	
	UBankDeviceTransferMoneyWidget* TransferMoneyWidget = StateContainer.GetWidgetFor<UBankDeviceTransferMoneyBehavior, UBankDeviceTransferMoneyWidget>();
	TransferMoneyWidget->OnGoBack.AddUObject(this, &ABankTerminal::GoToMainMenu);
	
	UBankDevicePrintCheckWidget* PrintCheckWidget = StateContainer.GetWidgetFor<UBankDevicePrintCheckBehavior, UBankDevicePrintCheckWidget>();
	PrintCheckWidget->OnGoBack.AddUObject(this, &ABankTerminal::GoToMainMenu);
	
	UBankDeviceShowMoneyWidget* CheckMoneyWidget = StateContainer.GetWidgetFor<UBankDeviceShowMoneyBehavior, UBankDeviceShowMoneyWidget>();
	CheckMoneyWidget->OnGoBack.AddUObject(this, &ABankTerminal::GoToMainMenu);

	UBankDeviceShowResponseWidget* ResponseWidget = StateContainer.GetWidgetFor<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>();
	ResponseWidget->OnConfirmation.AddUObject(this, &ABankTerminal::OnShowGenericResponseSuccess);

	UBankDevicePOSReceiptWidget* CheckWidget = StateContainer.GetWidgetFor<UBankDevicePOSReceiptBehavior, UBankDevicePOSReceiptWidget>();
	CheckWidget->OnOkPressed.AddUObject(this, &ABankTerminal::GoToMainMenu);
}

void ABankTerminal::ClientOnCardInserted_Implementation()
{
	Super::ClientOnCardInserted_Implementation();
}

bool ABankTerminal::ServerPushMoney_Validate(const FMoneyInfo& Money)
{
	return IsAuthorized() && CanPushMoney();
}

bool ABankTerminal::CanPushMoney() const
{
	return true;
}

void ABankTerminal::ClientGetTransactions_Implementation(const TArray<FTransactionInfo>& Extracts)
{
	StateContainer.GetWidgetFor<UBankDevicePrintCheckBehavior, UBankDevicePrintCheckWidget>()->UpdateExtracts(Extracts);
}

void ABankTerminal::ClientTopUpFunds_Implementation(EResponse Response)
{
	StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()
		->ShowResponse(Response);
}

void ABankTerminal::OnLoginFailure(FLoginFailureReason Reason)
{
	if(Reason.IsType<EResponse>() && Reason.Get<EResponse>() == EResponse::Blocked)
	{
		StateContainer.GetBehavior<UBankDeviceLoginBehavior>()->Clear();
		StateContainer.SetDeviceState<UBankDeviceUnlockCardBehavior>();
	}
	else
	{
		BackToGreeting();
	}
}

void ABankTerminal::OnClientChangePin(EResponse Response)
{
	Super::OnClientChangePin(Response);
	StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()->ShowResponse(Response);
}

void ABankTerminal::ServerPushMoney_Implementation(const FMoneyInfo& Money)
{
	if (AcceptableBanknotes.Contains(Money.Key))
	{
		int32& CurrentAmount = InsertedCash.FindOrAdd(Money.Key.Currency);
		CurrentAmount += Money.Value * Money.Key.Denomination;
		ClientPushMoney(Money);
	}
	else
	{
		if (ACoreGameState* CoreGameState = GetWorld()->GetGameState<ACoreGameState>())
		{
			FTransform Transform = CashSocket->GetComponentTransform();
			Transform.SetScale3D(FVector(1.f));
			TSubclassOf<APickupCash> CashClass = CoreGameState->GetPickupCashClass().LoadSynchronous();
			check(CashClass);
			APickupCash* CashToSpawn = GetWorld()->SpawnActorDeferred<APickupCash>(CashClass, Transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);
			CashToSpawn->Init(Money.Key, Money.Value);
			CashToSpawn->FinishSpawning(Transform);
			UE_LOG(Log_BankDevice, Log, TEXT("Cash %s spawned"), *CashToSpawn->GetName());
		}
	}
}


void ABankTerminal::ServerTransferMoney_Implementation(const FString& DestinationAccount, const int32 Sum, ECurrency Currency)
{
	ClientTransferMoney(
		GetProcessingCenter()->TransferToCard(this, InsertedCard, DestinationAccount, Currency, Sum),
		DestinationAccount, Sum, Currency);
}

bool ABankTerminal::ServerTransferMoney_Validate(const FString& DestinationAccount, const int32 Sum, ECurrency Currency)
{
	return IsAuthorized() && CanTransferMoney() && Sum > 0;
}

void ABankTerminal::ServerGenerateOTP_Implementation(int64 PhoneNumber)
{
	ClientGenerateOTP(GetProcessingCenter()->GenerateAndSendOTP(this, PhoneNumber), PhoneNumber);
}

bool ABankTerminal::ServerGenerateOTP_Validate(int64 PhoneNumber)
{
		return CanBlockCardGenerateOTP() && PhoneNumber >= 0 && PhoneNumber <= 999999999999;
}

void ABankTerminal::ServerGetAllCards_Implementation(int64 PhoneNumber, int32 OTP)
{
	if (GetProcessingCenter()->IsOTPValid(this, PhoneNumber, OTP))
	{
		ClientShowAllCards(GetProcessingCenter()->GetAllCards(this, PhoneNumber));
	}
	else
	{
		ClientInvalidOTP();
	}
}

bool ABankTerminal::ServerGetAllCards_Validate(int64 PhoneNumber, int32 OTP)
{
	return CanBlockCardVerifyOTP() && PhoneNumber >= 0 && PhoneNumber <= 999999999999
							&& OTP >= 0 && OTP <= 9999;
}

void ABankTerminal::ClientInvalidOTP_Implementation()
{
	StateContainer.GetBehavior<UBankDeviceBlockCardBehavior>()->OnCodeTypingFailure();
}

bool ABankTerminal::CanTransferMoney() const
{
	return true;
}

bool ABankTerminal::CanBlockCardGenerateOTP() const
{
	return true;
}

bool ABankTerminal::CanBlockCardVerifyOTP() const
{
	return true;
}

bool ABankTerminal::CanTopUpFunds() const
{
	return InsertedCash.Num() != 0;;
}

void ABankTerminal::ServerGetCurrentCardPhone_Implementation()
{
	check(!(InsertedCard == FCardInfo::InvalidCard));
	int64* Phone = GetProcessingCenter()->GetPhoneNumber(InsertedCard);
	if(Phone)
	{
		ClientGetConnectedPhone(EResponse::Success, *Phone );
	}
	else
	{
		ClientGetConnectedPhone(EResponse::UnknownCard, -1 );
	}
}

bool ABankTerminal::ServerGetCurrentCardPhone_Validate()
{
	return true;
}

void ABankTerminal::ClientGetConnectedPhone_Implementation(EResponse Response, int64 Phone)
{
	//Make check because this function is only used by unlocking behavior
	//Card info must be valid
	check(Response == EResponse::Success);
	StateContainer.GetBehavior<UBankDeviceUnlockCardBehavior>()->ReceivedPhone(Phone);
}

void ABankTerminal::ClientUnlockCard_Implementation(EResponse Response)
{
	StateContainer.GetBehavior<UBankDeviceUnlockCardBehavior>()->OnFinished(Response);
}

void ABankTerminal::ServerUnlockInsertedCard_Implementation(int32 Code)
{
	UProcessingCenter* Center = GetProcessingCenter();
	int64* Phone = Center->GetPhoneNumber(InsertedCard);
	check(Phone);
	if(Center->IsOTPValid(this, *Phone, Code))
	{
		ClientUnlockCard(Center->SetBlockedState(this, InsertedCard, false));	
	}
	else
	{
		ClientUnlockCard(EResponse::InvalidOTP);
	}
}

bool ABankTerminal::ServerUnlockInsertedCard_Validate(int32 Code)
{
	return true;
}

void ABankTerminal::ClientBlockCard_Implementation(EResponse Response)
{
	StateContainer.GetBehavior<UBankDeviceBlockCardBehavior>()->OnBlockedSuccess();
	StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()
				   ->ShowResponse(Response);
}

void ABankTerminal::ServerBlockCard_Implementation(const FCardInfo& CardInfo)
{
	ClientBlockCard(GetProcessingCenter()->SetBlockedState(this, CardInfo, true));
}

bool ABankTerminal::ServerBlockCard_Validate(const FCardInfo& CardInfo)
{
	return true;
}

void ABankTerminal::ServerTopUpFunds_Implementation()
{
	TArray<FBanknoteInfo> ToSend;
	for (const TTuple<ECurrency, int>&  Pair : InsertedCash)
	{
		ToSend.Add(FBanknoteInfo{Pair.Key, Pair.Value});
	}
	EResponse Response = GetProcessingCenter()->DepositFunds(this, InsertedCard, ToSend);
	if (Response == EResponse::Success)
	{
		InsertedCash.Empty();
	}
	ClientTopUpFunds(Response);
}

bool ABankTerminal::ServerTopUpFunds_Validate()
{
	return true;
}

void ABankTerminal::ClientGenerateOTP_Implementation(EResponse Response, int64 PhoneNumber)
{
	if(Response == EResponse::Success)
	{
		if(StateContainer.GetCurrentBehavior<UBankDeviceUnlockCardBehavior>())
		{
			StateContainer.GetBehavior<UBankDeviceUnlockCardBehavior>()->ReceivedOTP(PhoneNumber);
		}
		else
		{
			StateContainer.GetBehavior<UBankDeviceBlockCardBehavior>()->OnPhoneFound(PhoneNumber);
		}
	}
	else
	{
		StateContainer.GetBehavior<UBankDeviceBlockCardBehavior>()->OnPhoneNotFound(Response);
	}
}

void ABankTerminal::ClientPushMoney_Implementation(const FMoneyInfo& AddedMoneyInfo)
{
	OnMoneyPushed.Broadcast(AddedMoneyInfo);
}

void ABankTerminal::ClientTransferMoney_Implementation(EResponse Response, const FString& Destination,
                                                       const int32 Sum, ECurrency Currency)
{
	if(Response == EResponse::Success)
	{
		UBankDevicePOSReceiptWidget* CheckWidget = StateContainer.GetWidgetFor<UBankDevicePOSReceiptBehavior, UBankDevicePOSReceiptWidget>();
		CheckWidget->HeaderMessage->SetText(FText::FromString(FString("Successfully transferred to\n") + Destination));
		CheckWidget->Init(Sum, Currency);
		StateContainer.SetDeviceState<UBankDevicePOSReceiptBehavior>();
	}
	else
	{
		StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()->ShowResponse(Response);
	}
}

void ABankTerminal::ClientShowAllCards_Implementation(const TArray<FCardInfo>& Cards)
{
	StateContainer.GetBehavior<UBankDeviceBlockCardBehavior>()->OnCodeTypingSuccess(Cards);
}

void ABankTerminal::OnPINTyping()
{
	StateContainer.GetBehavior<UBankDeviceLoginBehavior>()->SetInsertedCard();
	StateContainer.SetDeviceState<UBankDeviceLoginBehavior>();
}

void ABankTerminal::OnGreetingSuccess()
{
	StateContainer.SetDeviceState<UBankDeviceLoginBehavior>();
}

void ABankTerminal::OnBlockCardSuccess()
{
	StateContainer.SetDeviceState<UBankDeviceBlockCardBehavior>();
}

void ABankTerminal::OnShowGenericResponseSuccess(EResponse Response)
{
	if (IsAuthorized())
	{
		GoToMainMenu();
	}
	else
	{
		BackToGreeting();
	}
}

void ABankTerminal::BackToGreeting()
{
	ServerCardEject();
	StateContainer.SetDeviceState<UBankDeviceGreetingTerminalBehavior>();
}

void ABankTerminal::OnGotoChangePinSuccess()
{
	StateContainer.SetDeviceState<UBankDeviceChangePinBehavior>();
}

void ABankTerminal::OnGotoCheckMoneySuccess()
{
	StateContainer.SetDeviceState<UBankDeviceShowMoneyBehavior>();
	StateContainer.GetCurrentWidget<UBankDeviceShowMoneyWidget>()->ServerUpdateCall();
}

void ABankTerminal::OnGotoTopUpSuccess()
{
	StateContainer.SetDeviceState<UBankDeviceTopUpBehavior>();
}

void ABankTerminal::OnGotoTransferMoneySuccess()
{
	StateContainer.SetDeviceState<UBankDeviceTransferMoneyBehavior>();
}

void ABankTerminal::OnGotoPrintCheckSuccess()
{
	StateContainer.SetDeviceState<UBankDevicePrintCheckBehavior>();
	ServerGetTransactions();
}

void ABankTerminal::GoToMainMenu()
{
	StateContainer.SetDeviceState<UBankDeviceMainMenuBehavior>();
}

void ABankTerminal::ServerGetTransactions_Implementation()
{
	ClientGetTransactions(GetProcessingCenter()->GetTransactions(InsertedCard));
}

bool ABankTerminal::ServerGetTransactions_Validate()
{
	return true;
}
