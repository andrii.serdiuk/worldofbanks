// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "BankPOS.h"

#include "Kismet/GameplayStatics.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceTransientBehaviors.h"
#include "WorldOfBanks/Core/CorePlayerController.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceGetMoneyWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceShowResponseWidget.h"
#include "WorldOfBanks/UI/Devices/Behaviors/BankDeviceGetMoneyWithButtonsWidget.h"
#include "WorldOfBanks/UI/Devices/Receipts/BankDevicePOSReceiptWidget.h"

ABankPOS::ABankPOS(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;
}

bool ABankPOS::CanPay() const
{
	return true;
}


bool ABankPOS::ServerPay_Validate(const int32 Sum, ECurrency Currency)
{
	return IsAuthorized() && CanPay() && Sum > 0;
}

void ABankPOS::SubscribeToEventsFromStates()
{
	Super::SubscribeToEventsFromStates();
	
	UBankDeviceGreetingPOSWidget* GreetingWidget = StateContainer.GetWidgetFor<UBankDeviceGreetingPosBehavior, UBankDeviceGreetingPOSWidget>();
	GreetingWidget->OnNextPressed.AddUObject(this, &ABankPOS::OnGreetingSuccess);
	
	UBankDeviceGetMoneyWithButtonsWidget* GetMoneyWidget = StateContainer.GetWidgetFor<UBankDeviceGetSumBehavior, UBankDeviceGetMoneyWithButtonsWidget>();
	GetMoneyWidget->OnSubmit.AddUObject(this, &ABankPOS::OnGetSumSuccess);
	GetMoneyWidget->OnGoBack.AddUObject(this, &ABankPOS::OnGetSumCancelled);
	
	UBankDeviceLoginBehavior* LoginBehavior = StateContainer.GetBehavior<UBankDeviceLoginBehavior>();
	LoginBehavior->OnLoginFinishedWithSuccess.AddUObject(this, &ABankPOS::OnLoginSuccess);
	LoginBehavior->OnLoginFinishedWithFailure.BindUObject(this, &ABankPOS::OnLoginFailure);
	
	UBankDeviceShowResponseWidget* ResponseWidget = StateContainer.GetWidgetFor<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>();
	ResponseWidget->OnConfirmation.AddUObject(this, &ABankPOS::OnResponseSuccess);
	
	UBankDevicePOSReceiptWidget* CheckWidget = StateContainer.GetWidgetFor<UBankDevicePOSReceiptBehavior, UBankDevicePOSReceiptWidget>();
	CheckWidget->OnOkPressed.AddUObject(this, &ABankPOS::OnOkSuccess);
}


void ABankPOS::ServerPay_Implementation(const int32 Sum, ECurrency Currency)
{
	const EResponse Response = GetProcessingCenter()->POSPayment(this, InsertedCard, Currency, Sum);
	ClientPay(Response, Sum, Currency);
}


void ABankPOS::ClientOnInteract()
{
	Super::ClientOnInteract();

	check(InsertedCard == FCardInfo::InvalidCard)
	StateContainer.SetDeviceState<UBankDeviceGreetingPosBehavior>();
}

void ABankPOS::OnGreetingSuccess()
{
	StateContainer.SetDeviceState<UBankDeviceGetSumBehavior>();
}

void ABankPOS::OnGetSumSuccess()
{
	// maybe add StateContainer.GetWidgetFor<UBankDeviceGetSumBehavior, UBankDeviceGetMoneyWidget>()->ResetData();
	StateContainer.SetDeviceState<UBankDeviceLoginBehavior>();
}

void ABankPOS::OnGetSumCancelled()
{
	StateContainer.SetDeviceState<UBankDeviceGreetingPosBehavior>();
}

void ABankPOS::OnLoginSuccess()
{
	check (StateContainer.IsCurrentState<UBankDeviceLoginBehavior>())
	
	if (const UBankDeviceGetMoneyWithButtonsWidget* GetMoneyWidget = StateContainer.GetWidgetFor<UBankDeviceGetSumBehavior, UBankDeviceGetMoneyWithButtonsWidget>())
	{
		ServerPay(GetMoneyWidget->GetSum().GetValue() * 100, GetMoneyWidget->GetCurrency());
		ServerCardEject();
	}
}

void ABankPOS::ClientPay_Implementation(EResponse Response, const int32 Sum, ECurrency Currency)
{
	UE_LOG(Log_BankDevice, Log, TEXT("ClientPay with %s response."), *UEnum::GetValueAsString(Response))
	
	if (Response == EResponse::Success)
	{
		//StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()->ShowResponse(Response);
		UBankDevicePOSReceiptWidget* CheckWidget = StateContainer.GetWidgetFor<UBankDevicePOSReceiptBehavior, UBankDevicePOSReceiptWidget>();
		CheckWidget->Init(Sum, Currency);
		StateContainer.SetDeviceState<UBankDevicePOSReceiptBehavior>();
	}
	else
	{
		StateContainer.SetDeviceStateW<UBankDeviceShowResponseBehavior, UBankDeviceShowResponseWidget>()->ShowResponse(Response);
	}
}

void ABankPOS::OnLoginFailure(FLoginFailureReason Reason)
{
	StateContainer.SetDeviceState<UBankDeviceGreetingPosBehavior>();
	ServerCardEject();
}

void ABankPOS::OnResponseSuccess(EResponse Response)
{
	StateContainer.SetDeviceState<UBankDeviceGreetingPosBehavior>();
	ServerCardEject();
}

void ABankPOS::OnOkSuccess()
{
	OnResponseSuccess(EResponse::Success);
}

