// Copyright Epic Games, Inc. All Rights Reserved.

#include "WorldOfBanks.h"

#include "Bank/Currency.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, WorldOfBanks, "WorldOfBanks" );

DEFINE_LOG_CATEGORY(Log_Character);
DEFINE_LOG_CATEGORY(Log_Connection);
DEFINE_LOG_CATEGORY(Log_Interactable);
DEFINE_LOG_CATEGORY(Log_UI);
DEFINE_LOG_CATEGORY(Log_Processing);
DEFINE_LOG_CATEGORY(Log_BankDevice);
DEFINE_LOG_CATEGORY(Log_Login);
DEFINE_LOG_CATEGORY(Log_Wallet_Pick);
DEFINE_LOG_CATEGORY(Log_Phone);
DEFINE_LOG_CATEGORY(Log_Save);
DEFINE_LOG_CATEGORY(Log_Bank_Device);
DEFINE_LOG_CATEGORY(Log_Transaction);

const FString FConnectOptionKeys::Name = TEXT("NickName");
const FString FConnectOptionKeys::Password = TEXT("Pass");

const FString FConnectErrors::NoOptionKeys = TEXT("Required login options were not get with connection URL");
const FString FConnectErrors::InvalidPassword = TEXT("Invalid password");
const FString FConnectErrors::AlreadyPlaying = TEXT("This player is already joined the server");
const FString FConnectErrors::BadPlayerControllerClass = TEXT("Other PlayerController class!");

const FString FPhoneMessage::POSPayment = TEXT("POS payment: {Sum} {Currency} has been withdrawn from {Card}.\n Commission: {Commission} {Currency}");
const FString FPhoneMessage::TransferStringSender = TEXT("Transfer notification: {Sum} {Currency} has been sent from your card {SenderCard} to {ReceiverCard}. Commission: {Commission} {Currency}");
const FString FPhoneMessage::TransferStringReceiver = TEXT("Transfer notification: {Sum} {Currency} has been has been transfered on your card {ReceiverCard} from {SenderCard}");
const FString FPhoneMessage::DepositString = TEXT("Transaction notification: {Sum} has been put on your card {Card}. Commission: {Commission} {Currency}");
const FString FPhoneMessage::ATMWithdrawal = TEXT("Transaction notification: {Sum} {Currency} has been withdrawn from card {Card}. Commission: {Commission} {Currency}");

const FString FTransactions::POSTransaction = TEXT("POS payment: {Sum} {Currency}");
const FString FTransactions::ATMWithdrawalTransaction = TEXT("ATM withdrawal: {Sum} {Currency}");
const FString FTransactions::DepositFundsTransaction = TEXT("Deposit in a terminal: {Money}");
const FString FTransactions::TransferReceiverTransaction = TEXT("{Sum} {Currency} transfer from {SenderCard}");
const FString FTransactions::TransferSenderTransaction = TEXT("{Sum} {Currency} transfer to {ReceiverCard}");

const FString FBankActions::CardHasBeenBlocked = TEXT("Your card {Card} has been blocked. Sincerely yours, {Bank}");
const FString FBankActions::CardHasBeenUnblocked = TEXT("Your card {Card} has been unblocked. Sincerely yours, {Bank}");
const FString FBankActions::PINHasBeenChanged = TEXT("The PIN of your card {Card} has been changed. Sincerely yours, {Bank}");


const FString FOTPMessages::NewOTP = TEXT("Your new OTP code: {Code}");

const FString FSaveOptions::ServerSlotName = "ServerSave";
const int32 FSaveOptions::ServerUserID = 0;

const FString FSaveOptions::ClientSlotName = "ClientSave";
const int32 FSaveOptions::ClientUserID = 1;

const FString FSaveOptions::DefaultHost = TEXT("127.0.0.1:7777");
const FString FSaveOptions::DefaultLogin = TEXT("Vozniuk");
const FString FSaveOptions::DefaultPassword = TEXT("Password");

const FString FMessages::PlayerJoined = TEXT(" has just joined the game!");
const FString FMessages::PlayerExited = TEXT(" left the game!");
const FString FMessages::DeviceAlreadyOccupied = TEXT("This Device is already occupied!");

extern const TMap<ECurrency, FString> CurrencySymbols =
{
	{ ECurrency::Dollar, TEXT("$") },
	{ ECurrency::Euro, TEXT("€") },
	{ ECurrency::Hryvnia, TEXT("₴") }
};

extern const TMap<ECurrency, FString> CurrencyNames =
{
	{ ECurrency::Dollar, TEXT("Dollar") },
	{ ECurrency::Euro, TEXT("Euro") },
	{ ECurrency::Hryvnia, TEXT("Hryvnia") }
};

FString FStringHelpers::GetStringFromSum(int32 Sum)
{
	FString Result = FString::FromInt(Sum);
	Result.InsertAt(Result.Len() - 2, L'.');
	return Result;
}