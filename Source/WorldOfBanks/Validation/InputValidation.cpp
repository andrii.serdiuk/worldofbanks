﻿#include "inputValidation.h"

TOptional<int32> FInputValidation::GetSumFromText(const FText& Input)
{
	const FString InputString = Input.ToString();
	if (IsSumValid(InputString))
	{
		const int32 ExtractedValue = FCString::Atoi(*InputString);
		return IsSumValid(ExtractedValue) ? ExtractedValue : TOptional<int32>();
	}
	
	return TOptional<int32>();
	
}

bool FInputValidation::IsSumValid(const FString& Input)
{
	return Input.IsNumeric() && Input.Len() < 15;
}

bool FInputValidation::IsSumValid(int32 Sum)
{
	return Sum > 0 && Sum < 10000000;
}

bool FInputValidation::IsCardAccountValid(const FString& Input)
{
	return Input.IsNumeric() && Input.Len() == 16;
}

bool FInputValidation::IsPhoneNumberValid(const FText& Text)
{
	const FString TextString = Text.ToString();
	return TextString.TrimStartAndEnd().Len() == 12 && IsPositiveInt(TextString);
}

bool FInputValidation::IsPositiveInt(const FString& String)
{
	for (int32 i = 0; i < String.Len(); ++i)
	{
		if (String[i] < '0' || String[i] > '9')
		{
			return false;
		}
	}
	return true;
}