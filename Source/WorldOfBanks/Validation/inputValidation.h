﻿#pragma once

struct FInputValidation
{

	static TOptional<int32> GetSumFromText(const FText& Input);
	static bool IsSumValid(const FString& Input);
	static bool IsSumValid(int32 Sum);

	static bool IsCardAccountValid(const FString& Input);

	static bool IsPositiveInt(const FString& String);
	static bool IsPhoneNumberValid(const FText& Text);
};