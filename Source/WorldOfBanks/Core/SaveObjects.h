﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/Bank/Banknote.h"
#include "WorldOfBanks/Bank/CardInfo.h"
#include "WorldOfBanks/Bank/BankAccount.h"
#include "WorldOfBanks/Core/ReplicationUtils.h"
#include "SaveObjects.generated.h"

/**
* Save Data about bank, saves accounts. Also, used as a wrapper for TArray<FBankAccount>
*/
USTRUCT()
struct WORLDOFBANKS_API FBankData
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<FBankAccount> Accounts;
};

/**
* Save Data about wallet of a specific player. Is contained in FPlayerSaveData. Passed to UWalletComponent
* after auth process
*/
USTRUCT()
struct WORLDOFBANKS_API FPlayerWalletSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<FMoneyInfo> WalletCash;

	UPROPERTY()
	TArray<FCardInfo> WalletCards;
};

/**
* Save Data about phone of a specific player. Is contained in FPlayerSaveData. Passed to UPhoneComponent
* after auth process
*/
USTRUCT()
struct WORLDOFBANKS_API FPlayerPhoneSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	int64 PhoneNumber;

	UPROPERTY()
	TArray<FString> Messages;
};

/**
* Save Data about a specific player. Is contained in FServerPlayerInfo. Passed to ACorePlayerController
* after auth process
*/
USTRUCT()
struct WORLDOFBANKS_API FPlayerSaveData
{
	GENERATED_BODY()

	/** Indicates whether player is "new" and starter pack should be given or not*/
	UPROPERTY()
	bool bNewPlayer = true;

	UPROPERTY()
	FPlayerWalletSaveData Wallet;

	UPROPERTY()
	FPlayerPhoneSaveData Phone;

	// #TODO ksydorov resolve problem on GameMode::Logout, that Character is nullptr
	/*UPROPERTY()
	FTransform CharacterTransform;*/
};

/**
* Save Data about a specific APickupCard
*/
USTRUCT()
struct FPickupCardSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	FTransform ActorTransform;

	UPROPERTY()
	TSoftClassPtr<class APickupCard> PickupActorClass;

	UPROPERTY()
	FCardInfo CardInfo;
};

/**
* Save Data about a specific APickupCash
*/
USTRUCT()
struct FPickupCashSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	FTransform ActorTransform;

	UPROPERTY()
	TSoftClassPtr<class APickupCash> PickupActorClass;

	UPROPERTY()
	FBanknote Banknote;

	UPROPERTY()
	int32 Amount;
};