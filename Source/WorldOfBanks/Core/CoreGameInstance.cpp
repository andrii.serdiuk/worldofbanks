// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "CoreGameInstance.h"
#include "Kismet/KismetSystemLibrary.h"
#include "WorldOfBanks/WorldOfBanks.h"

void UCoreGameInstance::ConnectToServer(const FString& Host, const FString& Name, const FString& Password)
{
	checkf(!Host.IsEmpty(), TEXT("Host address is empty"));
	checkf(!Name.IsEmpty(), TEXT("Player name is empty"));
	checkf(!Password.IsEmpty(), TEXT("Password is empty"));

	// Build connect request
	const FString CmdToExecute = FString::Printf(TEXT("open %s?%s=%s?%s=%s"),
		*Host,
		*FConnectOptionKeys::Name, *Name,
		*FConnectOptionKeys::Password, *Password);
	UE_LOG(Log_Connection, Warning, TEXT("Connection request: %s"), *CmdToExecute);
	
	// Execute connect request
	UKismetSystemLibrary::ExecuteConsoleCommand(GetWorld(), CmdToExecute);
}