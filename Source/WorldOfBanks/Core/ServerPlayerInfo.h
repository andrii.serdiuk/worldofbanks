﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "SaveObjects.h"
#include "ServerPlayerInfo.generated.h"

/**
 * Secret information representing player (server information that is not replicated to clients)
 */
USTRUCT()
struct WORLDOFBANKS_API FServerPlayerInfo
{
	GENERATED_BODY()

	FServerPlayerInfo()
	{
	}
	
	FServerPlayerInfo(const FString& Name, const FString& Password)
		: PlayerName(Name), PlayerPassword(Password)
	{
	}

	FServerPlayerInfo(const FServerPlayerInfo&) = default;
	FServerPlayerInfo& operator=(const FServerPlayerInfo&) = default;
	
	UPROPERTY()
	FString PlayerName = "";

	UPROPERTY()
	FString PlayerPassword = "";

	/** After connection, this bool is set to true to prevent several connections of a player */
	UPROPERTY(Transient)
	bool bConnectedToServer = false;

	UPROPERTY(Transient)
	APlayerController* AssociatedController = nullptr;

	UPROPERTY()
	FPlayerSaveData SaveData;
};