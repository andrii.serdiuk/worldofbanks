// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "CorePlayerController.h"
#include "CoreCharacter.h"
#include "CoreGameMode.h"
#include "CoreHUD.h"
#include "DrawDebugHelpers.h"
#include "Blueprint/UserWidget.h"
#include "Camera/CameraComponent.h"
#include "Components/WidgetComponent.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "WorldOfBanks/Bank/ProcessingCenter.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "WorldOfBanks/Game/InteractableActor.h"
#include "WorldOfBanks/Utility/CollisionHelpers.h"
#include "WorldOfBanks/Components/PhoneComponent.h"
#include "WorldOfBanks/Components/WalletComponent.h"
#include "WorldOfBanks/Utility/RotationHelpers.h"
#include "WorldOfBanks/UI/PINFailMiniGameWidget.h"
#include "CoreSaveGame.h"
#include "Net/UnrealNetwork.h"


#if !UE_BUILD_SHIPPING && !UE_BUILD_TEST

TAutoConsoleVariable<bool> CVarDebugInteraction
{
	TEXT("WOB.DebugInteraction"),
	false,
	TEXT("Draws cone and traces.\n")
};

#endif


ACorePlayerController::ACorePlayerController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	
	InteractionWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("InteractionWidgetComponent");
	InteractionWidgetComponent->SetHiddenInGame(true);
	InteractionWidgetComponent->SetupAttachment(RootComponent);

	SetHidden(false); // player controller is hidden in game by default, we dont want this behavior

	Wallet = CreateDefaultSubobject<UWalletComponent>("Wallet");
	Wallet->SetNetAddressable();
	Wallet->SetIsReplicated(true);

	Phone = CreateDefaultSubobject<UPhoneComponent>("Phone");
	Phone->SetNetAddressable();
	Phone->SetIsReplicated(true);
}

void ACorePlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ACorePlayerController, CurrentDevice);
}

void ACorePlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(FInputModeGameOnly());
}

void ACorePlayerController::AcknowledgePossession(APawn* InPawn)
{
	Super::AcknowledgePossession(InPawn);
	
	CoreCharacter = Cast<ACoreCharacter>(InPawn);
	check(CoreCharacter);
}

void ACorePlayerController::SetInteractionWidgetVisibility(bool bInteractionWidgetHidden)
{
	InteractionWidgetComponent->SetVisibility(bInteractionWidgetHidden);
}

void ACorePlayerController::LaunchPinFailedMinigame()
{
	bMinigameStarted = true;
	ClientOnMinigameLaunched();
}

void ACorePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Interact", IE_Pressed, this, &ACorePlayerController::Interact);
	InputComponent->BindAction("TogglePhone", IE_Pressed, this, &ACorePlayerController::TogglePhone);
	InputComponent->BindAction("ToggleWallet", IE_Pressed, this, &ACorePlayerController::ToggleWallet);
}

void ACorePlayerController::ClientOnMinigameLaunched_Implementation()
{
	if (ACoreHUD* CoreHUD = GetHUD<ACoreHUD>())
	{
		CoreHUD->SpawnMinigameWidget();

		if (UPINFailMiniGameWidget* MinigameWidget = CoreHUD->GetMinigameWidget())
		{
			MinigameWidget->OnMinigameFinished.BindWeakLambda(this, [this, CoreHUD](bool bMinigameSucceeded)
				{
					ServerOnMinigameFinished(bMinigameSucceeded);

					if (!bMinigameSucceeded)
					{
						DisableInput(this);

						ACoreCharacter* CoreCharacter = GetPawn<ACoreCharacter>();
						check(CoreCharacter);

						// Position camera as top-down
						UCameraComponent* Camera = CoreCharacter->GetFollowCamera();
						Camera->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
						Camera->SetWorldLocation(CoreCharacter->GetActorLocation() + FVector::UpVector * 500.f);
						Camera->bUsePawnControlRotation = false;
						Camera->SetWorldRotation(FRotator(-90.f, 0.f, 0.f));

						// Turn on Ragdoll
						CoreCharacter->GetMesh()->SetSimulatePhysics(true);
						
						// Show WASTED widget (like in GTA)
						CoreHUD->SpawnWastedWidget();

						// End interaction with CurrentDevice
						if (CurrentDevice)
						{
							CurrentDevice->ServerReleaseDevice(this);
						}
					}
					else
					{
						// Have some bug with widget focusing, reset Interaction state
						CoreHUD->OnEndDeviceInteraction();
						CoreHUD->OnStartDeviceInteraction();
					}
				});
		}
	}
}

void ACorePlayerController::ServerOnMinigameFinished_Implementation(bool bMinigameSucceeded)
{
	if (!bMinigameSucceeded)
	{
		// Remove all cards and cash from Wallet
		(*Wallet << FPlayerWalletSaveData());
	}
}

bool ACorePlayerController::ServerOnMinigameFinished_Validate(bool bResult)
{
	// We can only accept Minigame responses if minigame has already started
	return bMinigameStarted;
}

void ACorePlayerController::ClientNotifyFullWallet_Implementation(const FBanknote& Banknote) const
{
	checkf (GetWorld()->IsClient(), TEXT("ClientNotifyFullWallet has been called on server, wtf?"))

	const FString Error = FString::Printf(TEXT("Your wallet is full. You can't pick up %s cash any more."), *CurrencyNames.FindChecked(Banknote.Currency));
	GetHUD<ACoreHUD>()->ShowOneLineWidget(Error);
}

void ACorePlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetWorld()->IsClient() && CoreCharacter)
	{
		TickInteraction();
	}
}

ACorePlayerController& ACorePlayerController::operator<<(const FPlayerSaveData& SaveData)
{
	verifyf(GetWorld()->IsServer(), TEXT("Save logic should occur ONLY on server"));

	*Wallet << SaveData.Wallet;
	*Phone << SaveData.Phone;
	
	return *this;
}

const ACorePlayerController& ACorePlayerController::operator>>(FPlayerSaveData& SaveData) const
{
	verifyf(GetWorld()->IsServer(), TEXT("Save logic should occur ONLY on server"));
	
	SaveData.bNewPlayer = false;
		
	*Wallet >> SaveData.Wallet;
	*Phone >> SaveData.Phone;
	
	return *this;
}

void ACorePlayerController::TickInteraction()
{
	InteractionCandidate = GetBestInteractable();
	
	if (InteractionCandidate)
	{
		if (InteractionWidgetComponent->bHiddenInGame)
		{
			InteractionWidgetComponent->SetHiddenInGame(false);
		}

		InteractionWidgetComponent->SetWorldLocation(InteractionCandidate->GetActorLocation());
	}
	else
	{
		InteractionWidgetComponent->SetHiddenInGame(true);
	}
}

AInteractableActor* ACorePlayerController::GetBestInteractable() const
{
	const FVector MyLocation = CoreCharacter->GetFollowCamera()->GetComponentLocation();
	
	FHitResult HitResult;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	const FVector TraceEnd = MyLocation + CoreCharacter->GetFollowCamera()->GetForwardVector() * InteractionDistance;
	GetWorld()->LineTraceSingleByChannel(HitResult, MyLocation, TraceEnd, ECC_Visibility, Params);

	return Cast<AInteractableActor>(HitResult.Actor.Get());
}

void ACorePlayerController::Interact()
{	
	if (InteractionCandidate)
	{
		ServerInteractWith(InteractionCandidate);
	}
}

void ACorePlayerController::ServerInteractWith_Implementation(AInteractableActor* Interactable)
{
	check(Interactable);
	Interactable->TryInteractWith(GetPawn());
}

bool ACorePlayerController::ServerInteractWith_Validate(AInteractableActor* Interactable)
{
	if (!GetPawn() || !Interactable)
	{
		// Kick player if server's data differ from client's one
		return false;
	}
	
	// Not very accurate, because in case of invalid data, client will be disconnected from server.
	constexpr float MaxAllowedInteractDistance = 2000.f;
	return FVector::Dist(Interactable->GetActorLocation(), GetPawn()->GetActorLocation()) <= MaxAllowedInteractDistance;
}

void ACorePlayerController::ClientWelcomeMessage_Implementation(const FString& NewPlayerName)
{
	if (ACoreHUD* CoreHUD = GetHUD<ACoreHUD>())
	{
		FString WelcomeMessage = NewPlayerName;
		WelcomeMessage.Append(FMessages::PlayerJoined);
		
		CoreHUD->ShowOneLineWidget(WelcomeMessage);
	}
	UE_LOG(Log_Connection, Warning, TEXT("Player %s joined game"), *NewPlayerName);
}

void ACorePlayerController::ClientLogoutMessage_Implementation(const FString& PlayerName)
{
	if (ACoreHUD* CoreHUD = GetHUD<ACoreHUD>())
	{
		FString LogoutMessage = PlayerName;
		LogoutMessage.Append(FMessages::PlayerExited);
		
		CoreHUD->ShowOneLineWidget(LogoutMessage);
	}
	UE_LOG(Log_Connection, Warning, TEXT("Player %s left the game"), *PlayerName);
}

void ACorePlayerController::ClientNotifyAlreadyOccupied_Implementation()
{
	if (ACoreHUD* CoreHUD = GetHUD<ACoreHUD>())
	{
		CoreHUD->ShowOneLineWidget(FMessages::DeviceAlreadyOccupied);
	}
}

void ACorePlayerController::TogglePhone()
{
	if (ACoreHUD* CoreHUD = GetHUD<ACoreHUD>())
	{
		CoreHUD->TogglePhone();
	}
	else
	{
		UE_LOG(Log_UI, Error, TEXT("CoreHUD is inaccessible!"));
	}
}

void ACorePlayerController::WOB_SendPhoneMessage(int64 PhoneNumber, const FString& Message)
{
	ServerSendPhoneMessage(PhoneNumber, Message);
}

void ACorePlayerController::ServerSendPhoneMessage_Implementation(int64 PhoneNumber, const FString& Message)
{
	if (ACoreGameMode* CoreGM = GetWorld()->GetAuthGameMode<ACoreGameMode>())
	{
		CoreGM->GetProcessingCenter()->DebugSendMessage(PhoneNumber, Message);
	}
}

void ACorePlayerController::ServerStop_Implementation()
{
	if (ACoreGameMode* CoreGM = GetWorld()->GetAuthGameMode<ACoreGameMode>())
	{
		CoreGM->ExitGame();
	}
}

void ACorePlayerController::ServerSaveGame_Implementation()
{
	if (ACoreGameMode* CoreGM = GetWorld()->GetAuthGameMode<ACoreGameMode>())
	{
		CoreGM->DebugSaveGame();
	}
}

void ACorePlayerController::ServerCheatAddCents_Implementation(const FString& CardNumber, int32 Cents)
{
	if (ACoreGameMode* CoreGM = GetWorld()->GetAuthGameMode<ACoreGameMode>())
	{
		CoreGM->GetProcessingCenter()->DebugAddCents(CardNumber, Cents);
	}
}

void ACorePlayerController::ToggleWallet()
{
	if(ACoreHUD* CoreHUD = GetHUD<ACoreHUD>())
	{
		CoreHUD->ToggleWallet();
	}
}

void ACorePlayerController::WOB_TestDropCard()
{
	if (Wallet->GetCards().Num() >= 1)
	{
		FCardInfo DuplicateCardInfo;
		DuplicateCardInfo.CardNumber = Wallet->GetCards()[0].CardNumber;
		Wallet->DropCard(DuplicateCardInfo);
	}
}

void ACorePlayerController::WOB_TestDropCash()
{
	FBanknote Banknote;
	Banknote.Currency = ECurrency::Euro;
	Banknote.Denomination = 1;
	Wallet->DropCash(Banknote, 2);
}

void ACorePlayerController::WOB_ServerStop()
{
	ServerStop();
}

void ACorePlayerController::WOB_ServerSaveGame()
{
	ServerSaveGame();
}

void ACorePlayerController::WOB_CheatAddCents(const FString& CardNumber, int32 Cents)
{
	ServerCheatAddCents(CardNumber, Cents);
}
