// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "CoreCharacter.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"
#include "CorePlayerController.h"
#include "CorePlayerState.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/TextBlock.h"
#include "Components/TextRenderComponent.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "WorldOfBanks/Game/InteractableActor.h"
#include "WorldOfBanks/Core/CoreHUD.h"
#include "WorldOfBanks/UI/ShowText.h"
#include "WorldOfBanks/WorldOfBanks.h"

ACoreCharacter::ACoreCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a 1st person camera
	FirstPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCamera->SetupAttachment(RootComponent);
	FirstPersonCamera->bUsePawnControlRotation = true;

	PlayerNameWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("PlayerName");
	PlayerNameWidgetComponent->SetupAttachment(RootComponent);
}

//////////////////////////////////////////////////////////////////////////
// Input

void ACoreCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	// Set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ACoreCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACoreCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ACoreCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ACoreCharacter::LookUpAtRate);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ACoreCharacter::OnResetVR);
}

void ACoreCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld()->IsClient())
	{
		FTimerHandle Handle;
		GetWorldTimerManager().SetTimer(Handle, FTimerDelegate::CreateUObject(this, &ACoreCharacter::TrySetupPlayerNameWidget), 0.7, false);
	}
}

void ACoreCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TryAlignPlayerNameWidgetToLocalPlayer();
}

void ACoreCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ACoreCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ACoreCharacter::MoveForward(float Value)
{
	if (Controller != nullptr && Value != 0.0f)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ACoreCharacter::MoveRight(float Value)
{
	if (Controller != nullptr && Value != 0.0f)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ACoreCharacter::OnResetVR()
{
	// If WorldOfBanks is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in WorldOfBanks.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ACoreCharacter::TrySetupPlayerNameWidget()
{
	for (TActorIterator<ACoreCharacter> CharacterIterator(GetWorld()); CharacterIterator; ++CharacterIterator)
	{
		if (UShowText* TextWidget = Cast<UShowText>(CharacterIterator->PlayerNameWidgetComponent->GetWidget()))
		{
			if (const APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0))
			{
				const APawn* LocalPlayerPawn = PC->GetPawn();
				
				if (LocalPlayerPawn == *CharacterIterator)
				{
					TextWidget->SetText(FText::FromString(""));
				}
				else
				{
					ACorePlayerState* PS = CharacterIterator->GetPlayerState<ACorePlayerState>();
		
					if (PS)
					{
						TextWidget->SetText(FText::FromString(PS->GetPlayerName()));
					}
				}
			}
		}
		else
		{
			UWidget* Widget = PlayerNameWidgetComponent->GetWidget();
			UE_LOG(Log_Character, Warning, TEXT("%s: PlayerNameWidgetComponent does not contain UShowText. TextWidget = %s"),
				ANSI_TO_TCHAR(__FUNCTION__),
				Widget ? *Widget->GetName() : TEXT("nullptr"))
		}
	}
}

void ACoreCharacter::TryAlignPlayerNameWidgetToLocalPlayer()
{
	if (GetWorld()->IsClient())
	{
		if (const APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0))
		{
			const APawn* LocalPlayerPawn = PC->GetPawn();
			const FVector MeToLocalPlayer = LocalPlayerPawn->GetActorLocation() - PlayerNameWidgetComponent->GetComponentLocation();
			PlayerNameWidgetComponent->SetWorldRotation(MeToLocalPlayer.Rotation());
		}
	}
}