﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "ReplicationUtils.h"

int32& FindByBanknoteChecked(TArray<FMoneyInfo>& Array, const FBanknote& Banknote)
{
	int32* Value = FindByBanknote(Array, Banknote);
	check(Value);
	return *Value;
}

const int32& FindByBanknoteChecked(const TArray<FMoneyInfo>& Array, const FBanknote& Banknote)
{
	const int32* Value = FindByBanknote(Array, Banknote);
	check(Value);
	return *Value;
}

int32* FindByBanknote(TArray<FMoneyInfo>& Array, const FBanknote& Banknote)
{
	FMoneyInfo* MoneyInfo = Array.FindByPredicate(
		[Banknote](const FMoneyInfo& Info) -> bool
		{
			return Info.Key == Banknote;
		});

	return MoneyInfo ? &MoneyInfo->Value : nullptr;
}

const int32* FindByBanknote(const TArray<FMoneyInfo>& Array, const FBanknote& Banknote)
{
	const FMoneyInfo* MoneyInfo = Array.FindByPredicate(
		[Banknote](const FMoneyInfo& Info) -> bool
		{
			return Info.Key == Banknote;
		});

	return MoneyInfo ? &MoneyInfo->Value : nullptr;
}

int32& FindOrAddByBanknote(TArray<FMoneyInfo>& Array, const FBanknote& Banknote)
{
	FMoneyInfo* MoneyInfo = Array.FindByPredicate(
		[Banknote](const FMoneyInfo& Info) -> bool
		{
			return Info.Key == Banknote;
		});

	if (MoneyInfo)
	{
		return MoneyInfo->Value;
	}
	else
	{
		return Array.Add_GetRef({Banknote, 0}).Value;
	}
}

void RemoveByBanknote(TArray<FMoneyInfo>& Array, const FBanknote& Banknote)
{
	Array.RemoveAllSwap([Banknote](const FMoneyInfo& MoneyInfo) -> bool
	{
		return MoneyInfo.Key == Banknote;
	});
}
