// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "CoreGameState.generated.h"

class APickupCard;
class APickupCash;
class UBankInfo;
class UCardWidget;

UCLASS()
class WORLDOFBANKS_API ACoreGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	FORCEINLINE const TArray<TSoftObjectPtr<const UBankInfo>>& GetBanks() const { return Banks; }
	FORCEINLINE int32 GetInitialPINTriesAmount() { return InitialPINTriesAmount; }

	TSoftClassPtr<UCardWidget> GetRandomCardWidgetClass() const;

	FORCEINLINE TSoftClassPtr<APickupCard> GetPickupCardClass() const { return PickupCardClass; }
	FORCEINLINE TSoftClassPtr<APickupCash> GetPickupCashClass() const { return PickupCashClass; }
	
private:
	/** Assume, that this data is same on client and server, since will not be changed in runtime */
	UPROPERTY(EditDefaultsOnly)
	TArray<TSoftObjectPtr<const UBankInfo>> Banks;
	
	UPROPERTY(EditDefaultsOnly)
	int32 InitialPINTriesAmount = 3;

	UPROPERTY(EditDefaultsOnly)
	TArray<TSoftClassPtr<UCardWidget>> CardWidgetClasses;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<APickupCard> PickupCardClass;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<APickupCash> PickupCashClass;
};
