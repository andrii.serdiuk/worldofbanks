// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "CorePlayerState.generated.h"

UCLASS()
class WORLDOFBANKS_API ACorePlayerState : public APlayerState
{
	GENERATED_BODY()
	
};
