// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "CoreHUD.h"
#include "WorldOfBanks/Components/WalletComponent.h"
#include "WorldOfBanks/Components/PhoneComponent.h"
#include "WorldOfBanks/UI/WalletWidget.h"
#include "WorldOfBanks/UI/PhoneWidget.h"
#include "WorldOfBanks/UI/OneLineWidget.h"
#include "WorldOfBanks/UI/PINFailMiniGameWidget.h"

void ACoreHUD::ToggleWallet()
{
	if (!ensureAlwaysMsgf(!WalletWidgetClass.IsNull(), TEXT("PhoneWidgetClass is not set!")))
	{
		return;
	}

	// Collapse WalletWidget in case it is already shown
	if (WalletWidget && WalletWidget->GetVisibility() == ESlateVisibility::Visible)
	{
		WalletWidget->SetVisibility(ESlateVisibility::Collapsed);
		StartGamePlay();
		return;
	}

	// Get PhoneComponent from owning controller (our ACorePlayerController)
	UWalletComponent* WalletComponent = CastChecked<UWalletComponent>(GetOwningPlayerController()->GetComponentByClass(UWalletComponent::StaticClass()));

	// Create PhoneWidget if no widget is present
	if (!WalletWidget)
	{
		WalletWidget = CreateWidget<UWalletWidget>(GetOwningPlayerController(), WalletWidgetClass.LoadSynchronous());
		WalletWidget->AddToViewport();
	}

	WalletWidget->Update();
	WalletWidget->SetVisibility(ESlateVisibility::Visible);
	StopGamePlay();
}

void ACoreHUD::TogglePhone()
{
	if (!ensureAlwaysMsgf(!PhoneWidgetClass.IsNull(), TEXT("PhoneWidgetClass is not set!")))
	{
		return;
	}

	// Collapse PhoneWidget in case it is already shown
	if (PhoneWidget && PhoneWidget->GetVisibility() == ESlateVisibility::HitTestInvisible)
	{
		PhoneWidget->SetVisibility(ESlateVisibility::Collapsed);
		return;
	}

	// Get PhoneComponent from owning controller (our ACorePlayerController)
	UPhoneComponent* PhoneComponent = CastChecked<UPhoneComponent>(
		GetOwningPlayerController()->GetComponentByClass(UPhoneComponent::StaticClass()));
	
	// Create PhoneWidget if no widget is present
	if (!PhoneWidget)
	{
		PhoneWidget = CreateWidget<UPhoneWidget>(GetOwningPlayerController(), PhoneWidgetClass.LoadSynchronous());
		PhoneWidget->InitWidget(PhoneComponent->GetPhoneNumber());
		PhoneWidget->AddToViewport();
	}

	PhoneWidget->SetVisibility(ESlateVisibility::HitTestInvisible);
	PhoneWidget->RebuildPhoneMessages(PhoneComponent->GetMessages());
}

void ACoreHUD::UpdatePhoneWidget(const TArray<FString>& Messages)
{
	if (PhoneWidget && PhoneWidget->GetVisibility() != ESlateVisibility::Collapsed)
	{
		PhoneWidget->RebuildPhoneMessages(Messages);
	}
}

void ACoreHUD::OnStartDeviceInteraction()
{
	StopGamePlay();
}

void ACoreHUD::OnEndDeviceInteraction()
{
	StartGamePlay();
}

void ACoreHUD::StopGamePlay()
{
	if (++NumOfStopGamePlayRequestsActive == 1)
	{
		GetOwningPlayerController()->SetInputMode(FInputModeGameAndUI());	
		GetOwningPlayerController()->SetShowMouseCursor(true);
	}
}

void ACoreHUD::StartGamePlay()
{
	if (--NumOfStopGamePlayRequestsActive == 0)
	{
		GetOwningPlayerController()->SetInputMode(FInputModeGameOnly());
		GetOwningPlayerController()->SetShowMouseCursor(false);
	}
}

void ACoreHUD::SpawnMinigameWidget()
{
	if (ensure(!PINFailMiniGameWidgetClass.IsNull()))
	{
		PINFailMiniGameWidget = CreateWidget<UPINFailMiniGameWidget>(GetOwningPlayerController(), PINFailMiniGameWidgetClass.LoadSynchronous());
		PINFailMiniGameWidget->AddToViewport();
	}
}

void ACoreHUD::SpawnWastedWidget()
{
	if (ensure(!WastedWidgetClass.IsNull()))
	{
		UUserWidget* WastedWidget = CreateWidget(GetOwningPlayerController(), WastedWidgetClass.LoadSynchronous());
		WastedWidget->AddToViewport();
	}
}

void ACoreHUD::ShowOneLineWidget(const FString& StringToShow)
{
	// Create new container if it doesn't exist
	if (!OneLinerContainerWidget)
	{
		if (!ensure(!OneLinerContainerWidgetClass.IsNull()))
		{
			// No widget will be spawned if Container class is null
			return;
		}
		
		OneLinerContainerWidget = CreateWidget<UOneLineWidgetContainer>(GetOwningPlayerController(), OneLinerContainerWidgetClass.LoadSynchronous());
		check(OneLinerContainerWidget);

		OneLinerContainerWidget->AddToViewport();
	}

	OneLinerContainerWidget->AddChildOneLiner(StringToShow);
}
