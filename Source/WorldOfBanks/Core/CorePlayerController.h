// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "WorldOfBanks/Bank/Banknote.h"
#include "CorePlayerController.generated.h"

class ACoreCharacter;
class AInteractableActor;
class UWidgetComponent;
class UWalletComponent;
class UPhoneComponent;
struct FPlayerSaveData;

UCLASS()
class WORLDOFBANKS_API ACorePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ACorePlayerController(const FObjectInitializer& ObjectInitializer);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(Client, Reliable)
	void ClientWelcomeMessage(const FString& NewPlayerName);

	UFUNCTION(Client, Reliable)
	void ClientLogoutMessage(const FString& PlayerName);

	/** This function calls on Client to notify that InteractableActor is already occupied by someone */
	UFUNCTION(Client, Reliable)
	void ClientNotifyAlreadyOccupied();

	UFUNCTION(Client, Reliable)
	void ClientNotifyFullWallet(const FBanknote& Banknote) const;
	
	FORCEINLINE UPhoneComponent* GetPhone() const { return Phone; }
	FORCEINLINE UWalletComponent* GetWallet() const {return Wallet;}

	virtual void Tick(float DeltaTime) override;
	
	/** Load ACorePlayerController from FPlayerSaveData */
	ACorePlayerController& operator<<(const FPlayerSaveData& SaveData);

	/** Load ACorePlayerController to FPlayerSaveData */
	const ACorePlayerController& operator>>(FPlayerSaveData& SaveData) const;

	UPROPERTY(Transient, Replicated)
	class ABankDevice* CurrentDevice;
	
	/** Called on client after possessing a pawn. Cache CoreCharacter here */
	virtual void AcknowledgePossession(APawn* InPawn) override;

	void SetInteractionWidgetVisibility(bool bHidden);

	void LaunchPinFailedMinigame();
	
protected:
	virtual void BeginPlay() override;

	void TickInteraction();

	/*virtual void OnPossess(APawn* InPawn) override;*/
	virtual void SetupInputComponent() override;

private:
	UFUNCTION(Client, Reliable)
	void ClientOnMinigameLaunched();
	void ClientOnMinigameLaunched_Implementation();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerOnMinigameFinished(bool bResult);
	void ServerOnMinigameFinished_Implementation(bool bResult);
	bool ServerOnMinigameFinished_Validate(bool bResult);

	bool bMinigameStarted = false;

	/** Should be used only on Client! Use GetPawn() for gameplay logic with validation */
	UPROPERTY(Transient)
	ACoreCharacter* CoreCharacter;
	
	AInteractableActor* GetBestInteractable() const;
	UPROPERTY(EditDefaultsOnly, Transient)
	float InteractionDistance = 200;
	
	UPROPERTY(Transient)
	TArray<AInteractableActor*> ConeCollidingInteractables;
	
	UPROPERTY(Transient)
	TArray<FHitResult> PotentialInteractables;
	
	UPROPERTY(Transient)
	AInteractableActor* InteractionCandidate;

	// In order to display interaction button in world space
	UPROPERTY(EditDefaultsOnly, Transient, Category = Interactable)
	UWidgetComponent* InteractionWidgetComponent;

	/** Input event for Interact action */
	void Interact();

	/** Called from Client on Server (process interaction on Server) */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerInteractWith(AInteractableActor* Interactable);
	void ServerInteractWith_Implementation(AInteractableActor* Interactable);
	bool ServerInteractWith_Validate(AInteractableActor* Interactable);
	
	void ClientWelcomeMessage_Implementation(const FString& NewPlayerName);
	void ClientLogoutMessage_Implementation(const FString& PlayerName);
	void ClientNotifyAlreadyOccupied_Implementation();

	UPROPERTY(VisibleAnywhere, Category = Bank)
	UWalletComponent* Wallet;

	UPROPERTY(VisibleAnywhere, Category = Bank)
	UPhoneComponent* Phone;

	void ToggleWallet();
	
	void TogglePhone();

	// Cheat functions
public:
	UFUNCTION(Exec)
	void WOB_SendPhoneMessage(int64 PhoneNumber, const FString& Message);

	UFUNCTION(Exec)
	void WOB_TestDropCard();

	UFUNCTION(Exec)
	void WOB_TestDropCash();

	UFUNCTION(Exec)
	void WOB_ServerStop();

	UFUNCTION(Exec)
	void WOB_ServerSaveGame();

	UFUNCTION(Exec)
	void WOB_CheatAddCents(const FString& CardNumber, int32 Cents);
	
private:
	UFUNCTION(Server, Reliable)
	void ServerSendPhoneMessage(int64 PhoneNumber, const FString& Message);
	void ServerSendPhoneMessage_Implementation(int64 Phone, const FString& Message);

	UFUNCTION(Server, Reliable)
	void ServerStop();
	void ServerStop_Implementation();

	UFUNCTION(Server, Reliable)
	void ServerSaveGame();
	void ServerSaveGame_Implementation();

	UFUNCTION(Server, Reliable)
	void ServerCheatAddCents(const FString& CardNumber, int32 Cents);
	void ServerCheatAddCents_Implementation(const FString& CardNumber, int32 Cents);
};

/** Executes member functions of ACorePlayerControllers with different return types and parameters */
template<typename TReturnType, typename ...TTypesParams, typename ...TTypesValues>
void ExecuteOnAllControllers(UWorld* World, TReturnType (ACorePlayerController::*Function) (TTypesParams...), TTypesValues&& ...Args)
{
	for (FConstPlayerControllerIterator Iter = World->GetPlayerControllerIterator(); Iter; ++Iter)
	{
		ACorePlayerController* Controller = CastChecked<ACorePlayerController>(*Iter);
		(Controller->*Function)(Args...);
	}
}