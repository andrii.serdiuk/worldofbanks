// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "CoreGameMode.h"
#include "CoreGameState.h"
#include "CorePlayerController.h"
#include "CorePlayerState.h"
#include "CoreSaveGame.h"
#include "EngineUtils.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "Kismet/GameplayStatics.h"
#include "WorldOfBanks/Bank/ProcessingCenter.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "WorldOfBanks/Components/WalletComponent.h"
#include "WorldOfBanks/Pickup/PickupCard.h"
#include "WorldOfBanks/Pickup/PickupCash.h"

void ACoreGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
	
	if (!ErrorMessage.IsEmpty())
	{
		return;
	}

	if (!UGameplayStatics::HasOption(Options, FConnectOptionKeys::Name)
		|| !UGameplayStatics::HasOption(Options, FConnectOptionKeys::Password))
	{
		UE_LOG(Log_Connection, Error, TEXT("%s tried to connect to server without ?%s or ?%s"),
			*Address, *FConnectOptionKeys::Name, *FConnectOptionKeys::Password);
		ErrorMessage = FConnectErrors::NoOptionKeys;
		return;
	}
	
	FString PlayerName = UGameplayStatics::ParseOption(Options, FConnectOptionKeys::Name);
	PlayerName.ToUpperInline(); // Name is case-insensitive
	
	const FString PlayerPass = UGameplayStatics::ParseOption(Options, FConnectOptionKeys::Password);

	if (TryLoginWithCredentials(PlayerName, PlayerPass))
	{
		UE_LOG(Log_Connection, Log, TEXT("%s authenticated successfully"), *Address);
	}
	else
	{
		UE_LOG(Log_Connection, Warning, TEXT("%s: authentication failed. Player %s exists, but password is incorrect"),
			*Address, *PlayerName);
		ErrorMessage = FConnectErrors::InvalidPassword;
	}
}

APlayerController* ACoreGameMode::Login(UPlayer* NewPlayer, ENetRole InRemoteRole, const FString& Portal,
	const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	APlayerController* Controller = Super::Login(NewPlayer, InRemoteRole, Portal, Options, UniqueId, ErrorMessage);

	ACorePlayerController* CoreController = Cast<ACorePlayerController>(Controller);
	if (!CoreController)
	{
		ErrorMessage = FConnectErrors::BadPlayerControllerClass;
		return nullptr;
	}
	
	if (!ErrorMessage.IsEmpty())
	{
		return nullptr;
	}
	
	checkf(UGameplayStatics::HasOption(Options, FConnectOptionKeys::Name), TEXT("Option not exists, but checked in PreLogin"));
	FString PlayerName = UGameplayStatics::ParseOption(Options, FConnectOptionKeys::Name);
	PlayerName.ToUpperInline(); // Name is case-insensitive
	
	FServerPlayerInfo& PlayerInfo = GetPlayerInfoByName(PlayerName);
	if (PlayerInfo.bConnectedToServer)
	{
		// On this step, it means that this player has already joined server and plays right now.
		// We should block this Login.
		ErrorMessage = FConnectErrors::AlreadyPlaying;
		Controller->Destroy();
		return nullptr;
	}

	PlayerInfo.bConnectedToServer = true;
	PlayerInfo.AssociatedController = Controller;
	Controller->SetName(PlayerName);

	UE_LOG(Log_Connection, Log, TEXT("Player %s passed Login function"), *PlayerName);
	
	return Controller;
}

void ACoreGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	FServerPlayerInfo* PlayerInfo = GetPlayerInfoByController(NewPlayer);
	checkf(PlayerInfo, TEXT("PostLogin: can't find PlayerInfo by APlayerController"));

	// Give starter pack and setup player if it is their first login
	if (PlayerInfo->SaveData.bNewPlayer)
	{
		InitialPlayerSetup(*PlayerInfo);
	}

	ACorePlayerController& Controller = *CastChecked<ACorePlayerController>(NewPlayer);
	Controller << PlayerInfo->SaveData;
	PlayerInfo->SaveData.bNewPlayer = false;

	// Send "greetings" message to all clients
	ExecuteOnAllControllers(GetWorld(), &ACorePlayerController::ClientWelcomeMessage, PlayerInfo->PlayerName);
}

void ACoreGameMode::Logout(AController* Exiting)
{
	ACorePlayerController* CorePC = CastChecked<ACorePlayerController>(Exiting);
	
	FServerPlayerInfo* PlayerInfo = GetPlayerInfoByController(CorePC);
	UE_CLOG(PlayerInfo, Log_Connection, Warning, TEXT("PostLogin: can't find PlayerInfo by APlayerController 0x%x"), Exiting);

	// Release current owning device when logging out
	if (CorePC->CurrentDevice)
	{
		CorePC->CurrentDevice->ReleaseDevice(CorePC);
	}
	
	if (PlayerInfo)
	{		
		// Save info about player
		*CorePC >> PlayerInfo->SaveData;
		
		// Mark player as disconnected
		PlayerInfo->bConnectedToServer = false;
		PlayerInfo->AssociatedController = nullptr;

		// Send "good bye" message to all clients
		ExecuteOnAllControllers(GetWorld(), &ACorePlayerController::ClientLogoutMessage, PlayerInfo->PlayerName);
	}
	Super::Logout(Exiting);
}

void ACoreGameMode::ExitGame()
{
	UKismetSystemLibrary::QuitGame(this, nullptr, EQuitPreference::Quit, true);
}

void ACoreGameMode::BeginPlay()
{
	Super::BeginPlay();

	// Initialize gameplay modules
	ProcessingCenter = UProcessingCenter::CreateInstance(GetWorld());

	LoadGame();

	// Setup timer for automatic game state saving
	FTimerHandle TimerHandle; // no need to save TimerHandle in class, since our timer is infinite
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ACoreGameMode::SaveGame, AutoSaveIntervalSeconds, true);
}

void ACoreGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	SaveGame();

	Super::EndPlay(EndPlayReason);
}

void ACoreGameMode::SaveGame()
{
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::SaveGame() : saving has begun!"));
	UCoreSaveGame* SaveObject = NewObject<UCoreSaveGame>(GetTransientPackage(), UCoreSaveGame::StaticClass());

	// Update info about each connected player
	for (FServerPlayerInfo& ServerPlayerInfo : Players)
	{
		if (ACorePlayerController* PC = Cast<ACorePlayerController>(ServerPlayerInfo.AssociatedController))
		{
			*PC >> ServerPlayerInfo.SaveData;
			UE_LOG(Log_Save, Log, TEXT("ACoreGameMode::SaveGame() : updated info about player %s!"), *PC->GetName());
		}
	}
	
	SaveObject->Players = Players;
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::SaveGame() : saved %d players!"), Players.Num());
	
	SaveObject->UsedPhoneNumbers = UsedPhoneNumbers;

	*ProcessingCenter >> SaveObject;
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::SaveGame() : saved processing center!"));

	// Iterate through APickupCards in world and add them to save data
	for (TActorIterator<APickupCard> CardIterator(GetWorld()); CardIterator; ++CardIterator)
	{
		FPickupCardSaveData CardSaveData;
		
		// Dereference iterator -> dereference Actor ptr -> use operator>>
		**CardIterator >> CardSaveData;
		UE_LOG(Log_Save, Log, TEXT("ACoreGameMode::SaveGame() : saved card %s!"), *CardSaveData.CardInfo.CardNumber);

		SaveObject->PickupCards.Add(CardSaveData);
	}

	// BankDevices could be occupied when creating game save. And cards could still remain in devices.
	// Iterate through devices and save cards as they would been ejected (just not to waste card)
	for (TActorIterator<ABankDevice> DeviceIterator(GetWorld()); DeviceIterator; ++DeviceIterator)
	{
		const FCardInfo& InsertedCard = (*DeviceIterator)->GetInsertedCard();
		if (InsertedCard != FCardInfo::InvalidCard)
		{
			FPickupCardSaveData CardSaveData;
			CardSaveData.CardInfo = InsertedCard;
			CardSaveData.PickupActorClass = InsertedCard.CardClass;
			CardSaveData.ActorTransform = (*DeviceIterator)->GetCardSocketTransform();

			UE_LOG(Log_Save, Log, TEXT("ACoreGameMode::SaveGame() : saved card %s (as stuck in Device)!"), *CardSaveData.CardInfo.CardNumber);
			SaveObject->PickupCards.Add(CardSaveData);
		}
	}

	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::SaveGame() : saved %d cards!"), SaveObject->PickupCards.Num());

	// Iterate through APickupCash actors in world and add them to save data
	for (TActorIterator<APickupCash> CashIterator(GetWorld()); CashIterator; ++CashIterator)
	{
		FPickupCashSaveData CashSaveData;
		
		// Dereference iterator -> dereference Actor ptr -> use operator>>
		**CashIterator >> CashSaveData;
		UE_LOG(Log_Save, Log, TEXT("ACoreGameMode::SaveGame() : saved cash %s!"), *((*CashIterator)->GetName()));

		SaveObject->PickupCash.Add(CashSaveData);
	}
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::SaveGame() : saved %d cash actors!"), SaveObject->PickupCash.Num());

	UGameplayStatics::SaveGameToSlot(SaveObject, FSaveOptions::ServerSlotName, FSaveOptions::ServerUserID);
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::SaveGame() : saving has finished!"));
}

void ACoreGameMode::LoadGame()
{
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::LoadGame() : loading has begun!"));
	UCoreSaveGame* SaveObject = Cast<UCoreSaveGame>(UGameplayStatics::LoadGameFromSlot(FSaveOptions::ServerSlotName, FSaveOptions::ServerUserID));
	if (!SaveObject)
	{
		UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::LoadGame() : no save game found!"));
		return;
	}

	// Can move data safely since no one will use these fields of SaveObject anymore
	UsedPhoneNumbers = MoveTemp(SaveObject->UsedPhoneNumbers);
	Players = MoveTemp(SaveObject->Players);
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::LoadGame() : loaded %d players!"), Players.Num());
	
	*ProcessingCenter << SaveObject;
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::LoadGame() : loaded processing center!"));

	UWorld* CurrentWorld = GetWorld();
	check(IsValid(CurrentWorld));

	// Iterate through save data of cards, spawn APickupCard actors and initialize them 
	for (const FPickupCardSaveData& CardSaveData : SaveObject->PickupCards)
	{
		APickupCard* Card = APickupCard::SpawnFromSave(CurrentWorld, CardSaveData);
		UE_LOG(Log_Save, Log, TEXT("ACoreGameMode::LoadGame() : loaded card %s!"), *Card->GetName());
	}
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::LoadGame() : loaded %d cards!"), SaveObject->PickupCards.Num());

	// Iterate through save data of cash, spawn APickupCash actors and initialize them 
	for (const FPickupCashSaveData& CashSaveData : SaveObject->PickupCash)
	{
		APickupCash* Cash = APickupCash::SpawnFromSave(CurrentWorld, CashSaveData);
		UE_LOG(Log_Save, Log, TEXT("ACoreGameMode::LoadGame() : loaded cash %s!"), *Cash->GetName());
	}
	UE_LOG(Log_Save, Warning, TEXT("ACoreGameMode::LoadGame() : loaded %d cash actors!"), SaveObject->PickupCash.Num());
}

bool ACoreGameMode::TryLoginWithCredentials(const FString& Name, const FString& Password)
{
	for (const FServerPlayerInfo& PlayerInfo : Players)
	{
		if (PlayerInfo.PlayerName == Name)
		{
			// In case we have info about player with PlayerName = Name, we should check that password is correct
			return PlayerInfo.PlayerPassword == Password;
		}
	}

	// If we don't have player with PlayerName = Name, create new "account"
	Players.Add(FServerPlayerInfo(Name, Password));
	return true;
}

void ACoreGameMode::InitialPlayerSetup(FServerPlayerInfo& ServerPlayerInfo)
{
	FPlayerPhoneSaveData& Phone = ServerPlayerInfo.SaveData.Phone;
	FPlayerWalletSaveData& Wallet = ServerPlayerInfo.SaveData.Wallet;
	
	// Valid phone numbers: from +11 (111) 111-11-11 to +99 (999) 999-99-99
	Phone.PhoneNumber = GenerateUniquePhoneNumber();
	Phone.Messages = { "Notification: Welcome to World Of Banks!" };

	FBankAccount Account;
	Account.ClientName = ServerPlayerInfo.PlayerName;
	Account.PhoneNumber = Phone.PhoneNumber;

	ACoreGameState* CoreGS = GetGameState<ACoreGameState>();
	check(CoreGS);

	const int32 BankNum = CoreGS->GetBanks().Num();
	check(BankNum > 0);
	
	const int32 RandomBankID = FMath::RandRange(0, BankNum - 1);
	const TSoftObjectPtr<const UBankInfo>& RandomBank = CoreGS->GetBanks()[RandomBankID];

	// Register Account in Processing Center
	const FBankAccount& RegisteredAccount = ProcessingCenter->RegisterNewAccount(RandomBank, Account);

	// Register cards of all currencies in Processing Center for a specific account
	const FCardInfo& CardUSD = ProcessingCenter->RegisterNewCard(RandomBank, RegisteredAccount, ECurrency::Dollar);
	const FCardInfo& CardUAH = ProcessingCenter->RegisterNewCard(RandomBank, RegisteredAccount, ECurrency::Hryvnia);
	const FCardInfo& CardEUR = ProcessingCenter->RegisterNewCard(RandomBank, RegisteredAccount, ECurrency::Euro);

	// Add some Sum to player cards (as a Starter Pack)
	constexpr int32 StarterSumInUAH = 300000; // this sum will be added to all cards (in equivalent) == 3000.00 UAH
	ProcessingCenter->AddFundsToCard(CardUSD, ECurrency::Hryvnia, StarterSumInUAH);
	ProcessingCenter->AddFundsToCard(CardUAH, ECurrency::Hryvnia, StarterSumInUAH);
	ProcessingCenter->AddFundsToCard(CardEUR, ECurrency::Hryvnia, StarterSumInUAH);

	Wallet.WalletCards = { CardUSD, CardUAH, CardEUR };
}

FServerPlayerInfo& ACoreGameMode::GetPlayerInfoByName(const FString& Name)
{
	FServerPlayerInfo* PlayerInfo = Players.FindByPredicate([&Name](const FServerPlayerInfo& Candidate)
	{
		return Candidate.PlayerName == Name;
	});

	checkf(PlayerInfo, TEXT("PlayerInfo should exist (existed earlier or created in PreLogin)"));

	return *PlayerInfo;
}

FServerPlayerInfo* ACoreGameMode::GetPlayerInfoByController(APlayerController* Player)
{
	return Players.FindByPredicate([Player](const FServerPlayerInfo& Candidate)
	{
		return Candidate.AssociatedController == Player;
	});
}

int64 ACoreGameMode::GenerateUniquePhoneNumber()
{
	constexpr int32 MaxNumOfTries = 1000;
	for (int32 i = 0; i < MaxNumOfTries; ++i)
	{
		// Valid phone numbers: from +11 (111) 111-11-11 to +99 (999) 999-99-99
		int64 PotentialNumber = FMath::RandRange(1000, 9999); // first digit should not be 0
		for (int32 j = 0; j < 2; ++j)
		{
			PotentialNumber *= 10000;
			PotentialNumber += FMath::RandRange(0000, 9999);
		}
		
		if (!UsedPhoneNumbers.Contains(PotentialNumber))
		{
			UsedPhoneNumbers.Add(PotentialNumber);
			return PotentialNumber;
		}
	}
	
	// Yes-yes, this algorithm doesn't guarantee that a valid card number will be generated.
	// However, Chance of it is very-very small and this gonna never happen in reality.
	UE_LOG(Log_Processing, Fatal,
		TEXT("If you see this, you are very lucky! Contact kyryl.sydorov@ukma.edu.ua to get your prize: 2 000 UAH"));
	return -1;
}
