// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "CoreHUD.generated.h"

class UWalletWidget;
class UPhoneWidget;
class UPINFailMiniGameWidget;
class UOneLineWidgetContainer;

UCLASS()
class WORLDOFBANKS_API ACoreHUD : public AHUD
{
	GENERATED_BODY()

public:
	FORCEINLINE UWalletWidget* GetWalletWidget() const { return WalletWidget; }
	FORCEINLINE UPhoneWidget* GetPhoneWidget() const { return PhoneWidget; };
	FORCEINLINE UPINFailMiniGameWidget* GetMinigameWidget() const { return PINFailMiniGameWidget; }

	/** Toggles visibility of WalletWidget */
	UFUNCTION()
	void ToggleWallet();

	/** Toggles visibility of PhoneWidget */
	void TogglePhone();
	
	/** Updates messages in PhoneWidget ONLY when widget is visible */
	void UpdatePhoneWidget(const TArray<FString>& Messages);

	void OnStartDeviceInteraction();
	void OnEndDeviceInteraction();

	void SpawnMinigameWidget();

	void SpawnWastedWidget();

	void ShowOneLineWidget(const FString& StringToShow);
	
private:
	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<UWalletWidget> WalletWidgetClass;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<UPhoneWidget> PhoneWidgetClass;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<UPINFailMiniGameWidget> PINFailMiniGameWidgetClass;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<UUserWidget> WastedWidgetClass;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<UOneLineWidgetContainer> OneLinerContainerWidgetClass;

	UPROPERTY(Transient)
	UWalletWidget* WalletWidget;

	UPROPERTY(Transient)
	UPhoneWidget* PhoneWidget;

	UPROPERTY(Transient)
	UPINFailMiniGameWidget* PINFailMiniGameWidget;

	UPROPERTY(Transient)
	UOneLineWidgetContainer* OneLinerContainerWidget;

	/** Turn Game and UI input and show cursor */
	void StopGamePlay();

	/** Turn Game only input and hide cursor */
	void StartGamePlay();

	// in order to resolve conflicts when multiple widgets want to stop/start
	uint8 NumOfStopGamePlayRequestsActive = 0;
};
