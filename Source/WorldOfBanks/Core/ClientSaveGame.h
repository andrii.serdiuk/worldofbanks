﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "ClientSaveGame.generated.h"

/**
 * Same Game object for client side settings (e.g. Connection menu)
 */
UCLASS()
class WORLDOFBANKS_API UClientSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY()
	FString Host;

	UPROPERTY()
	FString Login;

	UPROPERTY()
	FString Password;
};
