// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CoreGameInstance.generated.h"

UCLASS()
class WORLDOFBANKS_API UCoreGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	/** Builds and executes connection request. */
	void ConnectToServer(const FString& Host, const FString& Name, const FString& Password);
};
