// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "CoreGameState.h"

TSoftClassPtr<UCardWidget> ACoreGameState::GetRandomCardWidgetClass() const
{
	check(CardWidgetClasses.Num() > 0);
	int32 ID = FMath::RandRange(0, CardWidgetClasses.Num() - 1);
	return CardWidgetClasses[ID];
}
