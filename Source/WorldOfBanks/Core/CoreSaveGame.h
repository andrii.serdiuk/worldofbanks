﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SaveObjects.h"
#include "ServerPlayerInfo.h"
#include "CoreSaveGame.generated.h"

/**
 * CoreSaveGame is a class that stores information about state of the game. You can Save your data or Load from save
 */
UCLASS()
class WORLDOFBANKS_API UCoreSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	/** Information about accounts of banks (from UProcessingCenter) */
	UPROPERTY()
	TMap<TSoftObjectPtr<const UBankInfo>, FBankData> Banks;

	/** Server info about players (wallets, phones etc) */
	UPROPERTY()
	TArray<FServerPlayerInfo> Players;

	/** APickupCards that are placed/dropped in world */
	UPROPERTY()
	TArray<FPickupCardSaveData> PickupCards;

	/** APickupCash actors that are placed/dropped in world */
	UPROPERTY()
	TArray<FPickupCashSaveData> PickupCash;
	
	/** Information about used card numbers (from UProcessingCenter) */
	UPROPERTY()
	TSet<int64> UsedCardNumbers;

	/** Information about used phone numbers (from ACoreGameMode) */
	UPROPERTY()
	TSet<int64> UsedPhoneNumbers;
};
