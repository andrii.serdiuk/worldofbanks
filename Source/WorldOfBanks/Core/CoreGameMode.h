// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ServerPlayerInfo.h"
#include "CoreGameMode.generated.h"

class UProcessingCenter;
class ACorePlayerController;

UCLASS()
class WORLDOFBANKS_API ACoreGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	/** Authentication logic.
	 *	If user played on server earlier, we have to check password correctness,
	 *	otherwise create new "account" with credentials: PlayerName and Password
	 */
	virtual void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;

	/** Login logic.
	 *	On this step we spawn APlayerController and associate it with appropriate FServerPlayerInfo.
	 *	If player with this PlayerName is already playing - do not allow connection
	 */
	virtual APlayerController* Login(UPlayer* NewPlayer, ENetRole InRemoteRole, const FString& Portal, const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;

	/** Success login of NewPlayer.
	 *	On this step, we should configure APlayerController of new player, spawn player pawn etc.
	 *	All auth checks have been completed in PreLogin and Login
	 */
	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void Logout(AController* Exiting) override;

	FORCEINLINE UProcessingCenter* GetProcessingCenter() const { return ProcessingCenter; }

	void ExitGame();

#if !(UE_BUILD_SHIPPING)
	// Should not be included in shipping
	FORCEINLINE void DebugSaveGame()
	{
		SaveGame();
	}
#endif
	
protected:
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
private:
	void SaveGame();

	void LoadGame();
	
	/** Checks whether we have "account" for this PlayerName. If we have, validate password, otherwise create new */
	bool TryLoginWithCredentials(const FString& Name, const FString& Password);

	/** Sets up player, gives starter pack of resources */
	void InitialPlayerSetup(FServerPlayerInfo& ServerPlayerInfo);

	/** Finds "account" by PlayerName */
	FServerPlayerInfo& GetPlayerInfoByName(const FString& Name);

	FServerPlayerInfo* GetPlayerInfoByController(APlayerController* Player);
	
	UPROPERTY()
	TArray<FServerPlayerInfo> Players;

	UPROPERTY(Transient)
	UProcessingCenter* ProcessingCenter;

	UPROPERTY()
	TSet<int64> UsedPhoneNumbers;

	UPROPERTY(EditDefaultsOnly)
	float AutoSaveIntervalSeconds = 120;

	int64 GenerateUniquePhoneNumber();
};