// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CoreCharacter.generated.h"

class UTextRenderComponent;
class USpringArmComponent;
class UCameraComponent;
class UPhoneComponent;
class UWalletComponent;
class AInteractableActor;
class UWidgetComponent;

UCLASS(Abstract)
class ACoreCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ACoreCharacter(const FObjectInitializer& ObjectInitializer);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
	void MoveForward(float Value);
	void MoveRight(float Value);

	/** @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate */
	void TurnAtRate(float Rate);
	
	/** @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate */
	void LookUpAtRate(float Rate);
	
	/** Resets HMD orientation in VR. */
	void OnResetVR();

	void TrySetupPlayerNameWidget();
	void TryAlignPlayerNameWidgetToLocalPlayer();

public:
	/// Selectors for components of ACoreCharacter
	
	FORCEINLINE UCameraComponent* GetFollowCamera() const { return FirstPersonCamera; }

private:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(EditAnywhere, Category = Camera)
	float BaseTurnRate = 45.f;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(EditAnywhere, Category = Camera)
	float BaseLookUpRate = 45.f;
	
	/// Components of ACoreCharacter
	/** 1st person camera, from which we are looking into World when possessed */
	UPROPERTY(VisibleAnywhere, Category = Camera)
	UCameraComponent* FirstPersonCamera;

	UPROPERTY(VisibleAnywhere)
	UWidgetComponent* PlayerNameWidgetComponent;
};
