﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/Bank/Banknote.h"
#include "ReplicationUtils.generated.h"

/**
 * There is an Engine problem with replicating TMaps or TSets. Only TArrays are supported.
 * Original banknote information in UWalletComponent: TMap<FBanknote, int32>,
 * New banknote information is TArray<FMoneyInfo>. So, have to realize some functions to work with
 */
USTRUCT()
struct WORLDOFBANKS_API FMoneyInfo
{
	GENERATED_BODY()
	
	UPROPERTY()
	FBanknote Key;

	UPROPERTY()
	int32 Value = 0;
};

int32& FindByBanknoteChecked(TArray<FMoneyInfo>& Array, const FBanknote& Banknote);
const int32& FindByBanknoteChecked(const TArray<FMoneyInfo>& Array, const FBanknote& Banknote);

int32* FindByBanknote(TArray<FMoneyInfo>& Array, const FBanknote& Banknote);
const int32* FindByBanknote(const TArray<FMoneyInfo>& Array, const FBanknote& Banknote);

int32& FindOrAddByBanknote(TArray<FMoneyInfo>& Array, const FBanknote& Banknote);
void RemoveByBanknote(TArray<FMoneyInfo>& Array, const FBanknote& Banknote);