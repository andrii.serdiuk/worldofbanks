﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "CurrencyComboBoxWidget.h"

#include "WorldOfBanks/Bank/Currency.h"

void UCurrencyComboBoxWidget::Init()
{
	for (int8 e = static_cast<int8>(ECurrency::MIN) + 1; e < static_cast<int8>(ECurrency::MAX); ++e)
	{
		AddOption(CurrencyNames.FindChecked(static_cast<ECurrency>(e)));
	}

	SetCurrency(ECurrency::Hryvnia);
}

void UCurrencyComboBoxWidget::SetCurrency(ECurrency Currency)
{
	SetSelectedOption(CurrencyNames.FindChecked(Currency));
}

ECurrency UCurrencyComboBoxWidget::GetCurrency() const
{
	return static_cast<ECurrency>(GetSelectedIndex() + static_cast<int8>(ECurrency::MIN) + 1);
}
