// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "WorldOfBanks/UI/ConnectionMenu.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Kismet/GameplayStatics.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "WorldOfBanks/Core/ClientSaveGame.h"
#include "WorldOfBanks/Core/CoreGameInstance.h"

void UConnectionMenu::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	// Bind to the event of button click
	ConnectButton->OnClicked.AddDynamic(this, &UConnectionMenu::OnConnectPressed);

	GetOwningPlayer()->SetShowMouseCursor(true);

	LoadData();
}

void UConnectionMenu::OnConnectPressed()
{
	// Retrieving strings from editable text boxes
	const FString HostAddress = Host->GetText().ToString();
	const FString Name = PlayerName->GetText().ToString();
	const FString Password = PlayerPassword->GetText().ToString();

	SaveData(HostAddress, Name, Password);
	
	UE_CLOG(HostAddress.IsEmpty(), Log_UI, Error, TEXT("Host Address is empty!"));
	UE_CLOG(Name.IsEmpty(), Log_UI, Error, TEXT("Player name is empty!"));
	UE_CLOG(Password.IsEmpty(), Log_UI, Error, TEXT("Player password is empty!"));

	// Try connect only if required parameters are not empty
	if (!(HostAddress.IsEmpty() || Name.IsEmpty() || Password.IsEmpty()))
	{
		UCoreGameInstance* GameInstance = GetGameInstance<UCoreGameInstance>();
		GameInstance->ConnectToServer(HostAddress, Name, Password);
	}
}

void UConnectionMenu::LoadData()
{
	UClientSaveGame* ClientSave = Cast<UClientSaveGame>(UGameplayStatics::LoadGameFromSlot(FSaveOptions::ClientSlotName, FSaveOptions::ClientUserID));

	const FString HostAddress = ClientSave ? ClientSave->Host : FSaveOptions::DefaultHost;
	const FString Name = ClientSave ? ClientSave->Login : FSaveOptions::DefaultLogin;
	const FString Password = ClientSave ? ClientSave->Password : FSaveOptions::DefaultPassword;

	Host->SetText(FText::FromString(HostAddress));
	PlayerName->SetText(FText::FromString(Name));
	PlayerPassword->SetText(FText::FromString(Password));
}

void UConnectionMenu::SaveData(const FString& HostAddress, const FString& Name, const FString& Password)
{
	UClientSaveGame* ClientSave = NewObject<UClientSaveGame>(GetTransientPackage(), UClientSaveGame::StaticClass());

	ClientSave->Host = HostAddress;
	ClientSave->Login = Name;
	ClientSave->Password = Password;
	
	UGameplayStatics::SaveGameToSlot(ClientSave, FSaveOptions::ClientSlotName, FSaveOptions::ClientUserID);
}
