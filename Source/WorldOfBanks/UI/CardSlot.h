// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CardSlot.generated.h"

class UCanvasPanel;
class UOverlay;
class UCardWidget;
struct FCardInfo;


/**
 * 
 */
UCLASS()
class WORLDOFBANKS_API UCardSlot : public UUserWidget
{
	GENERATED_BODY()

public:
	void AddCard(const FCardInfo& Card);
	void Clear();

	void ShowCard();
	void HideCard();

	virtual void NativeOnInitialized() override;
	virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;

	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry,	const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation*& OutOperation) override;
	virtual void NativeOnDragCancelled(const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;

	UCardWidget* GetCardWidget() const;

	FORCEINLINE bool IsEmpty() const { return bEmptyState; }

private:
	UPROPERTY(meta = (BindWidget))
	UCanvasPanel* SlotCanvas;

	UPROPERTY(meta = (BindWidget))
	UOverlay* CardOverlay;

	bool bEmptyState;
};
