// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "Blueprint/UserWidget.h"
#include "WorldOfBanks/Bank/CardInfo.h"
#include "WorldOfBanks/Components/DeviceBehavior/BlockCard/BankDeviceBlockCardBehaviorCommons.h"
#include "CardWidget.generated.h"

class UTextBlock;
DECLARE_DELEGATE_OneParam(FOnCardDoubleClicked, FCardInfo)

UCLASS()
class WORLDOFBANKS_API UCardWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	/** Sets card number. Card number must consists of 16 numbers */
	void SetCardNumber(FString CardNumberInput);
	void SetBankName(const FString& CardOwnerInput);

	/** Sets month of card expiration. Month must be between 1 and 12 */
	void SetEndDateMonth(int32 Month);

	/** Sets year of card expiration. Month must be between 2000 and MaxYear parameter */
	void SetEndDateYear(int32 Year);

	void Init(const FCardInfo& CardInfo);

	FORCEINLINE const FCardInfo& GetCardInfo() const { return CardInfo; }

	FOnCardDoubleClicked OnCardDoubleClicked;
protected:
	virtual FReply NativeOnMouseButtonDoubleClick(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
private:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* CardNumber;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* BankName;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* EndDateMonth;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* EndDateYear;

	UPROPERTY()
	FCardInfo CardInfo;
};