﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "BaseDeviceWidget.h"

#include "Components/WidgetSwitcher.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "WorldOfBanks/Utility/BankStateContainer.h"

void UBaseDeviceWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UBaseDeviceWidget::InitWidget(ABankDevice* ParentBankDevice, UBankDeviceBehaviorComponent* CorrespondingBehaviorComponent)
{
	BankDevice = ParentBankDevice;
	
	BankDevice->GetState().OnChanged.AddUObject(this, &UBaseDeviceWidget::OnDeviceStateChanged);
}

void UBaseDeviceWidget::OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState)
{

}

void UBaseDeviceWidget::ClearWidget()
{
}
