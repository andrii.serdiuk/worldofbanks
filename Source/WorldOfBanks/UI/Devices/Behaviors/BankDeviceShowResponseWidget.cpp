﻿#include "BankDeviceShowResponseWidget.h"

#include "Components/TextBlock.h"

void UBankDeviceShowResponseWidget::ShowResponse(EResponse ReasonToBeDisplayed)
{
	DisplayedResponse = ReasonToBeDisplayed;
	Message1->SetText(FText::FromString(ResponseToExplanation(ReasonToBeDisplayed)));
}
