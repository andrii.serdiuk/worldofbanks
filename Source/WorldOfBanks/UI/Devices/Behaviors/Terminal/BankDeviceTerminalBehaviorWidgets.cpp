﻿#include "BankDeviceTerminalBehaviorWidgets.h"

#include "Components/Button.h"
#include "Components/EditableText.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"

void UBankDeviceGreetingTerminalWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	BlockCardButton->OnClicked.AddDynamic(this, &UBankDeviceGreetingTerminalWidget::OnBlockCard_Clicked);
}

void UBankDeviceGreetingTerminalWidget::OnAction1_Clicked()
{
	OnNextPressed.Broadcast();
}

void UBankDeviceGreetingTerminalWidget::OnBlockCard_Clicked()
{
	OnBlockCardPressed.Broadcast();
}
