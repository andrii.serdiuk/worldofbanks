// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/UI/Devices/BaseDeviceWidget.h"
#include "BankDeviceTerminalMainMenuWidget.generated.h"

struct FCardInfo;
class UButton;
class UScrollBox;
class UTextBlock;

UCLASS()
class WORLDOFBANKS_API UBankDeviceTerminalMainMenuWidget : public UBaseDeviceWidget
{
	GENERATED_BODY()
public:
	virtual void InitWidget(ABankDevice* BankDevice, UBankDeviceBehaviorComponent* BehaviorComponent) override;

	UPROPERTY(meta = (BindWidget))
	UButton* CheckMoneyButton;
	UPROPERTY(meta = (BindWidget))
	UButton* ChangePinButton;
	UPROPERTY(meta = (BindWidget))
	UButton* TopUpButton;
	UPROPERTY(meta = (BindWidget))
	UButton* TransferMoneyButton;
	UPROPERTY(meta = (BindWidget))
	UButton* PrintTransactionsButton;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* BankName;
};
