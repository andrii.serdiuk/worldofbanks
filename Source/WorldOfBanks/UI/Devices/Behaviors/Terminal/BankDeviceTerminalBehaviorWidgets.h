﻿#pragma once
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "BankDeviceTerminalBehaviorWidgets.generated.h"

UCLASS()
class UBankDeviceGreetingTerminalWidget: public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:
	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
	UButton* BlockCardButton;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* BlockCardButtonName;

	FOnActionTriggeredDelegate OnBlockCardPressed;
	FOnActionTriggeredDelegate OnNextPressed;
	virtual void OnAction1_Clicked() override;

	UFUNCTION()
	void OnBlockCard_Clicked();
};