// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "BankDeviceTerminalMainMenuWidget.h"
#include "Components/TextBlock.h"
#include "WorldOfBanks/Devices/BankDevice.h"

void UBankDeviceTerminalMainMenuWidget::InitWidget(ABankDevice* BankDeviceIn,
	UBankDeviceBehaviorComponent* BehaviorComponent)
{
	Super::InitWidget(BankDeviceIn, BehaviorComponent);
	BankName->SetText(FText::FromString(BankDevice->GetBankIssuer().LoadSynchronous()->GetBankName()));
}