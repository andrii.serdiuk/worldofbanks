#include "BankDeviceTerminalBlockCardWidgets.h"

#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Components/ScrollBox.h"
#include "WorldOfBanks/Devices/BankTerminal.h"
#include "WorldOfBanks/UI/CardSlot.h"
#include "WorldOfBanks/UI/CardWidget.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceGetMoneyWidget.h"
#include "WorldOfBanks/Validation/inputValidation.h"


//Phone widget
void UBankDeviceWaitingForPhoneWidget::OnOkButton_Clicked()
{
	const int64 Phone = GetPhone();
	Cast<ABankTerminal>(BankDevice)->ServerGenerateOTP(Phone);
}

void UBankDeviceWaitingForPhoneWidget::OnCancelButton_Clicked()
{
	OnCancelled.Broadcast();
}

void UBankDeviceWaitingForPhoneWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	GetPhoneNumberWidget->OnTextChanged.AddDynamic(this, &UBankDeviceWaitingForPhoneWidget::ValidatePhone);
	GetPhoneNumberWidget->bAllowNegative = false;

	OkButton->OnClicked.AddDynamic(this, &UBankDeviceWaitingForPhoneWidget::OnOkButton_Clicked);
	CancelButton->OnClicked.AddDynamic(this, &UBankDeviceWaitingForPhoneWidget::OnCancelButton_Clicked);
}

void UBankDeviceWaitingForPhoneWidget::OnDeviceStateChanged(TDeviceState MovieSceneBlends, TDeviceState PreviousState)
{
	Super::OnDeviceStateChanged(MovieSceneBlends, PreviousState);
	ValidatePhone(GetPhoneNumberWidget->GetText());
}

void UBankDeviceWaitingForPhoneWidget::ValidatePhone(const FText& Text)
{
	OkButton->SetIsEnabled(FInputValidation::IsPhoneNumberValid(Text));
}


int64 UBankDeviceWaitingForPhoneWidget::GetPhone() const
{
	return FCString::Strtoi64(*GetPhoneNumberWidget->GetText().ToString(), nullptr, 10);
}

//Code widget
void UBankDeviceWaitingForCodeWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	Form1->OnTextChanged.AddDynamic(this, &UBankDeviceWaitingForCodeWidget::ValidateCode);
}

void UBankDeviceWaitingForCodeWidget::OnDeviceStateChanged(TDeviceState MovieSceneBlends, TDeviceState PreviousState)
{
	Super::OnDeviceStateChanged(MovieSceneBlends, PreviousState);
	Form1->SetText(FText::GetEmpty());
	Action1->SetIsEnabled(false);
}

int32 UBankDeviceWaitingForCodeWidget::GetCode() const
{
	return FCString::Strtoi64(*Form1->GetText().ToString(), nullptr, 10);
}

void UBankDeviceWaitingForCodeWidget::ValidateCode(const FText& Text) 
{
	Action1->SetIsEnabled(Form1->GetText().IsNumeric() && Form1->GetText().ToString().TrimStartAndEnd().Len() == CODE_LENGTH);
}

void UBankDeviceWaitingForCodeWidget::OnAction1_Clicked()
{
	Super::OnAction1_Clicked();
	OnCardCodeTyped.Execute(GetCode());
}

void UBankDeviceWaitingForCodeWidget::OnAction2_Clicked()
{
	Super::OnAction2_Clicked();
	OnCancelled.Broadcast();
}

//Choose card widget
void UBankDeviceTerminalChoosingCardWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	CancelButton->OnClicked.AddDynamic(this, &UBankDeviceTerminalChoosingCardWidget::OnCancelPressed);
}

void UBankDeviceTerminalChoosingCardWidget::InitList(const TArray<FCardInfo>& Cards) const
{
	CardsList->ClearChildren();
	for(const FCardInfo& Card : Cards)
	{
		UCardWidget* CardWidget = CreateWidget<UCardWidget>(GetOwningPlayer(), Card.CardWidgetClass.LoadSynchronous());
		CardWidget->OnCardDoubleClicked.BindUObject(this, &UBankDeviceTerminalChoosingCardWidget::OnCardChosen);
		CardWidget->Init(Card);
		CardsList->AddChild(CardWidget);
	}
}


void UBankDeviceTerminalChoosingCardWidget::OnCardChosen(FCardInfo CardInfo) const
{
	Cast<ABankTerminal>(BankDevice)->ServerBlockCard(CardInfo);
}

void UBankDeviceTerminalChoosingCardWidget::OnCancelPressed()
{
	OnCancelled.Broadcast();
}