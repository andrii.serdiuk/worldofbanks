#pragma once
#include "CoreMinimal.h"
#include "Components/ScrollBox.h"
#include "WorldOfBanks/Components/DeviceBehavior/BlockCard/BankDeviceBlockCardBehaviorCommons.h"
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/NumericTextBoxWidget.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "WorldOfBanks/UI/Devices/BaseDeviceWidget.h"
#include "BankDeviceTerminalBlockCardWidgets.generated.h"

class UBankDeviceGetMoneyWidget;
class UCardWidget;
class UCardSlot;
class UScrollBox;
struct FCardInfo;
UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceWaitingForPhoneWidget final: public UBankDeviceShowMessageWidget
{
	GENERATED_BODY()
	
public:

	virtual void OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState) override;
	virtual void NativeOnInitialized() override;
	
	int64 GetPhone() const;
	
	FOnActionTriggeredDelegate OnSubmit;
	FOnActionTriggeredDelegate OnCancelled;

	UPROPERTY(meta = (BindWidget))
	UNumericTextBoxWidget* GetPhoneNumberWidget;

protected:

	UFUNCTION()
	void ValidatePhone(const FText& Text);
	
	UPROPERTY(meta = (BindWidget))
	UButton* OkButton;
	
	UPROPERTY(meta = (BindWidget))
	UButton* CancelButton;

	UFUNCTION()
	void OnOkButton_Clicked();
	
	UFUNCTION()
	void OnCancelButton_Clicked();

};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceWaitingForCodeWidget final: public UBankDeviceOneFormTwoActionsWidget
{
	GENERATED_BODY()
	
public:
	
	static constexpr int32 CODE_LENGTH{4};
	UFUNCTION()
	void ValidateCode(const FText& Text);
	virtual void OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState) override;
	int32 GetCode() const;

	virtual void NativeOnInitialized() override;
	virtual void OnAction1_Clicked() override;
	virtual void OnAction2_Clicked() override;

	FOnActionTriggeredDelegate OnCancelled;
	FOnCardCodeTyped OnCardCodeTyped;
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceTerminalChoosingCardWidget final: public UBaseDeviceWidget
{
	GENERATED_BODY()
	
public:
	
	virtual void NativeOnInitialized() override;
	FOnActionTriggeredDelegate OnCancelled;

	void InitList(const TArray<FCardInfo>& Cards) const;
	FORCEINLINE void Clear() const{ CardsList->ClearChildren(); }
	
private:
	
	UPROPERTY(meta = (BindWidget))
	UButton* CancelButton;
	UPROPERTY(meta = (BindWidget))
	UScrollBox* CardsList;

	UFUNCTION()
	void OnCancelPressed();
	void OnCardChosen(FCardInfo CardInfo) const;

	
};