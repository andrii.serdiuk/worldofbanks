﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "WorldOfBanks/Bank/Currency.h"
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/Devices/BankTerminal.h"
#include "WorldOfBanks/UI/Devices/BaseDeviceWidget.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "BankDeviceTopUpWidget.generated.h"

struct FMoneyInfo;
/**
 * 
 */
UCLASS()
class WORLDOFBANKS_API UBankDeviceTopUpWidget : public UBankDeviceShowMessageWidget
{
	GENERATED_BODY()

public:

	virtual void InitWidget(ABankDevice* BankDevice, UBankDeviceBehaviorComponent* BehaviorComponent) override;
	virtual void OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState) override;

	void UpdateAccumulatedSumRepresentation(const FMoneyInfo& AddedMoneyInfo);
	void UpdateAccumulatedSumRepresentation(UTextBlock* TextBlock, int32 Sum);
	void UpdateSubmitButtonEnabled();

	FOnActionTriggeredDelegate OnGoBack;
	
protected:

	void OnMoneyPushed(const FMoneyInfo& AddedMoneyInfo);
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* SumUSD;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* SumEUR;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* SumUAH;

	TMap<ECurrency, TPair<UTextBlock*, int32>> CurrenciesToWidgets;

	UPROPERTY(meta = (BindWidget))
	UButton* SubmitButton;

	bool CanPerformTopUp() const;
	
	UFUNCTION()
	void OnSubmitButton_Pressed();
	
	UPROPERTY(meta = (BindWidget))
	UButton* CancelButton;
	
	UFUNCTION()
	void OnCancelButton_Pressed();
	
	UPROPERTY(Transient)
	ABankTerminal* Terminal;
};
