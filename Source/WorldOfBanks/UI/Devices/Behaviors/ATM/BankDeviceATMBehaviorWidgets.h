﻿#pragma once
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "BankDeviceATMBehaviorWidgets.generated.h"

UCLASS()
class UBankDeviceGreetingATMWidget : public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:

	FOnActionTriggeredDelegate OnNextPressed;
	virtual void OnAction1_Clicked() override;
};