// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/UI/Devices/BaseDeviceWidget.h"
#include "BankDeviceATMMainMenuWidget.generated.h"

struct FCardInfo;
class UButton;
class UScrollBox;
class UTextBlock;

UCLASS()
class WORLDOFBANKS_API UBankDeviceATMMainMenuWidget : public UBaseDeviceWidget
{
	GENERATED_BODY()
public:
	virtual void InitWidget(ABankDevice* BankDevice, UBankDeviceBehaviorComponent* BehaviorComponent) override;

	UPROPERTY(meta = (BindWidget))
	UButton* CheckMoneyButton;
	UPROPERTY(meta = (BindWidget))
	UButton* ChangePinButton;
	UPROPERTY(meta = (BindWidget))
	UButton* WithdrawButton;
	UPROPERTY(meta = (BindWidget))
	UButton* PrintCheckButton;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* BankName;
};
