// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "BankDeviceATMMainMenuWidget.h"
#include "Components/TextBlock.h"
#include "WorldOfBanks/Devices/BankDevice.h"

void UBankDeviceATMMainMenuWidget::InitWidget(ABankDevice* BankDeviceIn, UBankDeviceBehaviorComponent* BehaviorComponent)
{
	Super::InitWidget(BankDeviceIn, BehaviorComponent);
	BankName->SetText(FText::FromString(BankDevice->GetBankIssuer().LoadSynchronous()->GetBankName()));
}