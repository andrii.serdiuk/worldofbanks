﻿#pragma once
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "BankDeviceShowResponseWidget.generated.h"

UCLASS()
class UBankDeviceShowResponseWidget : public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:

	void ShowResponse(EResponse ReasonToBeDisplayed);
	
	FOnResponseReceivedDelegate OnConfirmation;

	TOptional<EResponse> DisplayedResponse;

protected:

	virtual void OnAction1_Clicked() override { OnConfirmation.Broadcast(DisplayedResponse.GetValue()); }

};