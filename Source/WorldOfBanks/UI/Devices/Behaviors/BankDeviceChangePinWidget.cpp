﻿#include "BankDeviceChangePinWidget.h"

#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "WorldOfBanks/UI/NumericTextBoxWidget.h"

void UBankDeviceChangePinWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	OkButton->OnClicked.AddDynamic(this, &UBankDeviceChangePinWidget::OnOkButton_Clicked);
	CancelButton->OnClicked.AddDynamic(this, &UBankDeviceChangePinWidget::OnCancelButton_Clicked);
}

void UBankDeviceChangePinWidget::OnDeviceStateChanged(TDeviceState MovieSceneBlends, TDeviceState PreviousState)
{
	Super::OnDeviceStateChanged(MovieSceneBlends, PreviousState);
	PINTextBoxWidget->SetText(FText::GetEmpty());
}

void UBankDeviceChangePinWidget::OnOkButton_Clicked()
{
	const FString InputString = PINTextBoxWidget->GetText().ToString();
	const int32 InputValue = FCString::Atoi(*InputString);
	
	if (InputValue > 0 && InputString.Len() == 4) // atof returns 0 if unable to convert
	{
		BankDevice->ServerChangePin(InputValue);
	}
}

void UBankDeviceChangePinWidget::OnCancelButton_Clicked()
{
	OnGoBack.Broadcast();
}
