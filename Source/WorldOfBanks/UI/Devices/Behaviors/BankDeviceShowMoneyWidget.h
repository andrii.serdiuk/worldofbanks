﻿#pragma once
#include "Components/TextBlock.h"
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "BankDeviceShowMoneyWidget.generated.h"


UCLASS()
class UBankDeviceShowMoneyWidget : public UBankDeviceShowMessageWithReasonAndActionWidget
{
	GENERATED_BODY()

public:
	
	FORCEINLINE void ServerUpdateCall() const { BankDevice->ServerGetCurrentBalance(); }
	FORCEINLINE void SetMoneyAmount(FString AlreadyFormattedMoneyAmount, ECurrency Currency)
	{

		AlreadyFormattedMoneyAmount.InsertAt(AlreadyFormattedMoneyAmount.Len() - 2, L'.');
		Reason1->SetText(FText::FromString(AlreadyFormattedMoneyAmount.Append(CurrencySymbols.FindChecked(Currency))));
	}

	FOnActionTriggeredDelegate OnGoBack;
	FORCEINLINE virtual void OnAction1_Clicked() override { OnGoBack.Broadcast(); }
};