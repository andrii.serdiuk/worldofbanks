﻿#pragma once
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/Components/DeviceBehavior/Login/BankDeviceLoginBehavior.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "BankDeviceBehaviorWidgets.generated.h"


// separate C++ widget classes are needed in order to make device-side logic easier
// and also for easier configuration

UCLASS()
class UBankDeviceCardBlockedWidget : public UBankDeviceShowMessageWithReasonAndActionWidget
{
	GENERATED_BODY()

public:

	virtual void NativeConstruct() override;

	void SetBlockReason(EResponse Reason);

protected:

	UPROPERTY(EditDefaultsOnly)
	TMap<EResponse, FText> BlockReasons;
	
};

UCLASS()
class UBankDeviceGreetingPOSWidget : public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:

	FOnActionTriggeredDelegate OnNextPressed;
	virtual void OnAction1_Clicked() override;
};

UCLASS()
class UBankDeviceInsufficientFundsWidget : public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:

	FOnActionTriggeredDelegate OnNextPressed;
	virtual void OnAction1_Clicked() override;
};

UCLASS()
class UBankDeviceSuccessWidget : public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:
	
	FOnActionTriggeredDelegate OnNextPressed;
	virtual void OnAction1_Clicked() override;
};
