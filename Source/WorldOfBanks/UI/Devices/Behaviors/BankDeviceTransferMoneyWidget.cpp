﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "BankDeviceTransferMoneyWidget.h"

#include "Components/Button.h"
#include "Components/ComboBox.h"
#include "Components/ComboBoxString.h"
#include "Components/EditableTextBox.h"
#include "WorldOfBanks/Devices/BankTerminal.h"
#include "WorldOfBanks/UI/NumericTextBoxWidget.h"
#include "WorldOfBanks/Validation/inputValidation.h"

void UBankDeviceTransferMoneyWidget::InitWidget(ABankDevice* InBankDevice, UBankDeviceBehaviorComponent* BehaviorComponent)
{
	Super::InitWidget(InBankDevice, BehaviorComponent);
	
	BankTerminal = Cast<ABankTerminal>(InBankDevice);
	
	AccountTextBoxWidget->OnTextChanged.AddDynamic(this, &UBankDeviceTransferMoneyWidget::OnAccountFormChanged);
	GetMoneyWidget->InitWidget(InBankDevice, BehaviorComponent);
	GetMoneyWidget->SumTextBoxWidget->OnTextChanged.AddDynamic(this, &UBankDeviceTransferMoneyWidget::OnSumFormChanged);
	
	OkButton->SetIsEnabled(false);
	OkButton->OnClicked.AddDynamic(this, &UBankDeviceTransferMoneyWidget::OnOkButton_Clicked);
	CancelButton->OnClicked.AddDynamic(this, &UBankDeviceTransferMoneyWidget::OnCancelButton_Clicked);
}

void UBankDeviceTransferMoneyWidget::OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState)
{
	Super::OnDeviceStateChanged(NewState, PreviousState);
	
	AccountTextBoxWidget->SetText(FText::GetEmpty());
	GetMoneyWidget->ClearWidget();
	OkButton->SetIsEnabled(false);
}

void UBankDeviceTransferMoneyWidget::OnOkButton_Clicked()
{
	check(IsCardAccountValid())
	check(GetMoneyWidget->GetSum().IsSet())
	
	BankTerminal->ServerTransferMoney(AccountTextBoxWidget->GetText().ToString(), GetMoneyWidget->GetSum().GetValue() * 100, GetMoneyWidget->GetCurrency());
}

void UBankDeviceTransferMoneyWidget::OnCancelButton_Clicked()
{
	AccountTextBoxWidget->SetText(FText::FromString(""));
	GetMoneyWidget->ClearWidget();
	
	OnGoBack.Broadcast();
}

void UBankDeviceTransferMoneyWidget::OnAccountFormChanged(const FText&)
{
	OkButton->SetIsEnabled(ShouldSubmitButtonBeActive());
}

void UBankDeviceTransferMoneyWidget::OnSumFormChanged(const FText&)
{
	OkButton->SetIsEnabled(ShouldSubmitButtonBeActive());
}

bool UBankDeviceTransferMoneyWidget::IsCardAccountValid() const
{
	return FInputValidation::IsCardAccountValid(*AccountTextBoxWidget->GetText().ToString());
}

bool UBankDeviceTransferMoneyWidget::ShouldSubmitButtonBeActive() const
{
	return IsCardAccountValid() && GetMoneyWidget->GetSum().IsSet();
}