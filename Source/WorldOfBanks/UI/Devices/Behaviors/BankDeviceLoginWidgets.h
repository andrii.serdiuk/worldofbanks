﻿#pragma once
#include "CoreMinimal.h"
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/NumericTextBoxWidget.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "BankDeviceLoginWidgets.generated.h"


class UBankDeviceWaitingForCardBehavior;
class UBankDeviceWaitingForPinBehavior;

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceWaitingForPinWidget : public UBankDeviceShowMessageWidget
{
	GENERATED_BODY()

public:
	
	virtual void InitWidget(ABankDevice* InBankDevice, UBankDeviceBehaviorComponent* BehaviorComponent) override;
	
	int32 GetPIN() const;

	UPROPERTY(meta = (BindWidget))
	UNumericTextBoxWidget* PINTextBoxWidget;
	
protected:
	
	UPROPERTY()
	UBankDeviceWaitingForPinBehavior* Behavior;
	
	UPROPERTY(meta = (BindWidget))
	UButton* OkButton;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* OkButtonName;
	
	UPROPERTY(meta = (BindWidget))
	UButton* CancelButton;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* CancelButtonName;

	UFUNCTION()
	void OnOkButton_Clicked();
	
	UFUNCTION()
    void OnCancelButton_Clicked();

	
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceWaitingForCardWidget : public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:

	virtual void InitWidget(ABankDevice* InBankDevice, UBankDeviceBehaviorComponent* BehaviorComponent) override;

	virtual void OnAction1_Clicked() override;

	FOnActionTriggeredDelegate OnCancelled;
};
