﻿#pragma once
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "BankDeviceChangePinWidget.generated.h"

class UNumericTextBoxWidget;

UCLASS()
class UBankDeviceChangePinWidget : public UBankDeviceShowMessageWidget
{
	GENERATED_BODY()

public:
	
	virtual void NativeOnInitialized() override;
	virtual void OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState) override;
	
	FOnActionTriggeredDelegate OnGoBack;
	FOnActionTriggeredDelegate OnSubmit;
	
	UPROPERTY(meta = (BindWidget))
	UNumericTextBoxWidget* PINTextBoxWidget;
	
protected:
	
	UPROPERTY(meta = (BindWidget))
	UButton* OkButton;
	
	UPROPERTY(meta = (BindWidget))
	UButton* CancelButton;
	
	UFUNCTION()
	void OnOkButton_Clicked();
	
	UFUNCTION()
	void OnCancelButton_Clicked();
	
};

