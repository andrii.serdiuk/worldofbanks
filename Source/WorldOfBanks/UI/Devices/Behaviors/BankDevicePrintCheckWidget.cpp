﻿#include "BankDevicePrintCheckWidget.h"
#include "Components/TextBlock.h"
#include "Components/ScrollBox.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "WorldOfBanks/Devices/BankATM.h"

void UBankDevicePrintCheckWidget::UpdateExtracts(TArray<FTransactionInfo> Transactions) const
{
	ExtractsList->ClearChildren();

	for(const FTransactionInfo& Transaction : Transactions)
	{
		UTextBlock* Child = NewObject<UTextBlock>();
		Child->SetAutoWrapText(true);
		Child->Font.Size = 18;
		Child->SetColorAndOpacity(Transaction.Sum > 0 ? FLinearColor::Green : FLinearColor::Red );
		Child->SetText(FText::FromString(Transaction.Action));
		ExtractsList->AddChild(Child);
	}
}
