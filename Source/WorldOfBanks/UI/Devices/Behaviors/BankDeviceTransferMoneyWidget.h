﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once
#include "WorldOfBanks/Bank/Currency.h"
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/CurrencyComboBoxWidget.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceGetMoneyWidget.h"
#include "BankDeviceTransferMoneyWidget.generated.h"

class UNumericTextBoxWidget;
class UComboBoxString;
class ABankTerminal;

UCLASS()
class UBankDeviceTransferMoneyWidget : public UBankDeviceShowMessageWidget
{
	GENERATED_BODY()

public:
	
	virtual void InitWidget(ABankDevice* BankDevice, UBankDeviceBehaviorComponent* BehaviorComponent) override;
	virtual void OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState) override;

	FOnActionTriggeredDelegate OnGoBack;
	
	UPROPERTY(meta = (BindWidget))
	UNumericTextBoxWidget* AccountTextBoxWidget;

protected:
	
	UFUNCTION()
	void OnAccountFormChanged(const FText& CardAccountInput);
	UFUNCTION()
	void OnSumFormChanged(const FText& SumInput);
	
	bool IsCardAccountValid() const;
	bool ShouldSubmitButtonBeActive() const;
	
	UPROPERTY(meta = (BindWidget))
	UBankDeviceGetMoneyWidget* GetMoneyWidget;
	
	UPROPERTY(meta = (BindWidget))
	UButton* OkButton;
	
	UPROPERTY(meta = (BindWidget))
	UButton* CancelButton;

	UFUNCTION()
	void OnOkButton_Clicked();
	
	UFUNCTION()
	void OnCancelButton_Clicked();
	
	UPROPERTY()
	ABankTerminal* BankTerminal;
	
};
