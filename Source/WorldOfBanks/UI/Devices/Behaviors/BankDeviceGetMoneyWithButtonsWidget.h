﻿#pragma once
#include "WorldOfBanks/Bank/Currency.h"
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/CurrencyComboBoxWidget.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "BankDeviceGetMoneyWithButtonsWidget.generated.h"


class UBankDeviceGetMoneyWidget;
class UComboBoxString;
class UNumericTextBoxWidget;
class UBankDeviceWithdrawBehavior;

UCLASS()
class UBankDeviceGetMoneyWithButtonsWidget : public UBankDeviceShowMessageWidget
{
	GENERATED_BODY()

public:
	
	virtual void InitWidget(ABankDevice* InBankDevice, UBankDeviceBehaviorComponent* BehaviorComponent) override;
	virtual void OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState) override;

	TOptional<int32> GetSum() const;
	ECurrency GetCurrency() const;
	
	FOnActionTriggeredDelegate OnSubmit;
	FOnActionTriggeredDelegate OnGoBack;

	UPROPERTY(meta = (BindWidget))
	UBankDeviceGetMoneyWidget* GetMoneyWidget;

protected:

	UFUNCTION()
	void OnSumFormChanged(const FText& NewText);
	
	UPROPERTY(meta = (BindWidget))
	UButton* OkButton;
	
	UPROPERTY(meta = (BindWidget))
	UButton* CancelButton;

	UFUNCTION()
	void OnOkButton_Clicked();
	
	UFUNCTION()
	void OnCancelButton_Clicked();

};

UCLASS()
class UBankDeviceWithdrawSuccessWidget : public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:

	void SetSum(int32 Amount, ECurrency Currency);

	FOnActionTriggeredDelegate OnConfirmed;

protected:

	UPROPERTY(meta = (BindWidget))
	UTextBlock* SumTextBlock;

	virtual void OnAction1_Clicked() override;
};
