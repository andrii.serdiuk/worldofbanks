﻿#include "BankDeviceLoginWidgets.h"

#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"
#include "WorldOfBanks/Components/DeviceBehavior/Login/BankDeviceLoginSubBehaviors.h"


void UBankDeviceWaitingForPinWidget::InitWidget(ABankDevice* InBankDevice, UBankDeviceBehaviorComponent* BehaviorComponent)
{
	Super::InitWidget(InBankDevice, BehaviorComponent);

	Behavior = Cast<UBankDeviceWaitingForPinBehavior>(BehaviorComponent);
	
	OkButton->OnClicked.AddDynamic(this, &UBankDeviceWaitingForPinWidget::OnOkButton_Clicked);
	CancelButton->OnClicked.AddDynamic(this, &UBankDeviceWaitingForPinWidget::OnCancelButton_Clicked);
}

void UBankDeviceWaitingForPinWidget::OnOkButton_Clicked()
{
	Behavior->TryLogin(GetPIN());
}

void UBankDeviceWaitingForPinWidget::OnCancelButton_Clicked()
{
	FLoginFailureReason Reason;
	Reason.Set<ELoginAbortReason>(ELoginAbortReason::CancelledByUser);
	Behavior->OnLoginFinishedWithFailure.Execute(Reason);
}

int32 UBankDeviceWaitingForPinWidget::GetPIN() const
{
	return FCString::Atoi(*PINTextBoxWidget->GetText().ToString());
}

void UBankDeviceWaitingForCardWidget::InitWidget(ABankDevice* InBankDevice, UBankDeviceBehaviorComponent* BehaviorComponent)
{
	Super::InitWidget(InBankDevice, BehaviorComponent);
}

void UBankDeviceWaitingForCardWidget::OnAction1_Clicked()
{
	Super::OnAction1_Clicked();

	OnCancelled.Broadcast();
}
