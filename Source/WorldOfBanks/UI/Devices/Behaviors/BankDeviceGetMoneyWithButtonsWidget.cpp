﻿#include "BankDeviceGetMoneyWithButtonsWidget.h"

#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceTransientBehaviors.h"
#include "WorldOfBanks/Devices/BankATM.h"
#include "WorldOfBanks/UI/NumericTextBoxWidget.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceGetMoneyWidget.h"
#include "WorldOfBanks/Validation/inputValidation.h"

void UBankDeviceGetMoneyWithButtonsWidget::InitWidget(ABankDevice* InBankDevice, UBankDeviceBehaviorComponent* BehaviorComponent)
{
	Super::InitWidget(InBankDevice, BehaviorComponent);
	
	OkButton->SetIsEnabled(false);
	OkButton->OnClicked.AddDynamic(this, &UBankDeviceGetMoneyWithButtonsWidget::OnOkButton_Clicked);
	CancelButton->OnClicked.AddDynamic(this, &UBankDeviceGetMoneyWithButtonsWidget::OnCancelButton_Clicked);

	GetMoneyWidget->InitWidget(InBankDevice, BehaviorComponent);
	GetMoneyWidget->SumTextBoxWidget->OnTextChanged.AddDynamic(this, &UBankDeviceGetMoneyWithButtonsWidget::OnSumFormChanged);
}

void UBankDeviceGetMoneyWithButtonsWidget::OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState)
{
	Super::OnDeviceStateChanged(NewState, PreviousState);
	
	OkButton->SetIsEnabled(false);
}

TOptional<int32> UBankDeviceGetMoneyWithButtonsWidget::GetSum() const
{
	return GetMoneyWidget->GetSum();
}

ECurrency UBankDeviceGetMoneyWithButtonsWidget::GetCurrency() const
{
	return GetMoneyWidget->GetCurrency();
}

void UBankDeviceGetMoneyWithButtonsWidget::OnSumFormChanged(const FText&)
{
	OkButton->SetIsEnabled(GetMoneyWidget->GetSum().IsSet());
}

void UBankDeviceGetMoneyWithButtonsWidget::OnOkButton_Clicked()
{
	check(GetSum().IsSet())

	OnSubmit.Broadcast();
}

void UBankDeviceGetMoneyWithButtonsWidget::OnCancelButton_Clicked()
{
	OnGoBack.Broadcast();
}

void UBankDeviceWithdrawSuccessWidget::SetSum(int32 Amount, ECurrency Currency)
{
	const FString String = FString::Printf(TEXT("%d %s"), Amount / 100, *CurrencySymbols[Currency]);
	SumTextBlock->SetText(FText::FromString(String));
}

void UBankDeviceWithdrawSuccessWidget::OnAction1_Clicked()
{
	Super::OnAction1_Clicked();

	OnConfirmed.Broadcast();
}
