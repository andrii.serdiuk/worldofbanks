﻿#pragma once
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "WorldOfBanks/Devices/BankATM.h"
#include "BankDevicePrintCheckWidget.generated.h"

class UScrollBox;

UCLASS()
class UBankDevicePrintCheckWidget : public UBankDeviceShowMessageWithReasonAndActionWidget
{
	GENERATED_BODY()

public:
	void UpdateExtracts(TArray<FTransactionInfo> Transactions) const;

	UPROPERTY(meta = (BindWidget))
	UScrollBox* ExtractsList;

	FOnActionTriggeredDelegate OnGoBack;
	virtual void OnAction1_Clicked() override { OnGoBack.Broadcast(); }
};