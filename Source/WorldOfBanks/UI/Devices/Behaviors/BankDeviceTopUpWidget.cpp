﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "BankDeviceTopUpWidget.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "WorldOfBanks/WorldOfBanks.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "WorldOfBanks/Devices/BankTerminal.h"

void UBankDeviceTopUpWidget::InitWidget(ABankDevice* InBankDevice, UBankDeviceBehaviorComponent* BehaviorComponent)
{
	Super::InitWidget(InBankDevice, BehaviorComponent);

	CurrenciesToWidgets =
	{
		{ ECurrency::Dollar,	TPair<UTextBlock*, int32>(SumUSD, 0) },
		{ ECurrency::Euro,		TPair<UTextBlock*, int32>(SumEUR, 0) },
		{ ECurrency::Hryvnia,	TPair<UTextBlock*, int32>(SumUAH, 0) },
	};
	
	for (const TTuple<ECurrency, TPair<UTextBlock*, int32>>& CurrencyToWidget : CurrenciesToWidgets)
	{
		UpdateAccumulatedSumRepresentation(CurrencyToWidget.Value.Key, CurrencyToWidget.Value.Value);
	}
	
	Terminal = Cast<ABankTerminal>(BankDevice);
	if (Terminal)
	{
		Terminal->OnMoneyPushed.AddUObject(this, &UBankDeviceTopUpWidget::OnMoneyPushed);
	}

	SubmitButton->OnPressed.AddDynamic(this, &UBankDeviceTopUpWidget::OnSubmitButton_Pressed);
	CancelButton->OnPressed.AddDynamic(this, &UBankDeviceTopUpWidget::OnCancelButton_Pressed);

	UpdateSubmitButtonEnabled();
}

void UBankDeviceTopUpWidget::UpdateAccumulatedSumRepresentation(const FMoneyInfo& AddedMoneyInfo)
{
	TPair<UTextBlock*, int32>& WidgetAndAccumulatedValue = CurrenciesToWidgets[AddedMoneyInfo.Key.Currency];
	WidgetAndAccumulatedValue.Value += AddedMoneyInfo.Value * AddedMoneyInfo.Key.Denomination / 100;
	UpdateAccumulatedSumRepresentation(WidgetAndAccumulatedValue.Key, WidgetAndAccumulatedValue.Value);
}

void UBankDeviceTopUpWidget::UpdateAccumulatedSumRepresentation(UTextBlock* TextBlock, int32 Sum)
{
	TextBlock->SetText(FText::FromString(FString::FromInt(Sum)));
}

void UBankDeviceTopUpWidget::UpdateSubmitButtonEnabled()
{
	SubmitButton->SetIsEnabled(CanPerformTopUp());
}

void UBankDeviceTopUpWidget::OnMoneyPushed(const FMoneyInfo& AddedMoneyInfo)
{
	CancelButton->SetIsEnabled(false);

	UpdateAccumulatedSumRepresentation(AddedMoneyInfo);

	UpdateSubmitButtonEnabled();
}

bool UBankDeviceTopUpWidget::CanPerformTopUp() const
{
	for (const TTuple<ECurrency, TPair<UTextBlock*, int32>>& CurrencyToWidget : CurrenciesToWidgets)
	{
		if (CurrencyToWidget.Value.Value > 0)
		{
			return true;
		}
	}

	return false;
}

void UBankDeviceTopUpWidget::OnSubmitButton_Pressed()
{
	check(CanPerformTopUp())
	
	if (Terminal)
	{
		Terminal->ServerTopUpFunds();
	}
}

void UBankDeviceTopUpWidget::OnCancelButton_Pressed()
{
	OnGoBack.Broadcast();
}

void UBankDeviceTopUpWidget::OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState)
{
	Super::OnDeviceStateChanged(NewState, PreviousState);

	// when we change state (thus, widget as well)
	CancelButton->SetIsEnabled(true);

	for (TTuple<ECurrency, TPair<UTextBlock*, int32>>& CurrencyToWidget : CurrenciesToWidgets)
	{
		CurrencyToWidget.Value.Value = 0;
	}
}
