﻿#include "BankDeviceBehaviorWidgets.h"

#include "Components/Button.h"
#include "Components/EditableText.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"

void UBankDeviceCardBlockedWidget::NativeConstruct()
{
	Super::NativeConstruct();

	BlockReasons.FindOrAdd(EResponse::Blocked) = FText::FromString("Your card is already blocked.");
	BlockReasons.FindOrAdd(EResponse::HasBeenJustBlocked) = FText::FromString("PIN is incorrect. No attempts left.");
}

void UBankDeviceCardBlockedWidget::SetBlockReason(EResponse Reason)
{
	Reason1->SetText(BlockReasons.FindOrAdd(Reason));
}

void UBankDeviceGreetingPOSWidget::OnAction1_Clicked()
{
	OnNextPressed.Broadcast();
}

void UBankDeviceInsufficientFundsWidget::OnAction1_Clicked()
{
	Super::OnAction1_Clicked();
	OnNextPressed.Broadcast();
}

void UBankDeviceSuccessWidget::OnAction1_Clicked()
{
	Super::OnAction1_Clicked();
	OnNextPressed.Broadcast();
}
