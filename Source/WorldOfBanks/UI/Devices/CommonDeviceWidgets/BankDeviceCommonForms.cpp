﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "BankDeviceCommonForms.h"

#include "Components/Button.h"
#include "Components/EditableTextBox.h"


void UBankDeviceShowMessageWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

}

void UBankDeviceShowMessageWithActionWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	Action1->OnClicked.AddDynamic(this, &UBankDeviceShowMessageWithActionWidget::OnAction1_Clicked);
}

void UBankDeviceShowMessageWithReasonAndActionWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	Action1->OnClicked.AddDynamic(this, &UBankDeviceShowMessageWithReasonAndActionWidget::OnAction1_Clicked);
}

void UBankDeviceShowMessageWithTwoReasonsAndActionWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	Action1->OnClicked.AddDynamic(this, &UBankDeviceShowMessageWithTwoReasonsAndActionWidget::OnAction1_Clicked);
}

void UBankDeviceOneFormWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	Form1->OnTextChanged.AddDynamic(this, &UBankDeviceOneFormWidget::OnForm1Changed);
}

void UBankDeviceOneFormWithReasonWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	Form1->OnTextChanged.AddDynamic(this, &UBankDeviceOneFormWithReasonWidget::OnForm1Changed);
}

void UBankDeviceTwoFormWithTwoActionWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	
	Action2->OnClicked.AddDynamic(this, &UBankDeviceTwoFormWithTwoActionWidget::OnAction2_Clicked);
}

void UBankDeviceOneFormTwoActionsWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	Action2->OnClicked.AddDynamic(this, &UBankDeviceOneFormTwoActionsWidget::OnAction2_Clicked);
}

void UBankDeviceOneFormTwoActionsWithReasonWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	Action2->OnClicked.AddDynamic(this, &UBankDeviceOneFormTwoActionsWithReasonWidget::OnAction2_Clicked);
}
