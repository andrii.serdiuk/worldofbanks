﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/UI/Devices/BaseDeviceWidget.h"
#include "BankDeviceCommonForms.generated.h"


class UTextBlock;

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceShowMessageWidget : public UBaseDeviceWidget
{
	GENERATED_BODY()

public:

	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Message1;
	
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceShowMessageWithReasonWidget : public UBankDeviceShowMessageWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* Reason1;
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceShowMessageWithTwoReasonsWidget : public UBankDeviceShowMessageWithReasonWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* Reason2;
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceShowMessageWithActionWidget : public UBankDeviceShowMessageWidget
{
	GENERATED_BODY()

public:
	
	virtual void NativeOnInitialized() override;
	
	UPROPERTY(meta = (BindWidget))
	UButton* Action1;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Action1Name;
	
	UFUNCTION()
	virtual void OnAction1_Clicked() {};

};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceShowMessageWithReasonAndActionWidget : public UBankDeviceShowMessageWithReasonWidget
{
	GENERATED_BODY()

public:
	
	virtual void NativeOnInitialized() override;
	
	UPROPERTY(meta = (BindWidget))
	UButton* Action1;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Action1Name;
	
	UFUNCTION()
	virtual void OnAction1_Clicked() {};
	
};


UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceShowMessageWithTwoReasonsAndActionWidget : public UBankDeviceShowMessageWithTwoReasonsWidget
{
	GENERATED_BODY()

public:
	
	virtual void NativeOnInitialized() override;
	
	UPROPERTY(meta = (BindWidget))
	UButton* Action1;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Action1Name;
	
	UFUNCTION()
	virtual void OnAction1_Clicked() {}
	
};


UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceOneFormWidget : public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:
	
	virtual void NativeOnInitialized() override;
	
	UPROPERTY(meta = (BindWidget))
	UEditableTextBox* Form1;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* Form1Name;
	
protected:
	
	UFUNCTION()
	virtual void OnForm1Changed(const FText& Text) {}
	
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceOneFormWithReasonWidget : public UBankDeviceShowMessageWithReasonAndActionWidget
{
	GENERATED_BODY()

public:

	virtual void NativeOnInitialized() override;
	
	UPROPERTY(meta = (BindWidget))
	UEditableTextBox* Form1;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* Form1Name;

protected:

	UFUNCTION()
	virtual void OnForm1Changed(const FText& Text) {}
	
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceTwoFormWidget : public UBankDeviceOneFormWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(meta = (BindWidget))
	UEditableTextBox* Form2;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* Form2Name;
	
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceTwoFormWithTwoActionWidget : public UBankDeviceTwoFormWidget
{
	GENERATED_BODY()

public:

	virtual void NativeOnInitialized() override;
	
	UPROPERTY(meta = (BindWidget))
	UButton* Action2;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Action2Name;
	
	UFUNCTION()
	virtual void OnAction2_Clicked() {}
	
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceTwoFormWithReasonWidget : public UBankDeviceOneFormWithReasonWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(meta = (BindWidget))
	UEditableTextBox* Form2;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* Form2Name;

};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceOneFormTwoActionsWidget : public UBankDeviceOneFormWidget
{
	GENERATED_BODY()

public:
	
	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
	UButton* Action2;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Action2Name;
	
	UFUNCTION()
	virtual void OnAction2_Clicked() {};
	
};

UCLASS(Blueprintable)
class WORLDOFBANKS_API UBankDeviceOneFormTwoActionsWithReasonWidget : public UBankDeviceOneFormWithReasonWidget
{
	GENERATED_BODY()

public:
	
	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
	UButton* Action2;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Action2Name;
	
	UFUNCTION()
	virtual void OnAction2_Clicked() {}
	
};