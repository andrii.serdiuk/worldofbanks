﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "BankDeviceGetMoneyWidget.h"

#include "Components/EditableTextBox.h"
#include "WorldOfBanks/UI/CurrencyComboBoxWidget.h"
#include "WorldOfBanks/UI/NumericTextBoxWidget.h"
#include "WorldOfBanks/Validation/inputValidation.h"

void UBankDeviceGetMoneyWidget::ClearWidget()
{
	Super::ClearWidget();

	SumTextBoxWidget->SetText(FText::GetEmpty());
	CurrencyComboBox->SetSelectedIndex(0);
}

ECurrency UBankDeviceGetMoneyWidget::GetCurrency() const
{
	return CurrencyComboBox->GetCurrency();
}

void UBankDeviceGetMoneyWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	CurrencyComboBox->Init();
	
	SumTextBoxWidget->bAllowDot = true;
}

TOptional<int32> UBankDeviceGetMoneyWidget::GetSum() const
{
	return FInputValidation::GetSumFromText(SumTextBoxWidget->GetText()); 
}
