﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "WorldOfBanks/Bank/Currency.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "BankDeviceGetMoneyWidget.generated.h"

class UCurrencyComboBoxWidget;
class UNumericTextBoxWidget;

UCLASS()
class UBankDeviceGetMoneyWidget : public UBaseDeviceWidget
{
	GENERATED_BODY()

public:
	
	virtual void ClearWidget() override;

	TOptional<int32> GetSum() const;
	ECurrency GetCurrency() const;

	UPROPERTY(meta = (BindWidget))
	UNumericTextBoxWidget* SumTextBoxWidget;
	
	UPROPERTY(meta = (BindWidget))
	UCurrencyComboBoxWidget* CurrencyComboBox;

protected:
	
	virtual void NativeOnInitialized() override;
	
};