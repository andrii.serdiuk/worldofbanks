﻿#pragma once
#include "WorldOfBanks/Components/DeviceBehavior/Commons/BankCommonBehaviorDelegates.h"
#include "WorldOfBanks/UI/Devices/CommonDeviceWidgets/BankDeviceCommonForms.h"
#include "WorldOfBanks/Devices/BankATM.h"
#include "BankDevicePOSReceiptWidget.generated.h"

class UScrollBox;

UCLASS()
class UBankDevicePOSReceiptWidget : public UBankDeviceShowMessageWithActionWidget
{
	GENERATED_BODY()

public:
	virtual void Init(const int32 Sum, ECurrency Currency);

	UPROPERTY(meta = (BindWidget))
	UTextBlock* HeaderMessage;

	FOnActionTriggeredDelegate OnOkPressed;
	virtual void OnAction1_Clicked() override { OnOkPressed.Broadcast(); }
};