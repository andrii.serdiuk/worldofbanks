﻿#include "BankDevicePOSReceiptWidget.h"
#include "Components/TextBlock.h"
#include "Components/ScrollBox.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "WorldOfBanks/Devices/BankATM.h"

void UBankDevicePOSReceiptWidget::Init(const int32 Sum, ECurrency Currency)
{
	Message1->SetText(FText::FromString(FString("Sum: ") + 
										FStringHelpers::GetStringFromSum(Sum) +
										SignTranslation(Currency)));
}
