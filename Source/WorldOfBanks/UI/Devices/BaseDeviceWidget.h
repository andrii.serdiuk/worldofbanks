﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "Blueprint/UserWidget.h"
#include "WorldOfBanks/Utility/BankStateContainerCommons.h"
#include "BaseDeviceWidget.generated.h"

class UWidgetSwitcher;
class UButton;
class UEditableText;
class UEditableTextBox;
class UBankDeviceBehaviorComponent;
class ABankDevice;

UCLASS(Abstract)
class WORLDOFBANKS_API UBaseDeviceWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	
	virtual void NativeConstruct() override;
	virtual void InitWidget(ABankDevice* BankDevice, UBankDeviceBehaviorComponent* BehaviorComponent);
	virtual void OnDeviceStateChanged(TDeviceState NewState, TDeviceState PreviousState);
	virtual void ClearWidget();
	
protected:
	
	UPROPERTY(Transient)
	ABankDevice* BankDevice; // the one we show GUI for
	
};
