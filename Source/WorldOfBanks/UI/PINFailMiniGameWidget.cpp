// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "PINFailMiniGameWidget.h"
#include "MediaAssets/Public/MediaPlayer.h"
#include "Components/Button.h"
#include "Components/EditableText.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "MediaAssets/Public/MediaSource.h"

void UPINFailMiniGameWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	
	GetOwningPlayer()->SetInputMode(FInputModeUIOnly());
	GetOwningPlayer()->SetShowMouseCursor(true);
	
	GameResult->SetVisibility(ESlateVisibility::Collapsed);
	JavaButton->OnClicked.AddDynamic(this, &UPINFailMiniGameWidget::ProcessButtonJava);
	PythonButton->OnClicked.AddDynamic(this, &UPINFailMiniGameWidget::ProcessButtonPython);
	JavaPlaceholder->OnTextChanged.AddDynamic(this, &UPINFailMiniGameWidget::ProcessInputJava);
	PythonPlaceholder->OnTextChanged.AddDynamic(this, &UPINFailMiniGameWidget::ProcessInputPython);
	PINFailMediaFailure->OnEndReached.AddDynamic(this, &UPINFailMiniGameWidget::FailEnded);
}

void UPINFailMiniGameWidget::FailEnded()
{
	if (ensure(!FailEnd.IsNull()))
	{
		GameResult->SetBrushFromTexture(FailEnd.LoadSynchronous());
	}

	RemoveFromParent(); // delete widget
	OnMinigameFinished.ExecuteIfBound(false); // call delegate
}

void UPINFailMiniGameWidget::ProcessButtonJava()
{
	ProcessButton(Cast<UTextBlock>(JavaButton->GetChildAt(0))->GetText().ToString());
}

void UPINFailMiniGameWidget::ProcessButtonPython()
{
	ProcessButton(Cast<UTextBlock>(PythonButton->GetChildAt(0))->GetText().ToString());
}

void UPINFailMiniGameWidget::ProcessButton(const FString& String)
{
	GameResult->SetVisibility(ESlateVisibility::Visible);
	if(String.TrimStartAndEnd().ToLower() == "c++")
	{
		//User pass the game
		if (ensureAlways(!SuccessEnd.IsNull()))
		{
			GameResult->SetBrushFromTexture(SuccessEnd.LoadSynchronous());
		}

		FTimerDelegate Delegate;
		Delegate.BindWeakLambda(this, [this]()
			{
				RemoveFromParent(); // delete widget
				OnMinigameFinished.ExecuteIfBound(true); // execute delegate
			});

		FTimerHandle Handle;
		GetWorld()->GetTimerManager().SetTimer(Handle, Delegate, 3.f, false);
	}
	else
	{
		if (ensureAlways(!PINFailVideo.IsNull()))
		{
			PINFailMediaFailure->OpenSource(PINFailVideo.LoadSynchronous());
		}
		PINFailMediaFailure->Play();
	}
}

void UPINFailMiniGameWidget::ProcessInputJava(const FText& Quantity)
{
	const FString TrimmedQuantity{ Quantity.ToString().TrimStartAndEnd()};
	if (TrimmedQuantity.ToLower() == "c++")
	{
		Cast<UTextBlock>(JavaButton->GetChildAt(0))->SetText(FText::FromString("C++"));
	}
}

void UPINFailMiniGameWidget::ProcessInputPython(const FText& Quantity)
{
	const FString TrimmedQuantity {Quantity.ToString().TrimStartAndEnd()};
	if (TrimmedQuantity.ToLower() == "c++")
	{
		Cast<UTextBlock>(PythonButton->GetChildAt(0))->SetText(FText::FromString("C++"));
	}
}
