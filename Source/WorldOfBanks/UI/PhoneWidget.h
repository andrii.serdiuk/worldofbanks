﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PhoneWidget.generated.h"

class UScrollBox;
class UTextBlock;

/** Class of a single message in UPhoneWidget. Instances are created by UPhoneWidget */
UCLASS(Abstract)
class WORLDOFBANKS_API UPhoneMessageWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetMessage(const FString& InMessage);

private:
	UPROPERTY(meta=(BindWidget))
	UTextBlock* Message;

	uint32 MessageHash = 0;
};

/** Phone widget class. Shows messages on client-side. Instance is created by CoreHUD */
UCLASS(Abstract)
class WORLDOFBANKS_API UPhoneWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void InitWidget(int64 InPhoneNumber);
	
	void RebuildPhoneMessages(const TArray<FString>& Messages);

protected:
	virtual void NativeOnInitialized() override;
	
private:
	UPROPERTY(meta=(BindWidget))
	UScrollBox* MessagesBox;

	UPROPERTY(meta=(BindWidget))
	UTextBlock* PhoneNumber;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<UPhoneMessageWidget> MessageClass;
};