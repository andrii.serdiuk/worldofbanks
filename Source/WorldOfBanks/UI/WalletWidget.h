// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "Components/ScrollBox.h"
#include "WorldOfBanks/UI/BanknoteSlotWidget.h"
#include "WalletWidget.generated.h" 

class UCardSlot;
class UGridPanel;
struct FCardInfo;

UCLASS(Abstract)
class WORLDOFBANKS_API UWalletWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void AddCardToSlot(const FCardInfo& Card, UCardSlot* Slot);

	FORCEINLINE void RemoveBanknote(UBillSlot* BillSlot) { BillList->RemoveChild(BillSlot); }

	UCardSlot* GetFirstEmptySlot() const;

	void Update();

private:
	void Clear();
	
	UPROPERTY(meta = (BindWidget))
	UScrollBox* BillList;

	UPROPERTY(meta = (BindWidget))
	UGridPanel* CardPanel;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<UBillSlot> BanknoteSlotClass;
};