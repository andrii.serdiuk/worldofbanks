// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WalletLevelDrop.generated.h"

/**
 * 
 */
UCLASS()
class WORLDOFBANKS_API UWalletLevelDrop : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual bool NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;
};
