﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "UObject/Object.h"
#include "ShowText.generated.h"

/**
 * 
 */
UCLASS()
class WORLDOFBANKS_API UShowText : public UUserWidget
{
	GENERATED_BODY()

public:

	FORCEINLINE void SetText(const FText& Text) { TextWidget->SetText(Text); }
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* TextWidget;
};
