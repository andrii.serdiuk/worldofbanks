// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "CardSlot.h"
#include "Blueprint/DragDropOperation.h"
#include "Components/CanvasPanel.h"
#include "Components/Overlay.h"
#include "WorldOfBanks/UI/CardWidget.h"
#include "WorldOfBanks/Pickup/PickupCard.h"
#include "WorldOfBanks/WorldOfBanks.h"

void UCardSlot::AddCard(const FCardInfo& Card)
{
	TSubclassOf<UCardWidget> CardWidgetClass = Card.CardWidgetClass.LoadSynchronous();
	check(CardWidgetClass);

	UCardWidget* CardWidget = CreateWidget<UCardWidget>(GetOwningPlayer(), CardWidgetClass);

	CardWidget->Init(Card);
	CardOverlay->AddChildToOverlay(CardWidget);
	bEmptyState = false;
}

void UCardSlot::Clear()
{
	CardOverlay->ClearChildren();
	bEmptyState = true;
}

void UCardSlot::ShowCard()
{
	SlotCanvas->SetClipping(EWidgetClipping::Inherit);
}

void UCardSlot::HideCard()
{
	SlotCanvas->SetClipping(EWidgetClipping::ClipToBounds);
}

void UCardSlot::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	bEmptyState = true;
}

void UCardSlot::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	ShowCard();
}

void UCardSlot::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	HideCard();
}

FReply UCardSlot::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	FReply Res{ Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent) };
	if(IsEmpty())
	{
		return Res;
	}
	return Res.DetectDrag(TakeWidget(), EKeys::LeftMouseButton);
}

void UCardSlot::NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent,
	UDragDropOperation*& OutOperation)
{
	Super::NativeOnDragDetected(InGeometry, InMouseEvent, OutOperation);
	GetOwningPlayer()->SetInputMode(FInputModeUIOnly());
	OutOperation = NewObject<UDragDropOperation>();
	OutOperation->DefaultDragVisual = GetCardWidget();
	OutOperation->Payload = this;

	UE_LOG(Log_UI, Warning, TEXT("We're dragging"));
}

void UCardSlot::NativeOnDragCancelled(const FDragDropEvent& InDragDropEvent,
	UDragDropOperation* InOperation)
{
	Super::NativeOnDragCancelled(InDragDropEvent, InOperation);
	GetOwningPlayer()->SetInputMode(FInputModeGameAndUI());
}

UCardWidget* UCardSlot::GetCardWidget() const
{
	check(!IsEmpty());
	return Cast<UCardWidget>(CardOverlay->GetChildAt(0));
}
