﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "NumericTextBoxWidget.h"

void UNumericTextBoxWidget::HandleOnTextChanged(const FText& NewText)
{
	Super::HandleOnTextChanged(NewText);

	int32 DotLastPosition = INDEX_NONE;
	const FString NewTextString = NewText.ToString();
	const bool bDotExists = NewTextString.FindLastChar('.', DotLastPosition);
	const bool bAllowDotSatisfied = bAllowDot ? (DotLastPosition < NewTextString.Len()) : (!bDotExists);

	UE_LOG(LogTemp, Warning, TEXT("bDotExists %d"), bDotExists ? 1 : 0);
	
	bool bIsNumeric = NewText.IsNumeric();
	if (bCanBeEmpty)
	{
		bIsNumeric |= NewText.IsEmpty();
	}
	
	const bool bAllowNegativeSatisfied = bAllowNegative ? true : !NewTextString.Contains("-");
	
	if (bIsNumeric
		&& bAllowDotSatisfied
		&& bAllowNegativeSatisfied)
	{
		LastValidNumericInput = NewText;
	}
	else
	{
		SetText(LastValidNumericInput);
	}
}
