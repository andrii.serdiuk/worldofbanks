// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "BanknoteSlotWidget.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Components/EditableText.h"
#include "Components/Image.h"
#include "WorldOfBanks/Core/CorePlayerController.h"
#include "WorldOfBanks/Components/WalletComponent.h"
#include "WorldOfBanks/Core/CoreHUD.h"
#include "WorldOfBanks/UI/WalletWidget.h"
#include "WorldOfBanks/Devices/BankTerminal.h"


void UBillSlot::Init(const FBanknote& BanknoteIn, int32 BanknotesNumber)
{
	check(BanknotesNumber > 0);
	Banknote = BanknoteIn;
	BillQuantity->SetText(FText::AsNumber(BanknotesNumber));
	ChooseQuantity->SetText(FText::AsNumber(0));
			if (ensure(!Banknote.Texture.IsNull()))
	{
		ImageBill->SetBrushFromTexture(Banknote.Texture.LoadSynchronous());
	}

	if(Cast<ABankTerminal>(Cast<ACorePlayerController>(GetOwningPlayer())->CurrentDevice))
	{
		DropButtonText->SetText(FText::FromString("Insert card"));
	}

	ChooseQuantity->OnTextCommitted.AddDynamic(this, &UBillSlot::ValidateQuantity);
	DropButton->OnClicked.AddDynamic(this, &UBillSlot::DropBanknotes);
}

void UBillSlot::DropBanknotes()
{
	int32 ChooseNumber = FCString::Atoi(*ChooseQuantity->GetText().ToString());
	int32 QuantityNumber = FCString::Atoi(*BillQuantity->GetText().ToString());
	check(ChooseNumber <= QuantityNumber && ChooseNumber >= 0 && QuantityNumber != 0);
	if(ChooseNumber == 0)
	{
		return;
	}

	ACorePlayerController* CorePlayerController = GetOwningPlayer<ACorePlayerController>();
	check(CorePlayerController);

	UWalletComponent* PlayerWallet = CorePlayerController->GetWallet();
	check(PlayerWallet);

	if (ABankTerminal* Terminal = Cast<ABankTerminal>(CorePlayerController->CurrentDevice))
	{
		//Inserting into bank device
		if (Terminal->CanPushMoney())
		{
			PlayerWallet->RemoveCash(Banknote, ChooseNumber);
			Terminal->ServerPushMoney({ Banknote, ChooseNumber });
			UpdateInput();
		}
	}
	else
	{
		// Drop cash to level
		PlayerWallet->DropCash(Banknote, ChooseNumber);
		UpdateInput();
	}
}

void UBillSlot::ValidateQuantity(const FText& Quantity, ETextCommit::Type InCommitType)
{
	if(!Quantity.IsNumeric())
	{
		ChooseQuantity->SetText(FText());
	}
	else
	{
		int32 QuantityNumber = FCString::Atoi(*Quantity.ToString());
		if(QuantityNumber > FCString::Atoi(*BillQuantity->GetText().ToString()))
		{
			ChooseQuantity->SetText(BillQuantity->GetText());
		}
	}
}

void UBillSlot::UpdateInput()
{
	const int32 ChooseNumber = FCString::Atoi(*ChooseQuantity->GetText().ToString());
	const int32 QuantityNumber = FCString::Atoi(*BillQuantity->GetText().ToString());

	if (ChooseNumber == QuantityNumber)
	{
		GetOwningPlayer()->GetHUD<ACoreHUD>()->GetWalletWidget()->RemoveBanknote(this);
	}
	else
	{
		BillQuantity->SetText(FText::AsNumber(QuantityNumber - ChooseNumber));
		ChooseQuantity->SetText(FText::AsNumber(0));
	}
}
