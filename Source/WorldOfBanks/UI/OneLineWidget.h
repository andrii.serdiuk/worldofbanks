// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "OneLineWidget.generated.h"

class UTextBlock;
class UScrollBox;

UCLASS(Abstract)
class WORLDOFBANKS_API UOneLineWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	FORCEINLINE UTextBlock* GetOneLineText() const { return OneLineText; }

private:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* OneLineText;
};

UCLASS(Abstract)
class WORLDOFBANKS_API UOneLineWidgetContainer : public UUserWidget
{
	GENERATED_BODY()

public:
	void AddChildOneLiner(const FString& TextToDisplay);

private:
	/** How many seconds does Child OneLineWidget live */
	UPROPERTY(EditAnywhere)
	float ChildLifetime = 5.f;

	UPROPERTY(EditAnywhere)
	TSoftClassPtr<UOneLineWidget> OneLineWidgetClass;

	UPROPERTY(meta = (BindWidget))
	UScrollBox* OneLineContainer;
};