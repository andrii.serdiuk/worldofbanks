// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ConnectionMenu.generated.h"

class UEditableTextBox;
class UButton;

UCLASS(Abstract)
class WORLDOFBANKS_API UConnectionMenu : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeOnInitialized() override;
	
private:
	UFUNCTION()
	void OnConnectPressed();

	void LoadData();

	void SaveData(const FString& HostAddress, const FString& Name, const FString& Password);
	
	UPROPERTY(meta=(BindWidget))
	UEditableTextBox* Host;
	
	UPROPERTY(meta=(BindWidget))
	UEditableTextBox* PlayerName;

	UPROPERTY(meta=(BindWidget))
	UEditableTextBox* PlayerPassword;

	UPROPERTY(meta=(BindWidget))
	UButton* ConnectButton;
};
