// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "OneLineWidget.h"
#include "TimerManager.h"
#include "Components/TextBlock.h"
#include "Components/ScrollBox.h"

void UOneLineWidgetContainer::AddChildOneLiner(const FString& TextToDisplay)
{
	if (ensure(!OneLineWidgetClass.IsNull()))
	{
		UOneLineWidget* OneLiner = CreateWidget<UOneLineWidget>(GetOwningPlayer(), OneLineWidgetClass.LoadSynchronous());
		check(OneLiner);

		OneLiner->GetOneLineText()->SetText(FText::FromString(TextToDisplay));

		FTimerDelegate Delegate;
		// Lambda to remove OneLiner widget from Container after Lifetime ends.
		Delegate.BindWeakLambda(this,
			[this, WeakOneLiner = TWeakObjectPtr<UOneLineWidget>(OneLiner)]()
			{
				if (WeakOneLiner.IsValid())
				{
					OneLineContainer->RemoveChild(WeakOneLiner.Get());
				}
			});

		FTimerHandle Handle;
		GetWorld()->GetTimerManager().SetTimer(Handle, Delegate, ChildLifetime, false);

		OneLineContainer->AddChild(OneLiner);
	}
}