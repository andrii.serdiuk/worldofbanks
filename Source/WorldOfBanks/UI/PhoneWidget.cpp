﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "PhoneWidget.h"
#include "Components/ScrollBox.h"
#include "Components/TextBlock.h"

void UPhoneMessageWidget::SetMessage(const FString& InMessage)
{
	const uint32 InMessageHash = TextKeyUtil::HashString(InMessage);
	
	if (MessageHash != InMessageHash)
	{
		Message->SetText(FText::FromString(InMessage));
		MessageHash = InMessageHash;
	}
}

void UPhoneWidget::InitWidget(int64 InPhoneNumber)
{
	FString PhoneNumberStr = FString::Printf(TEXT("%lld"), InPhoneNumber);
	PhoneNumberStr.InsertAt(0, "+");
	PhoneNumberStr.InsertAt(3, "(");
	PhoneNumberStr.InsertAt(7, ")");
	PhoneNumberStr.InsertAt(11, "-");
	PhoneNumberStr.InsertAt(14, "-");
	PhoneNumber->SetText(FText::FromString(PhoneNumberStr));
}

void UPhoneWidget::RebuildPhoneMessages(const TArray<FString>& Messages)
{
	const TArray<UPanelSlot*>& MessageSlots = MessagesBox->GetSlots();	
	const int32 ReusableSlotNum = FMath::Min(Messages.Num(), MessageSlots.Num());
	
	for (int32 i = 0; i < ReusableSlotNum; ++i)
	{
		UPhoneMessageWidget* MessageWidget = CastChecked<UPhoneMessageWidget>(MessageSlots[i]->Content);
		MessageWidget->SetMessage(Messages[i]); // Will not be expensive since changing only if text differs
	}

	if (Messages.Num() < MessageSlots.Num())
	{
		// If we have more message widgets that really need, remove backwards
		for (int32 i = Messages.Num() - 1; i >= ReusableSlotNum; --i)
		{
			MessagesBox->RemoveChildAt(i);
		}
	}
	else
	{
		for (int32 i = ReusableSlotNum; i < Messages.Num(); ++i)
		{
			UPhoneMessageWidget* MessageWidget = CreateWidget<UPhoneMessageWidget>(GetOwningPlayer(), MessageClass.LoadSynchronous());
			MessageWidget->SetMessage(Messages[i]);
			MessagesBox->AddChild(MessageWidget);
		}
	}

	MessagesBox->ScrollToEnd();
}

void UPhoneWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	checkf(!MessageClass.IsNull(), TEXT("MessageClass is Null!"));
}
