﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Components/ComboBoxString.h"
#include "UObject/Object.h"
#include "WorldOfBanks/Bank/Currency.h"
#include "CurrencyComboBoxWidget.generated.h"

/**
 * 
 */
UCLASS()
class WORLDOFBANKS_API UCurrencyComboBoxWidget : public UComboBoxString
{
	GENERATED_BODY()

public:
	
	void Init();

	void SetCurrency(ECurrency Currency);
	ECurrency GetCurrency() const;
};
