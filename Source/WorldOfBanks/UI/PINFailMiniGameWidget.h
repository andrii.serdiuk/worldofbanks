// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PINFailMiniGameWidget.generated.h"

/**
 * Needed for game over mini game
 */
class UButton;
class UTextBlock;
class UEditableText;
class UMediaPlayer;
class UImage;
class UMediaSource;

DECLARE_DELEGATE_OneParam(FMinigameFinished, bool)

UCLASS()
class WORLDOFBANKS_API UPINFailMiniGameWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	FMinigameFinished OnMinigameFinished;

protected:
	virtual void NativeOnInitialized() override;

private:
	UFUNCTION()
	void ProcessButtonJava();
	
	UFUNCTION()
	void ProcessButtonPython();
	
	UFUNCTION()
	void FailEnded();
	
	void ProcessButton(const FString& String);

	/** Checks input and if ok change button label */
	UFUNCTION()
	void ProcessInputJava(const FText& Quantity);
	
	UFUNCTION()
	void ProcessInputPython(const FText& Quantity);

	UPROPERTY(meta = (BindWidget))
	UButton* JavaButton;

	UPROPERTY(meta = (BindWidget))
	UButton* PythonButton;

	UPROPERTY(meta = (BindWidget))
	UEditableText* PythonPlaceholder;

	UPROPERTY(meta = (BindWidget))
	UEditableText* JavaPlaceholder;
	
	UPROPERTY(meta = (BindWidget))
	UImage* GameResult;

	UPROPERTY(EditDefaultsOnly)
	UMediaPlayer* PINFailMediaFailure;

	UPROPERTY(EditDefaultsOnly)
	TSoftObjectPtr<UMediaSource> PINFailVideo;

	UPROPERTY(EditDefaultsOnly)
	TSoftObjectPtr<UTexture2D> SuccessEnd;

	UPROPERTY(EditDefaultsOnly)
	TSoftObjectPtr<UTexture2D> FailEnd;
};