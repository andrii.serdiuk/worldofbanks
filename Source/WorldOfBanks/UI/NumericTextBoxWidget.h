﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "Components/EditableTextBox.h"
#include "NumericTextBoxWidget.generated.h"


UCLASS(Blueprintable, BlueprintType)
class WORLDOFBANKS_API UNumericTextBoxWidget : public UEditableTextBox
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly)
	bool bCanBeEmpty = true;

	UPROPERTY(EditDefaultsOnly)
	bool bAllowDot = false;
	
	UPROPERTY(EditDefaultsOnly)
	bool bAllowNegative = true;
	
protected:

	virtual void HandleOnTextChanged(const FText& NewText) override;
	FText LastValidNumericInput;
	
};
