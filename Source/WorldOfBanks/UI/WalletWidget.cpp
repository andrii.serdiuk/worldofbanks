// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "WalletWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Components/GridPanel.h"
#include "WorldOfBanks/Core/CorePlayerController.h"
#include "WorldOfBanks/Components/WalletComponent.h"
#include "WorldOfBanks/UI/CardSlot.h"
#include "WorldOfBanks/Pickup/PickupCard.h"

void UWalletWidget::AddCardToSlot(const FCardInfo& Card, UCardSlot* CardSlot)
{
	check(CardSlot != nullptr && CardSlot->IsEmpty());
	CardSlot->AddCard(Card);
}

UCardSlot* UWalletWidget::GetFirstEmptySlot() const
{
	for (UWidget* CardSlot : CardPanel->GetAllChildren())
	{
		UCardSlot* CardSlotCast = Cast<UCardSlot>(CardSlot);
		if (CardSlotCast->IsEmpty())
		{
			return CardSlotCast;
		}
	}
	return nullptr;
}

void UWalletWidget::Update()
{
	Clear();
	
	UWalletComponent* Wallet = GetOwningPlayer<ACorePlayerController>()->GetWallet();
	if (ensure(Wallet))
	{
		for (const FMoneyInfo& Pair : Wallet->GetMoney())
		{
			if (ensure(!BanknoteSlotClass.IsNull()))
			{
				UBillSlot* SlotTmp = CreateWidget<UBillSlot>(GetOwningPlayer(), BanknoteSlotClass.LoadSynchronous());
				SlotTmp->Init(Pair.Key, Pair.Value);
				BillList->AddChild(SlotTmp);
			}
		}
		
		for (const FCardInfo& CardItem : Wallet->GetCards())
		{
			AddCardToSlot(CardItem, GetFirstEmptySlot());
		}
	}
}

void UWalletWidget::Clear()
{
	BillList->ClearChildren();
	
	for (UWidget* CardSlot : CardPanel->GetAllChildren())
	{
		Cast<UCardSlot>(CardSlot)->Clear();
	}
}