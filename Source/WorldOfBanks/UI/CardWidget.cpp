// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "CardWidget.h"
#include "Components/TextBlock.h"
#include "WorldOfBanks/Pickup/PickupCard.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

void UCardWidget::SetCardNumber(FString CardNumberInput)
{
	if (ensure(CardNumberInput.Len() == 16 && CardNumberInput.IsNumeric()))
	{
		CardNumberInput.InsertAt(4, "   ");
		CardNumberInput.InsertAt(11, "   ");
		CardNumberInput.InsertAt(18, "   ");
	}
	CardNumber->SetText(FText::FromString(CardNumberInput));
}

void UCardWidget::SetBankName(const FString& CardOwnerInput)
{
	BankName->SetText(FText::FromString(CardOwnerInput));
}

void UCardWidget::SetEndDateMonth(int32 Month)
{
	ensure(Month > 0 && Month <= 12);
	if (Month < 10) // if one-digit month, add first 0 to it
	{
		EndDateMonth->SetText(FText::Format(NSLOCTEXT("", "", "0{0}"), FText::AsNumber(Month)));
	}
	else
	{
		EndDateMonth->SetText(FText::AsNumber(Month));
	}
}

void UCardWidget::SetEndDateYear(int32 Year)
{
	Year %= 100;
	ensure(Year >= 0 && Year <= 99);
	if (Year < 10) // if one-digit year, add first 0 to it
	{
		EndDateYear->SetText(FText::Format(NSLOCTEXT("", "", "0{0}"), FText::AsNumber(Year)));
	}
	else
	{
		EndDateYear->SetText(FText::AsNumber(Year));
	}
}

void UCardWidget::Init(const FCardInfo& Card)
{
	SetCardNumber(Card.CardNumber);
	const FString BankNameStr = Card.BankIssuer.IsNull() ? "No Name" : Card.BankIssuer.LoadSynchronous()->GetBankName();
	SetBankName(BankNameStr);
	SetEndDateMonth(Card.ExpMonth);
	SetEndDateYear(Card.ExpYear);
	CardInfo = Card;
}

FReply UCardWidget::NativeOnMouseButtonDoubleClick(const FGeometry& MovieSceneBlends, const FPointerEvent& InMouseEvent)
{
	FReply Result = Super::NativeOnMouseButtonDoubleClick(MovieSceneBlends, InMouseEvent);
	OnCardDoubleClicked.Execute(CardInfo);
	return Result;
}
