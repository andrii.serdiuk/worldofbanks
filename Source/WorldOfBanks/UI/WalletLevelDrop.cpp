// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "WalletLevelDrop.h"
#include "WorldOfBanks/Core/CorePlayerController.h"
#include "WorldOfBanks/UI/CardWidget.h"
#include "WorldOfBanks/Components/WalletComponent.h"
#include "WorldOfBanks/Devices/BankDevice.h"
#include "Blueprint/DragDropOperation.h"
#include "CardSlot.h"

bool UWalletLevelDrop::NativeOnDrop(const FGeometry& InGeometry,
	const FDragDropEvent& InDragDropEvent,
	UDragDropOperation* InOperation)
{
	UCardSlot* CardSlot = Cast<UCardSlot>(InOperation->Payload);
	check (CardSlot && !CardSlot->IsEmpty());
	FCardInfo CardInfo(CardSlot->GetCardWidget()->GetCardInfo());

	ACorePlayerController* PlayerController = GetOwningPlayer<ACorePlayerController>();
	if (ABankDevice* Device = PlayerController->CurrentDevice)
	{
		//Inserting into bank device
		if (Device->CanInsertCard())
		{
			PlayerController->GetWallet()->RemoveCard(CardInfo);
			Device->ServerInsertCard(CardInfo);
			CardSlot->Clear();
		}
	}
	else
	{
		// Drop card to level
		PlayerController->GetWallet()->DropCard(CardInfo);
		CardSlot->Clear();
	}
	
	
	PlayerController->SetInputMode(FInputModeGameAndUI());
	return true;
}
