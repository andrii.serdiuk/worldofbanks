// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/Bank/Banknote.h"
#include "Blueprint/UserWidget.h"
#include "BanknoteSlotWidget.generated.h"

struct FBanknote;
class UTextBlock;
class UButton;
class UEditableText;
class UImage;

UCLASS()
class WORLDOFBANKS_API UBillSlot : public UUserWidget
{
	GENERATED_BODY()

public:
	void Init(const FBanknote& Banknote, int32 BanknotesNumber);

private:
	/** Handles DropButton click */
	UFUNCTION()
	void DropBanknotes();

	/** Binds to choose quantity OnTextCommitted delegate */
	UFUNCTION()
	void ValidateQuantity(const FText& Quantity, ETextCommit::Type InCommitType);

	/** Updates information about selected quantity or removes this from Parent */
	void UpdateInput();

	UPROPERTY(meta = (BindWidget))
	UTextBlock* BillQuantity;

	UPROPERTY(meta = (BindWidget))
	UEditableText* ChooseQuantity;

	UPROPERTY(meta = (BindWidget))
	UButton* DropButton;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* DropButtonText;

	UPROPERTY(meta = (BindWidget))
	UImage* ImageBill;

	UPROPERTY()
	FBanknote Banknote;
};
