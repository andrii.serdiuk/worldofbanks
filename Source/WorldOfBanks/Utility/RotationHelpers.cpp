﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "RotationHelpers.h"

FRotator Utils::ClampRotator(FRotator Rotator, FRotator MinPerAxisClamp, FRotator MaxPerAxisClamp)
{
	return FRotator(FMath::ClampAngle(Rotator.Pitch, MinPerAxisClamp.Pitch, MaxPerAxisClamp.Pitch),
					FMath::ClampAngle(Rotator.Yaw, MinPerAxisClamp.Yaw, MaxPerAxisClamp.Yaw),
	                FMath::ClampAngle(Rotator.Roll, MinPerAxisClamp.Roll, MaxPerAxisClamp.Roll));
}

FRotator Utils::ClampRotatorAroundDirection(FRotator Rotator, FRotator Direction, FRotator PerAxisClampAbs)
{
	return ClampRotator(Rotator, (Direction - PerAxisClampAbs).Clamp(), (Direction + PerAxisClampAbs).Clamp()).Clamp();
}

float Utils::Dist(FRotator A, FRotator B)
{
	return FVector::Dist(A.Vector(), B.Vector());
}
