﻿#pragma once
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceBehaviorComponent.h"
#include "BankStateContainerCommons.generated.h"

class UWidgetComponent;
class ABankDevice;
class UBaseDeviceWidget;

using TDeviceState = TSubclassOf<UBankDeviceBehaviorComponent>;
using TDeviceTransition = TPair<TDeviceState, TDeviceState>;
using TDeviceTransitionPredicate = TFunction<bool(UBankDeviceBehaviorComponent*, UBankDeviceBehaviorComponent*)>;

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnDeviceStateChangedDelegate, TDeviceState /* new state */, TDeviceState /* previous state */);

USTRUCT(BlueprintType)
struct FBankDeviceStateContainerConfig
{
	GENERATED_BODY()

	/// State to widget class
	UPROPERTY(EditAnywhere, meta = (AllowAbstract = "true"))
	TMap<TSoftClassPtr<UBankDeviceBehaviorComponent>, TSoftClassPtr<UBaseDeviceWidget>> Widgets;

	/// State to it's non-default behavior class. By default we create behavior instance of class that is the state.
	UPROPERTY(EditAnywhere, meta = (AllowAbstract = "true"))
	TMap<TSoftClassPtr<UBankDeviceBehaviorComponent>, TSoftClassPtr<UBankDeviceBehaviorComponent>> OverrideClassesOfInstantiatedBehaviors;
	
	UPROPERTY()
	APlayerController* OwnerPlayerController;

	UPROPERTY()
	ABankDevice* BankDevice;

	UPROPERTY()
	UWidgetComponent* WidgetComponent;
};
