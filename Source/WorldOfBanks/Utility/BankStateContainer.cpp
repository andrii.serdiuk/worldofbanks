﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk


#include "BankStateContainer.h"
#include "WorldOfBanks/Devices/BankDevice.h"


// Sets default values
FBankStateContainer::FBankStateContainer()
{
	
}

void FBankStateContainer::Init(TScriptInterface<IDeviceStateful> InOwner, const FBankDeviceStateContainerConfig& Config)
{
	Reset();

	ContainerOwner = InOwner;
	WidgetComponent = Config.WidgetComponent;
	
	for (const auto& StateWidget : Config.Widgets)
	{
		TSubclassOf<UBankDeviceBehaviorComponent> StateBehaviorClass = StateWidget.Key.LoadSynchronous();

		const auto Overrides = Config.OverrideClassesOfInstantiatedBehaviors;
		TSubclassOf<UBankDeviceBehaviorComponent> InstanceBehaviorClass = Overrides.Contains(StateWidget.Key) ? Overrides[StateWidget.Key].LoadSynchronous() : StateBehaviorClass;

		TSubclassOf<UBaseDeviceWidget> WidgetClass = StateWidget.Value.LoadSynchronous();

		UBankDeviceBehaviorComponent* BehaviorComponent = nullptr;
		
		if (!(StateBehaviorClass->GetClassFlags() & CLASS_Abstract)) // ignore transient behaviors
		{
			UObject* BankDeviceObject = Cast<UObject>(Config.BankDevice);
			BehaviorComponent = NewObject<UBankDeviceBehaviorComponent>(BankDeviceObject, InstanceBehaviorClass);
			BehaviorComponent->Init(Config.BankDevice, *this);
			BehaviorComponent->OnComponentCreated();
			BehaviorComponent->RegisterComponent();
			
			Behaviors.Add(StateBehaviorClass, BehaviorComponent);
		}
		else
		{
			Behaviors.Add(StateBehaviorClass, nullptr);
		}

		if (WidgetClass)
		{
			UBaseDeviceWidget* CorrespondingWidget = CreateWidget<UBaseDeviceWidget>(Config.OwnerPlayerController, WidgetClass);
				
			Widgets.Add(StateBehaviorClass, CorrespondingWidget);
			CorrespondingWidget->InitWidget(Config.BankDevice, BehaviorComponent);
		}
		else
		{
			Widgets.Add(StateBehaviorClass, nullptr);
		}
	}
}

void FBankStateContainer::Reset()
{
	Widgets.Reset();
	
	for (const auto& StateToBehavior : Behaviors)
	{
		if (StateToBehavior.Value)
		{
			StateToBehavior.Value->DestroyComponent();
		}
	}
	
	Behaviors.Reset();
}

UBaseDeviceWidget* FBankStateContainer::GetDeepestCurrentWidget() const
{
	TScriptInterface<IDeviceStateful> CurrContainerOwner = GetContainerOwner();
	UBaseDeviceWidget* CurrCurrentWidget = Widgets[GetCurrentState()];
		
	while (true)
	{
		if (UBankDeviceBehaviorComponent* CurrentBehavior = CurrContainerOwner->GetStateContainer()->GetCurrentBehavior())
		{
			if (const FBankStateContainer* NextContainer = CurrentBehavior->GetStateContainer())
			{
				CurrContainerOwner = NextContainer->GetContainerOwner();
				if (CurrContainerOwner.GetObject())
				{
					CurrCurrentWidget = NextContainer->GetCurrentWidget();
				}
				else break;
			}
			else break;
		}
		else break;
	}
	
	return CurrCurrentWidget;
}

TScriptInterface<IDeviceStateful> FBankStateContainer::GetContainerOwner() const
{
	return ContainerOwner;
}
