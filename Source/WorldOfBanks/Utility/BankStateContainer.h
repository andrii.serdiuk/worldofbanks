﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "BankStateContainerCommons.h"
#include "Components/WidgetComponent.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDeviceBehaviorComponent.h"
#include "WorldOfBanks/Components/DeviceBehavior/BankDevicePrintCheckBehavior.h"
#include "WorldOfBanks/UI/Devices/BaseDeviceWidget.h"
#include "BankStateContainer.generated.h"


class UWidgetComponent;
USTRUCT()
struct WORLDOFBANKS_API FBankStateContainer
{
	GENERATED_BODY()

public:
	
	// Sets default values for this actor's properties
	FBankStateContainer();
	
	void Init(TScriptInterface<IDeviceStateful> InOwner, const FBankDeviceStateContainerConfig& Config);
	void Reset();
	
	FORCEINLINE TDeviceState GetCurrentState() const { return CurrentState; }
	
	template <typename T>
	T* GetBehavior() const { return GetBehavior<T>(T::StaticClass()); }
	
	template <typename T>
	T* GetBehavior(TDeviceState State) const { return Cast<T>(GetBehavior(State)); }

	UBankDeviceBehaviorComponent* GetBehavior(TDeviceState State) const { return State ? Behaviors[State] : nullptr; }
	
	template <typename State, typename WidgetType>
	WidgetType* GetWidgetFor() const { return GetWidgetFor<WidgetType>(State::StaticClass()); }

	template <typename WidgetType>
	WidgetType* GetWidgetFor(TDeviceState State) const { return State ? Cast<WidgetType>(Widgets[State]) : nullptr; }
	
	template <typename T>
	FORCEINLINE T* GetCurrentBehavior() const { return GetBehavior<T>(GetCurrentState()); }

	FORCEINLINE UBankDeviceBehaviorComponent* GetCurrentBehavior() const { return GetBehavior(GetCurrentState()); }
	
	template <typename WidgetType>
	FORCEINLINE WidgetType* GetCurrentWidget() const { return GetWidgetFor<WidgetType>(GetCurrentState()); }
	
	UBaseDeviceWidget* GetDeepestCurrentWidget() const;

	FORCEINLINE UBaseDeviceWidget* GetCurrentWidget() const { return GetCurrentState() ? Widgets[GetCurrentState()] : nullptr; }

	TScriptInterface<IDeviceStateful> GetContainerOwner() const;

	template <typename TNewState>
	TNewState* SetDeviceState(); // use this instead of raw assignment in order to trigger events

	template <typename TNewState, typename TWidgetOfNewState>
	TWidgetOfNewState* SetDeviceStateW();
	
	template <typename T>
	bool IsCurrentState();

	FORCEINLINE UWidgetComponent* GetWidgetComponent() const { return WidgetComponent; }
	
	FOnDeviceStateChangedDelegate OnChanged;
	
protected:

	UPROPERTY(Transient)
	TSubclassOf<UBankDeviceBehaviorComponent> CurrentState;
	
	UPROPERTY(Transient)
	UBaseDeviceWidget* CurrentWidget;
	
	UPROPERTY(Transient)
	TMap<TSubclassOf<UBankDeviceBehaviorComponent>, UBankDeviceBehaviorComponent*> Behaviors;

	UPROPERTY(Transient)
	TMap<TSubclassOf<UBankDeviceBehaviorComponent>, UBaseDeviceWidget*> Widgets;

	UPROPERTY(Transient)
	UWidgetComponent* WidgetComponent; // <-- the one we change widgets for

	UPROPERTY(Transient)
	TScriptInterface<IDeviceStateful> ContainerOwner;
};

template <typename TNewState>
TNewState* FBankStateContainer::SetDeviceState()
{
	static_assert(std::is_base_of_v<UBankDeviceBehaviorComponent, TNewState>, "TNewState must be derived from UBankDeviceBehaviorComponent");
	
	const TDeviceState PreviousDeviceState = CurrentState;
	CurrentState = TNewState::StaticClass();
	CurrentWidget = GetDeepestCurrentWidget();
	WidgetComponent->SetWidget(CurrentWidget);
	
	OnChanged.Broadcast(CurrentState, PreviousDeviceState);

	return GetBehavior<TNewState>();
}

template <typename TNewState, typename TWidgetOfNewState>
TWidgetOfNewState* FBankStateContainer::SetDeviceStateW()
{
	SetDeviceState<TNewState>();
	return GetCurrentWidget<TWidgetOfNewState>();
}

template <typename T>
bool FBankStateContainer::IsCurrentState()
{
	return GetCurrentState() == T::StaticClass();
}
