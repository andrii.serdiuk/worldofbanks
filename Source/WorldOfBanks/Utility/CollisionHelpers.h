﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

template <typename T>
TArray<T*> GetOverlappingActors(UStaticMeshComponent* Collision)
{
	static_assert(TIsDerivedFrom<T, AActor>::IsDerived, "T must be derived from AActor");
	TSubclassOf<T> ClassFilter = T::StaticClass();
	
	TArray<AActor*> OverlappingActors;
	Collision->GetOverlappingActors(OverlappingActors, ClassFilter);
	
	
	TArray<T*> Result;
	Result.Reserve(OverlappingActors.Num());
	
	for (AActor* OverlappingActor : OverlappingActors)
	{
		Result.Add(CastChecked<T>(OverlappingActor));
	}
	
	check(Result.Num() == OverlappingActors.Num());
	
	return Result;
}
