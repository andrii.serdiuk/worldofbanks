﻿// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

namespace Utils
{
	FRotator ClampRotator(FRotator Rotator, FRotator MinPerAxisClamp, FRotator MaxPerAxisClamp);
	
	FRotator ClampRotatorAroundDirection(FRotator Rotator, FRotator Direction, FRotator PerAxisClampAbs);
	
	float Dist(FRotator A, FRotator B);
}
