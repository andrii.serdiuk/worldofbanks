// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "PickupCash.h"
#include "WorldOfBanks/Components/WalletComponent.h"
#include "WorldOfBanks/Bank/Banknote.h"
#include "WorldOfBanks/Core/CorePlayerController.h"
#include "WorldOfBanks/Core/CoreCharacter.h"
#include "WorldOfBanks/Core/CoreHUD.h"
#include "WorldOfBanks/Core/SaveObjects.h"

APickupCash::APickupCash(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	VisualMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cash Mesh"));
	RootComponent = VisualMesh;

	// We need replicate static mesh component for setting static mesh asset in Init function
	VisualMesh->SetIsReplicated(true);
}

void APickupCash::Init(const FBanknote& InBanknote, int32 InAmount)
{
	Banknote = InBanknote;
	Amount = InAmount;

	VisualMesh->SetStaticMesh(InBanknote.Mesh.LoadSynchronous());
}

bool APickupCash::CanInteractWith(APawn* Interactor) const
{
	return Cast<ACoreCharacter>(Interactor) != nullptr;
}

const APickupCash& APickupCash::operator>>(FPickupCashSaveData& CashSaveData) const
{
	CashSaveData.ActorTransform = GetActorTransform();
	CashSaveData.ActorTransform.SetScale3D(FVector(1.f));
	CashSaveData.Banknote = Banknote;
	CashSaveData.Amount = Amount;
	CashSaveData.PickupActorClass = GetClass();

	return *this;
}

APickupCash* APickupCash::SpawnFromSave(UWorld* World, const FPickupCashSaveData& CashSaveData)
{
	check(World);
	const TSubclassOf<APickupCash> ActorClass = CashSaveData.PickupActorClass.LoadSynchronous();
	check(ActorClass);

	APickupCash* Cash = World->SpawnActorDeferred<APickupCash>(ActorClass, CashSaveData.ActorTransform);
	Cash->Init(CashSaveData.Banknote, CashSaveData.Amount);
	Cash->FinishSpawning(CashSaveData.ActorTransform);

	return Cash;
}

void APickupCash::InteractWith(APawn* Interactor)
{
	const ACorePlayerController* Controller = Interactor->GetController<ACorePlayerController>();
	if (Controller->GetWallet()->AddCash(Banknote, Amount))
	{
		Destroy();
	}
	else
	{
		Controller->ClientNotifyFullWallet(Banknote);
	}
}