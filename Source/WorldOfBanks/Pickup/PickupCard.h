// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "CoreMinimal.h"
#include "WorldOfBanks/Game/InteractableActor.h"
#include "WorldOfBanks/Bank/CardInfo.h"
#include "PickupCard.generated.h"

struct FPickupCardSaveData;
	
UCLASS(Abstract)
class WORLDOFBANKS_API APickupCard : public AInteractableActor
{
	GENERATED_BODY()
	
public:
	APickupCard(const FObjectInitializer& ObjectInitializer);
	
	FORCEINLINE const FCardInfo& GetCardInfo() const { return Info; }

	virtual bool CanInteractWith(APawn* Interactor) const override;

	void Init(const FCardInfo& InInfo);

	/** Saves PickupCard info to save struct */
	const APickupCard& operator>>(FPickupCardSaveData& CardSaveData) const;

	static APickupCard* SpawnFromSave(UWorld* World, const FPickupCardSaveData& CardSaveData);

protected:
	/** Called on Server */
	virtual void InteractWith(APawn* Interactor) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category ="PickupCard")
	UStaticMeshComponent* VisualMesh;

private:
	UPROPERTY(EditAnywhere)
	FCardInfo Info;
};