// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#include "PickupCard.h"
#include "WorldOfBanks/Components/WalletComponent.h"
#include "WorldOfBanks/Core/CoreCharacter.h"
#include "WorldOfBanks/Core/CorePlayerController.h"
#include "WorldOfBanks/Core/SaveObjects.h"

APickupCard::APickupCard(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	VisualMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Card Mesh"));
	RootComponent = VisualMesh;
}

void APickupCard::Init(const FCardInfo& InInfo)
{
	Info = InInfo;
}

const APickupCard& APickupCard::operator>>(FPickupCardSaveData& CardSaveData) const
{
	CardSaveData.ActorTransform = GetActorTransform();
	CardSaveData.ActorTransform.SetScale3D(FVector(1.f));
	CardSaveData.CardInfo = Info;
	CardSaveData.PickupActorClass = GetClass();

	return *this;
}

APickupCard* APickupCard::SpawnFromSave(UWorld* World, const FPickupCardSaveData& CardSaveData)
{
	check(World);
	const TSubclassOf<APickupCard> ActorClass = CardSaveData.PickupActorClass.LoadSynchronous();
	check(ActorClass);

	APickupCard* Card = World->SpawnActorDeferred<APickupCard>(ActorClass, CardSaveData.ActorTransform);
	Card->Init(CardSaveData.CardInfo);
	Card->FinishSpawning(CardSaveData.ActorTransform);

	return Card;
}

void APickupCard::InteractWith(APawn* Interactor)
{
	const ACorePlayerController* Controller = Interactor->GetController<ACorePlayerController>();
	if(Controller->GetWallet()->AddCard(Info))
	{
		Destroy();
	}
}

bool APickupCard::CanInteractWith(APawn* Interactor) const
{
	return (Cast<ACoreCharacter>(Interactor) != nullptr);
}

