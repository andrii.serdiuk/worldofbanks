// Copyright World Of Banks - Sydorov, Barannik, Okhrimenko, Serdiuk

#pragma once

#include "WorldOfBanks/Bank/Banknote.h"
#include "WorldOfBanks/Game/InteractableActor.h"
#include "PickupCash.generated.h"

class ACorePlayerController;
struct FPickupCashSaveData;

UCLASS(Abstract)
class WORLDOFBANKS_API APickupCash : public AInteractableActor
{
	GENERATED_BODY()
	
public:

	APickupCash(const FObjectInitializer& ObjectInitializer);
	
	FORCEINLINE int32 GetAmount() const { return Amount; }
	
	FORCEINLINE const FBanknote& GetBanknote() const { return Banknote; }

	virtual bool CanInteractWith(APawn* Interactor) const override;

	void Init(const FBanknote& InBanknote, int32 InAmount);

	/** Saves PickupCash info to save struct */
	const APickupCash& operator>>(FPickupCashSaveData& CashSaveData) const;

	static APickupCash* SpawnFromSave(UWorld* World, const FPickupCashSaveData& CashSaveData);

protected:
	/** Called on Server */
	virtual void InteractWith(APawn* Interactor) override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category ="PickupCash")
	UStaticMeshComponent* VisualMesh;
	
private:
	UPROPERTY(EditAnywhere)
	int32 Amount;
	
	UPROPERTY(EditAnywhere)
	FBanknote Banknote;
};